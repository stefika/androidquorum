package edu.unlv.cs.spring2019graphexperiment.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.unlv.cs.spring2019graphexperiment.Constants;
import edu.unlv.cs.spring2019graphexperiment.Controller.Group1Controller;
import edu.unlv.cs.spring2019graphexperiment.Controller.Group2Controller;
import edu.unlv.cs.spring2019graphexperiment.Controller.Group3Controller;
import edu.unlv.cs.spring2019graphexperiment.Controller.TrainingController;
import edu.unlv.cs.spring2019graphexperiment.ExperimentManager;
import edu.unlv.cs.spring2019graphexperiment.R;

public class GroupChoiceActivity extends Activity {
    private int sessionNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        Intent mIntent = getIntent();
        sessionNumber = mIntent.getIntExtra(Constants.EXTRA_SESSION, 1);
    }

    private void goToSlide(int groupNumber, View v) {
        Intent intent = new Intent(v.getContext(), SlideActivity.class);

        switch (groupNumber) {
            case 0:
                ExperimentManager.setController(new TrainingController(sessionNumber));
                break;
            case 1:
                ExperimentManager.setController(new Group1Controller(sessionNumber));
                break;
            case 2:
                ExperimentManager.setController(new Group2Controller(sessionNumber));
                break;
            case 3:
                ExperimentManager.setController(new Group3Controller(sessionNumber));
                break;
        }

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Button button0 = findViewById(R.id.b_training);
        button0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setGroup(0);
                goToSlide(0, v);
            }
        });

        final Button button1 = findViewById(R.id.b_group1);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setGroup(1);
                goToSlide(1, v);
            }
        });

        final Button button2 = findViewById(R.id.b_group2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setGroup(2);
                goToSlide(2, v);
            }
        });

        final Button button3 = findViewById(R.id.b_group3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setGroup(3);
                goToSlide(3, v);
            }
        });
    }
}
