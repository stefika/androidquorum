package edu.unlv.cs.spring2019graphexperiment.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.File;

import edu.unlv.cs.spring2019graphexperiment.Constants;
import edu.unlv.cs.spring2019graphexperiment.ExperimentManager;
import edu.unlv.cs.spring2019graphexperiment.R;

public class SelectIdActivity  extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participant);

        Button beginButton = (Button) findViewById(R.id.b_begin);
        beginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Get the textbox with the participant number.
                final EditText textbox = (EditText)findViewById(R.id.txt_participantNumber);
                if (!textbox.getText().toString().matches("")) {
                    //Parse the textbox input into a String variable.
                    String partipantNumString = textbox.getText().toString();

                    //Convert the String variable to an integer variable.
                    int participantId = Integer.parseInt(partipantNumString);

                    ExperimentManager.setParticipantId(participantId);

                    //On click, load the ControlActivity.java class.
                    startActivity(new Intent(getApplicationContext(), SelectSessionActivity.class));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.sharemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                shareFile();
                break;
        }

        return true;
    }

    private void shareFile() {
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        File folder = ExperimentManager.getPublicDataStorage("data");


        File file = new File(folder.getAbsolutePath() + File.separator + Constants.DATA_FILE);
        if(file.exists()) {
            intentShareFile.setType("text/csv");
            intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+file.getAbsolutePath()));

            intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                    "Sharing File...");
            intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");

            startActivity(Intent.createChooser(intentShareFile, "Share File"));
        }
    }
}
