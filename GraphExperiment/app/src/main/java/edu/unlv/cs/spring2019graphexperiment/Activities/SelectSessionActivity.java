package edu.unlv.cs.spring2019graphexperiment.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import edu.unlv.cs.spring2019graphexperiment.Constants;
import edu.unlv.cs.spring2019graphexperiment.Controller.AudioExperimentController;
import edu.unlv.cs.spring2019graphexperiment.Controller.BarchartController;
import edu.unlv.cs.spring2019graphexperiment.Controller.DiagramController;
import edu.unlv.cs.spring2019graphexperiment.Controller.HapticExperimentController;
import edu.unlv.cs.spring2019graphexperiment.Controller.MapsController;
import edu.unlv.cs.spring2019graphexperiment.Controller.TrigController;
import edu.unlv.cs.spring2019graphexperiment.ExperimentManager;
import edu.unlv.cs.spring2019graphexperiment.R;

public class SelectSessionActivity extends Activity implements View.OnTouchListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_session);
    }

    private void goToGroupScreen(int sessionNumber, View v) {
        Intent intent = new Intent(v.getContext(), GroupChoiceActivity.class);

        intent.putExtra(Constants.EXTRA_SESSION, sessionNumber);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final Button buttonExp = findViewById(R.id.b_experiment);
        buttonExp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(0);
                goToSlide(ExperimentManager.getParticipantId(), 0, v);
            }
        });

        final Button buttonVoice = findViewById(R.id.b_voiceover);
        buttonVoice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(1);
                goToSlide(ExperimentManager.getParticipantId(), 1, v);
            }
        });

        final Button button0 = findViewById(R.id.b_barcharts);
        button0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(2);
                goToSlide(ExperimentManager.getParticipantId(), 2, v);
            }
        });

        final Button button0b = findViewById(R.id.b_trig);
        button0b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(3);
                goToSlide(ExperimentManager.getParticipantId(), 3, v);
            }
        });

        final Button button1 = findViewById(R.id.b_diagram);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(4);
                goToSlide(ExperimentManager.getParticipantId(), 4, v);
            }
        });

        final Button button2 = findViewById(R.id.b_maps);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ExperimentManager.setSessionId(5);
                goToSlide(ExperimentManager.getParticipantId(), 5, v);
            }
        });

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }

    private void goToSlide(int participantId,int sessionNumber, View v) {
        Intent intent = new Intent(v.getContext(), SlideActivity.class);
        int groupNumber = (participantId-1) % 4; // To start at group 0 given pids starting at 1
        ExperimentManager.setGroup(groupNumber);
        if (sessionNumber == 1) {
            ExperimentManager.setController(new AudioExperimentController(sessionNumber, groupNumber));
        } else if (sessionNumber == 2) {
            ExperimentManager.setController(new BarchartController(sessionNumber));
        } else if (sessionNumber == 3) {
            ExperimentManager.setController(new TrigController(sessionNumber));
        } else if (sessionNumber == 4) {
            ExperimentManager.setController(new DiagramController(sessionNumber));
        } else if (sessionNumber == 5) {
            ExperimentManager.setController(new MapsController(sessionNumber));
        } else if (sessionNumber == 0) {
            ExperimentManager.setController(new HapticExperimentController(sessionNumber, groupNumber));
        }


        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
