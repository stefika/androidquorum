package edu.unlv.cs.spring2019graphexperiment.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Switch;

import java.util.List;
import java.util.Locale;

import edu.unlv.cs.spring2019graphexperiment.Behavior.Node;
import edu.unlv.cs.spring2019graphexperiment.Behavior.ListNavigation;
import edu.unlv.cs.spring2019graphexperiment.Constants;
import edu.unlv.cs.spring2019graphexperiment.Controller.Event;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.ExperimentManager;
import edu.unlv.cs.spring2019graphexperiment.R;
import edu.unlv.cs.spring2019graphexperiment.TouchManagement;
import edu.unlv.cs.spring2019graphexperiment.TreeNode;
import edu.unlv.cs.vibration.VibrationManager;

public class SlideActivity extends Activity implements View.OnTouchListener {

    ImageView img;

    TouchManagement touchManagement;
    VibrationManager vibrationManager;
    boolean vibrating = false;
    int vibrationnumber = -1;
    Menu navigationMenu;

    private ToneGenerator beep = new ToneGenerator(AudioManager.STREAM_MUSIC, 500);

    float paintx1 = 0;
    float painty1 = 0;
    float paintx2 = 0;
    float painty2 = 0;

    Bitmap bitmap;
    Canvas canvas;

    TextToSpeech t1;

    SlideController controller;
    SlideBehavior behavior;

    long timestamp_start;
    long timestamp_end;

    long tapTime = System.currentTimeMillis();

    boolean hasWritePermission = false;
    boolean newslide = false;
    boolean should_click = true;
    boolean high_speed = true;
    boolean describes_fully = false;
    boolean temp_blocking = false;

    List<String> overviewList;
    String overviewString;
    int overviewListIndex = -1;

    Node currentNode;
    List<Node> mainNode;

    boolean locked = false;

    private boolean doOverviewStuff = false;

    MediaPlayer click_mp;
    MediaPlayer tang_mp;
    private Switch speedSwitch;
    private Switch descriptionSwitch;

    public SlideActivity() {
        super();
        this.controller = ExperimentManager.getController();
    }

    public void setCurrentNode(Node newNode) {
        int nodeIndex = findNode(newNode);
        if (nodeIndex != -1) {
            overviewListIndex = nodeIndex;
        }
    }

    public Node getCurrentNode() {
        System.out.println("locking before check");
        //|| mainNode.get(overviewListIndex).getChildren() == null || mainNode.get(overviewListIndex).getChildren().size() == 0
        if (mainNode == null || overviewListIndex == -1 || mainNode.get(overviewListIndex) == null ) {
            System.out.println("locking in check");
            return null;
        }
        System.out.println("locking after check");
        return mainNode.get(overviewListIndex).getCurrentChild();
    }

    public void lockNode() {
        locked = true;
    }

    private int findNode(Node node) {
        for (int i = 0; i < mainNode.size(); i++) {
            if (mainNode.get(i).getName().equals(node.getName())) {
                return i;
            }
        }
        return -1;
    }

    public void setMainNode(List<Node> newNode) {
        this.mainNode = newNode;
    }

    @Override
    protected void onStart() {
        super.onStart();
        click_mp = MediaPlayer.create(this, R.raw.click);
        tang_mp = MediaPlayer.create(this, R.raw.tang);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        touchManagement = new TouchManagement();

        setContentView(R.layout.activity_slide);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.US);
                }
            }
        });

        t1.setSpeechRate(2.0f);

        img = (ImageView) findViewById(R.id.imgRandom);

        initPopup();
        ensurePermissions();
        startNewSlide();
    }

    private void ensurePermissions() {
//        if (ContextWrapper.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED ) {
        if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)  != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            // No explanation needed; request the permission
//            ActivityCompat.requestPermissions(this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    Constants.REQUEST_WRITE_PERMISSION);
            this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 23);
//                    Constants.REQUEST_WRITE_PERMISSION);
        } else {
            hasWritePermission = true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    hasWritePermission = true;
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    hasWritePermission = false;
                    //TODO handle this
                }
                return;
            }

        }
    }

    private void startNewSlide() {
        loadSlide();
        controller.addStartEvent();

        newslide = true;
        invalidateOptionsMenu();
    }

    private void loadSlide() {
        behavior = controller.getSlideBehavior();

        Resources res = getResources();
        Bitmap bm = BitmapFactory.decodeResource(res, controller.getDrawableId());
        Bitmap bitmapRightSize = Bitmap.createScaledBitmap(bm, 2048, 1360, false);

        bitmap = bitmapRightSize.copy(Bitmap.Config.ARGB_8888, true);
        canvas = new Canvas(bitmap);

        overviewListIndex = -1;
        overviewString = "";
        canvas.drawBitmap(bitmap, 0, 0, null);

        if (behavior instanceof SlideBehaviorPolygon) {
            System.out.println("Is Slide behavior polygon");
            SlideBehaviorPolygon b = (SlideBehaviorPolygon) behavior;

            b.drawPolygons(this);
            overviewList = ((SlideBehaviorPolygon) behavior).getOverviewList();
            overviewString = ((SlideBehaviorPolygon) behavior).getOverviewString();

        } else {
            overviewList = null;
        }

        if (behavior instanceof ListNavigation) {
            System.out.println("instanceof");
            if (behavior != null) {
                System.out.println("behavior != null");
                ListNavigation ln = (ListNavigation) behavior;

                if (ln.getOverviewStructure() != null) {
                    System.out.println("setup main node");
                    mainNode = ln.getOverviewStructure();
                    currentNode = mainNode.get(0);
                    doOverviewStuff = true;
                    System.out.println("Overview set true");
                } else {
                    doOverviewStuff = false;
                    System.out.println("Overview set false");
                }
            }
        }
        System.out.println("Do Overview Stuff: " + doOverviewStuff);
        img.setImageBitmap(bitmap);

        img.setOnTouchListener(this);

    }

//    private long last_click = 0l;

    int clickCount = 0;
    int upClickCount = 0;
    int doubleFingerClickCount = 0;
    float mainFingerStartPositionX = -1;
    float mainFingerStartPositionY = -1;
    float mainFingerLastPositionX = -1;
    float mainFingerLastPositionY = -1;
    int mainFingerId = 0;


    @Override
    protected void onResume() {
        super.onResume();

//        click_mp = MediaPlayer.create(this, R.raw.click);
//        tang_mp = MediaPlayer.create(this, R.raw.tang);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        click_mp.release();
//        tang_mp.release();
    }


    long doubleFingerTapTimer = 0l;

    @Override
    public boolean onTouch(View view, final MotionEvent ev) {
        int action = ev.getActionMasked();

//        if (ev.getPointerCount() >= 2) {
//            Log.d("Touch", "multitouch: " + ev.getPointerCount());
//            if( ev.getPointerCount() == 3) {
//
//            }
//        } else {
            Log.d("Touch", "Touchevent");
            switch (action) {
                case MotionEvent.ACTION_OUTSIDE:
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (ev.getPointerCount() == 1) {
                        react(ev, Event.ACTION_MOVE);
                    } else {
                        int mainIndex = ev.findPointerIndex(mainFingerId);
                        if (mainIndex > -1) {
                            mainFingerLastPositionX = ev.getX(mainIndex);
                            mainFingerLastPositionY = ev.getY(mainIndex);
                        } else {
                        }
                    }
                    break;
                case MotionEvent.ACTION_DOWN:

                    if ((System.currentTimeMillis() - doubleFingerTapTimer) < 400) {
                        doubleFingerClickCount++;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (doubleFingerClickCount == 1) {
                                    System.out.println("Double finger tap");
                                    reactDoubleFingerTap();
                                } else if (doubleFingerClickCount >= 2) {
                                    System.out.println("Double finger double Tap");
                                    reactDoubleFingerDoubleTap();
                                }
                                doubleFingerClickCount = 0;
                            }
                        }, 400);
                        return true;
                    }
                    System.out.println("ACTION DOWN");
                    if (ev.getPointerCount() == 1 && System.currentTimeMillis() - tapTime < 400) {
                        clickCount++;
                        // This allows the system to recognize the down-up-down events as double
                        // tap triggered on down
                        System.out.println("Click count: " + clickCount);
                        if (clickCount == 1) {
                            final float x_event = ev.getX();
                            final float y_event = ev.getY();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (clickCount >= 1) {
                                        System.out.println("DOUBLE TAP");
                                        doubleTapReact(x_event, y_event);
                                    }
                                    clickCount = 0;
                                }
                            }, 400); // custom double
                        }
                    }

                    if (ev.getPointerCount() == 1) {
                        react(ev, Event.ACTION_DOWN);
                        paintx1 = ev.getX();
                        painty1 = ev.getY() * (1360f/1525f);
                        tapTime = System.currentTimeMillis();

                    }
                    mainFingerStartPositionX = ev.getX();
                    mainFingerStartPositionY = ev.getY();
                    mainFingerLastPositionX = 0;
                    mainFingerLastPositionY = 0;
                    mainFingerId = ev.getPointerId(0);
                    break;

                case MotionEvent.ACTION_POINTER_DOWN:
                   // System.out.println("POINTER DOWN: " + ev.getActionIndex());

                    if (ev.getPointerCount() == 3) {
                        final Handler triFingerHandler = new Handler();
                        triFingerHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                float xDelta = mainFingerLastPositionX - mainFingerStartPositionX;
                                float yDelta = mainFingerLastPositionY - mainFingerStartPositionY;
                            //    System.out.println("XDelta: " + xDelta + " LastPositionX: " + mainFingerLastPositionX + " StartPosition:" + mainFingerStartPositionX);
                            //    System.out.println("YDelta: " + yDelta + " LastPositionY: " + mainFingerLastPositionY + " StartPosition:" + mainFingerStartPositionY);

                                float absDeltaX = Math.abs(xDelta);
                                float absDeltaY = Math.abs(yDelta);

                                if (absDeltaX > absDeltaY ) {
                                    //     System.out.println("SWIPE HORIZONTAL");
                                    if (xDelta > 250f) {
                                        backSlide();
                                    } else {
                                        nextSlide();
                                    }
                                } if (absDeltaY > 250f) {
                                    //    System.out.println("SWIPE VERTICAL");
                                    startMenu();
                                }
                            }
                        }, ViewConfiguration.getDoubleTapTimeout());
                    } else if (ev.getPointerCount() == 2) {
                        System.out.println("Double Finger down");
                        doubleFingerTapTimer = System.currentTimeMillis();
                        final Handler duoFingerHandler = new Handler();
                        duoFingerHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                float xDelta = mainFingerLastPositionX - mainFingerStartPositionX;
                                float yDelta = mainFingerLastPositionY - mainFingerStartPositionY;
                                //    System.out.println("XDelta: " + xDelta + " LastPositionX: " + mainFingerLastPositionX + " StartPosition:" + mainFingerStartPositionX);
                                //    System.out.println("YDelta: " + yDelta + " LastPositionY: " + mainFingerLastPositionY + " StartPosition:" + mainFingerStartPositionY);

                                float absDeltaX = Math.abs(xDelta);
                                float absDeltaY = Math.abs(yDelta);
                                System.out.println("absDeltaX : " + xDelta);
                                System.out.println("absDeltaY : " + yDelta);
                                System.out.println("DO OVERVIEW STUFF: " + doOverviewStuff);
                                if (doOverviewStuff) {
                                    if (absDeltaX > absDeltaY) {
                                        //     System.out.println("SWIPE HORIZONTAL");
                                        if (xDelta > 200f) {
                                            backOverview();
                                        } else if (xDelta < -200f) {

                                            nextOverview();
                                        }
                                    } else {

                                        if (yDelta > 200f) {
                                            //    System.out.println("SWIPE VERTICAL");
                                            //  startMenu();
                                            upOverview();
                                        } else if (yDelta < -200) {
                                            downOverview();
                                        }
                                    }
                                }
                            }
                        }, ViewConfiguration.getDoubleTapTimeout());
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    System.out.println("ACTION UP");
                    reactUp(ev);
                    paintx2 = ev.getX();
                    painty2 = ev.getY() * (1360f/1525f);
                    Log.d("Draw", "drawevent: {" + paintx1 + "f, " + painty1 + "f}, {" + paintx2 + "f, " + painty2 + "f},");

                    System.out.println("Double finger tap pre timer: " + (System.currentTimeMillis() - doubleFingerTapTimer) + " pointers: " + ev.getPointerCount());
                    if ((System.currentTimeMillis() - doubleFingerTapTimer) < 400) {
                        doubleFingerClickCount++;
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (doubleFingerClickCount == 1) {
                                    System.out.println("Double finger tap");
                                    reactDoubleFingerTap();
                                } else if (doubleFingerClickCount >= 2) {
                                    System.out.println("Double finger double Tap");
                                    reactDoubleFingerDoubleTap();
                                }
                                doubleFingerClickCount = 0;
                            }
                        }, 400);
                        return true;
                    }

                    if (ev.getPointerCount() == 1 && (System.currentTimeMillis() - tapTime) < ViewConfiguration.getTapTimeout()) {
                        upClickCount++;
                        // This just reacts when the user taps and lifts the finger again to re-read an area for example
                        if (upClickCount == 1) {
                            final float x_event = ev.getX();
                            final float y_event = ev.getY();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (upClickCount == 1) {
                                        System.out.println("TAP REACT");
                                        tapReact(x_event, y_event);
                                    }

                                    upClickCount = 0;
                                }
                            }, 400); // custom double
                        }
                        break;
                    }

                    break;
            }
//        }
        return true;
    }

    void reactDoubleFingerTap() {
//        stopVibration();
//        t1.stop();
//        if (click_mp.isPlaying()) {
//            click_mp.stop();
//        }
//        if (tang_mp.isPlaying()) {
//            tang_mp.stop();
//        }
        eventBehaviorDoubleFingerTap();
    }

    void reactDoubleFingerDoubleTap() {
        eventBehaviorDoubleFingerDoubleTap();
    }

    void reactUp(MotionEvent ev) {
        controller.addTouchEventUp(ev.getX(), ev.getY());
        stopVibration();
    }

    void tapReact(float eventX, float eventY) {
        int x = Integer.valueOf((int) eventX);
        int y = Integer.valueOf((int) (eventY* (1360f/1525f)));

        if (0 < x && x <= bitmap.getWidth() && 0 < y && y < bitmap.getHeight()) {

            eventBehaviorTap(x, y);
        }
    }

    void doubleTapReact(float eventX, float eventY) {
        int x = Integer.valueOf((int) eventX);
        int y = Integer.valueOf((int) (eventY * (1360f/1525f)));


        if (0 < x && x <= bitmap.getWidth() && 0 < y && y < bitmap.getHeight()) {
            eventBehaviorDoubleTap(x,y);
        }
    }

    void react(MotionEvent ev, String action) {
        float eventX;
        float eventY;
        eventX = ev.getX();
        eventY = ev.getY() * (1360f/1525f);


        int x = Integer.valueOf((int) eventX);
        int y = Integer.valueOf((int) eventY);


        if (x>=0 && x <= bitmap.getWidth() && y>=0 && y < bitmap.getHeight()) {
            eventBehavior(x, y, action);
        }
    }

    public void drawPolygon(float[][] polygon) {

        Paint paint = new Paint();
        paint.setColor(Color.CYAN);
        paint.setStyle(Paint.Style.STROKE);
        Path path = new Path();
        path.moveTo(polygon[0][0], polygon[0][1]);
        for (int b = 0; b < polygon.length; b++) {
            float[] step = polygon[b];
            path.lineTo(step[0], step[1]);
        }
        path.lineTo(polygon[0][0], polygon[0][1]);

        canvas.drawPath(path, paint);
    }

    private void eventBehavior(int x, int y, String action) {
        behavior.touchReaction(x, y, bitmap, this, action);
    }

    private void eventBehaviorTap(int x, int y) {
        if (behavior instanceof SlideBehaviorPolygon) {
            ((SlideBehaviorPolygon) behavior).tapReaction(x, y, bitmap, this);
        }
    }

    private void eventBehaviorDoubleTap(int x, int y) {
        if (behavior instanceof SlideBehaviorPolygon) {
            ((SlideBehaviorPolygon) behavior).doubleTapReaction(x, y, bitmap, this);
        }
    }

    private void eventBehaviorDoubleFingerTap() {
        if (behavior instanceof SlideBehaviorPolygon) {
            ((SlideBehaviorPolygon) behavior).doubleFingerTapReaction(this);
        }
    }

    private void eventBehaviorDoubleFingerDoubleTap() {
        if (behavior instanceof SlideBehaviorPolygon) {
            ((SlideBehaviorPolygon) behavior).doubleFingerDoubleTapReaction(this);
        }
    }

    public void speak(String text) {
//        if (temp_blocking) {
//            if (!t1.isSpeaking() ) {
//                temp_blocking = false;
                t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//            }
//        } else {
//            temp_blocking = false;
//            t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//        }

    }

    public void speakQueue(String text) {
//        if (temp_blocking) {
//            if (!t1.isSpeaking() ) {
//                temp_blocking = false;
        t1.speak(text, TextToSpeech.QUEUE_ADD, null);
//            }
//        } else {
//            temp_blocking = false;
//            t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//        }

    }

    public void speakBlocking(String text) {
//        if (temp_blocking) {
//            if (!t1.isSpeaking() ) {
//                temp_blocking = true;
                t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//            }
//        } else {
//            temp_blocking = true;
//            t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//        }

    }

    public void playTone() {
        beep.startTone(ToneGenerator.TONE_CDMA_NETWORK_BUSY, 200);
    }

    public void playClick() {
        if (should_click && !click_mp.isPlaying()) {
           // img.playSoundEffect(SoundEffectConstants.CLICK);
//            beep.startTone(ToneGenerator.TONE_PROP_NACK, 50);
            click_mp.start();
        }
    }

    public void playToneQuick() {
//        if (!t1.isSpeaking()) {
//            beep.startTone(ToneGenerator.TONE_CDMA_HIGH_SS, 50);
//
//        }
        if (!tang_mp.isPlaying()) {
            tang_mp.start();
        }
    }

    public void startVibration() {
        if (vibrationnumber != 0) {
            vibrationnumber = 0;
            vibrationManager.vibratePattern(Constants.VIB_PATTERN, 0);
        }
    }

    public void startVibration(int vibPatternNumber) {
        if (vibrationnumber != vibPatternNumber) {
            vibrationnumber = vibPatternNumber;
            vibrationManager.stop();
            if (vibPatternNumber == 0) {
                vibrationManager.vibratePattern(Constants.VIB_PATTERN, 0);
            } else if (vibPatternNumber == 1) {
                vibrationManager.vibratePattern(Constants.VIB_CONSTANT, 0);
            } else if (vibPatternNumber == 2) {
                vibrationManager.vibratePattern(Constants.VIB_SHORT_BUZZ, 0);
            } else if (vibPatternNumber == 3) {
                vibrationManager.vibratePattern(Constants.VIB_TRANSITION_BUMP, 0);
            } else if (vibPatternNumber == 4) {
                vibrationManager.vibratePattern(Constants.VIB_WEAPON, 0);
            } else if (vibPatternNumber == 5) {
                vibrationManager.vibratePattern(Constants.VIB_ENGINE, 0);
            }
        }
    }

    public void vibrateClick() {
        if (vibrationnumber!= 0) {
            vibrationnumber = -1;
            vibrationManager.vibratePattern(Constants.VIB_CLICK, -1);
        }
    }

    public void stopVibration() {
        vibrationnumber = -1;
        vibrationManager.stop();
    }


    public TouchManagement getTouchManagement() {
        return touchManagement;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu, menu);
//        return true;
//    }

//    @Override
//    public boolean onPrepareOptionsMenu (Menu menu) {
//        navigationMenu = menu;
//        if (controller.getSlideNumber() == 0) {
//            menu.getItem(0).setEnabled(false);
//        } else {
//            menu.getItem(0).setEnabled(true);
//        }
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.next:
//                nextSlide();
//                break;
//
//            case R.id.back:
//                backSlide();
//                break;
//
//        }
//
//        return true;
//    }

    private void startMenu() {
        displayPopupWindow(img);
    }

    public void stopAllSignals() {
        stopVibration();
        t1.stop();
        if (click_mp.isPlaying()) {
            click_mp.stop();
        }
        if (tang_mp.isPlaying()) {
            tang_mp.stop();
        }
    }

    private void nextSlide() {
        if (controller.nextSlide()) {
            t1.stop();
            stopVibration();
            finalizeData();
            resetCoordinates();
            doOverviewStuff = false;
            startNewSlide();

        } else {
            doOverviewStuff = false;
            t1.stop();
            stopVibration();
            finalizeData();
            resetCoordinates();
            ExperimentManager.reset();
            //TODO: finish
            startActivity(new Intent(getApplicationContext(), SelectIdActivity.class));
        }
    }

    private void backSlide() {
        if (controller.backSlide()) {
            t1.stop();
            stopVibration();
            finalizeData();
            resetCoordinates();
            startNewSlide();
        }
    }

    private void nextOverview() {
        System.out.println("Diagram next");
        String currentText = "error";
        if (mainNode != null && overviewListIndex >=0 && mainNode.size() >= overviewListIndex ) {
            if (mainNode.get(overviewListIndex).hasChildren()) {
                currentText = mainNode.get(overviewListIndex).getNextChild().getName();
            } else {
                currentText = mainNode.get(overviewListIndex).getName();

            }
        }
        if (mainNode != null && mainNode.size() > 1) {
            speakQueue(currentText);
        }
        getController().addTouchEvent(-1, -1, Event.SWIPE_NEXT, stripStringOfComma(currentText), Event.ACTION_SWIPE_NEXT);

    }

    private String stripStringOfComma(String s) {
        String r = s.replaceAll("\\,","\\_");
        System.out.println(r);
        return r;
    }


    private void upOverview() {
//        if (describes_fully) {
//            if (!overviewString.isEmpty()) {
//                speak(overviewString);
//            }
//        } else if(overviewList != null) {
        System.out.println("Diagram up");
        String currentText = "error";
        overviewListIndex--;
        if ( mainNode!= null && mainNode.size() >= 1) {
            if (overviewListIndex >= 0 ) {
                mainNode.get(overviewListIndex).resetStatus();
                currentText = mainNode.get(overviewListIndex).getName();
            } else {
                overviewListIndex = mainNode.size()-1;
                mainNode.get(overviewListIndex).resetStatus();
                currentText = mainNode.get(overviewListIndex).getName();
            }
        }

        speakQueue (currentText);
        getController().addTouchEvent(-1, -1, Event.SWIPE_UP, stripStringOfComma(currentText), Event.ACTION_SWIPE_UP);
//        }
    }

    private void downOverview() {
        System.out.println("Diagram down");
        String currentText = "error";
        overviewListIndex++;
        if ( mainNode!= null && mainNode.size() >= 1) {
            if (mainNode.size() > overviewListIndex) {
                mainNode.get(overviewListIndex).resetStatus();
                currentText = mainNode.get(overviewListIndex).getName();
            } else {
                overviewListIndex = 0;
                mainNode.get(overviewListIndex).resetStatus();
                currentText = mainNode.get(overviewListIndex).getName();
            }
        }

        speakQueue (currentText);
        getController().addTouchEvent(-1, -1, Event.SWIPE_DOWN, stripStringOfComma(currentText), Event.ACTION_SWIPE_DOWN);
//        }
    }

    private void backOverview() {

        System.out.println("Diagram back");
        String currentText = "error";
        if (mainNode != null && overviewListIndex >=0 && mainNode.size() >= overviewListIndex) {
            if (mainNode.get(overviewListIndex).hasChildren()) {
                currentText = mainNode.get(overviewListIndex).getPreviousChild().getName();
            } else {
                currentText = mainNode.get(overviewListIndex).getName();
            }
        }
        if (mainNode != null && mainNode.size() > 1) {
            speakQueue(currentText);
        }
        getController().addTouchEvent(-1, -1, Event.SWIPE_BACK, stripStringOfComma(currentText), Event.ACTION_SWIPE_BACK);
    }




    private void resetCoordinates() {
        clickCount = 0;
        upClickCount = 0;
        doubleFingerClickCount = 0;
        mainFingerStartPositionX = -1;
        mainFingerStartPositionY = -1;
        mainFingerLastPositionX = -1;
        mainFingerLastPositionY = -1;
        mainFingerId = 0;
    }

    public SlideController getController() {
        return controller;
    }

    private void finalizeData() {
        controller.addEndEvent();
        controller.commitData();
    }


    PopupWindow popup;
    Switch clickSwitch;
    private void initPopup () {
        popup = new PopupWindow(SlideActivity.this);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                controller.addContinueEvent();
            }
        });

        //popup.setBack(R.color.white);

        clickSwitch = (Switch) findViewById(R.id.click_switch);
        clickSwitch = new Switch(this);
        clickSwitch.setChecked(should_click);
        clickSwitch.setText("Enable clicking sound");

        speedSwitch = (Switch) findViewById(R.id.speed_switch);
        speedSwitch = new Switch(this);
        speedSwitch.setChecked(high_speed);
        speedSwitch.setText("Fast Speed");

        descriptionSwitch = (Switch) findViewById(R.id.description_switch);
        descriptionSwitch = new Switch(this);
        descriptionSwitch.setChecked(describes_fully);
        descriptionSwitch.setText("Describe fully");

        LinearLayout layoutOfPopup = new LinearLayout(this);
        layoutOfPopup.setOrientation(LinearLayout.VERTICAL);
        layoutOfPopup.setBackgroundColor(getResources().getColor(R.color.white));

        layoutOfPopup.addView(clickSwitch);
        layoutOfPopup.addView(speedSwitch);
        layoutOfPopup.addView(descriptionSwitch);

        speedSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                high_speed = speedSwitch.isChecked();
                if (high_speed) {
                    t1.setSpeechRate(2.0f);
                } else {
                    t1.setSpeechRate(1.0f);
                }
                System.out.println("SWITCH: " + should_click);
            }
        });

        clickSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                should_click = clickSwitch.isChecked();
                System.out.println("SWITCH: " + should_click);
            }
        });

        descriptionSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                describes_fully = descriptionSwitch.isChecked();
                System.out.println("Description: " + should_click);
            }
        });

        popup.setContentView(layoutOfPopup);
    }


    private void displayPopupWindow(View anchorView) {
        controller.addPauseEvent();
        popup.showAtLocation(anchorView, 1, 0,0);
    }

}
