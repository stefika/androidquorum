package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioBarchart2Behavior extends SlideBehavior {

    public AudioBarchart2Behavior(SlideController sc) {
        super(R.drawable.barchart2, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        // bar 2 audio
        sa.speak("A bar chart titled Maine Voting Results. The horizontal axis is labeled Question, and has two sections labeled Vote in 2016 and Vote in 2020.  Each section contains two bars. The vertical axis is labeled Percentage, and ranges from 0 to 75 in increments of 25. All data are approximate. In the Vote in 2016 section, the first bar reaches 33 and the second bar reaches 65.  In the Vote in 2020 section, the first bar reaches 55 and the second bar reaches 45.");
    }
}