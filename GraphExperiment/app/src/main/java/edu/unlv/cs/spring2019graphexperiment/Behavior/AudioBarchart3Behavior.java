package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioBarchart3Behavior extends SlideBehavior {

    public AudioBarchart3Behavior(SlideController sc) {
        super(R.drawable.barchart3,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        sa.speak("A bar chart titled Breakfast Results. The horizontal axis is labeled Breakfast Item, and has two sections labeled Grain Products and Milk Products.  Each section contains two bars. The vertical axis is labeled Count, and ranges from 0 to 100 in increments of 25. All data are approximate. In the Grain Products section, the first bar reaches 72 and the second bar reaches 76.  In the Milk Products section, the first bar reaches 52 and the second bar reaches 50.");
    }
}