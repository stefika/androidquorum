package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioDiagram1Behavior extends SlideBehavior {

    public AudioDiagram1Behavior(SlideController sc) {
        super(R.drawable.diagram1,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        sa.speak("A mosquito. At the center is a body, divided into three sections. The top section is labeled Head, the center section is labeled Thorax, and the bottom section is labeled Abdomen.  A pair of short, thin structures extends from the head. One is labeled Antenna.  Between the antennae a long, thin structure labeled Proboscis extends straight out from the top of the head. On both the left and right sides of the Thorax are wings, one of which is labeled.  Near the bottom of the Thorax on each side is a tiny protrusion, one of which is labeled Halter. There are three pairs of legs. The first pair attaches to the top of the thorax near the head. One of these is labeled Foreleg. The second pair attaches to the bottom of the thorax near the abdomen. One of these is labeled Mid leg. The third pair attaches to the top of the abdomen. One of these is labeled Hindleg.");
    }
}
