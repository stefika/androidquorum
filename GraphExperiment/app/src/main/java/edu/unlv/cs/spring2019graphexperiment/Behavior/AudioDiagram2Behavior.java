package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioDiagram2Behavior extends SlideBehavior {

    public AudioDiagram2Behavior(SlideController sc) {
        super(R.drawable.science2, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        sa.speak("A bee. At the center is a body, divided into three sections. The top section is labeled Head, the center section is labeled Thorax, and the bottom section is labeled Abdomen.  A pair of long, curved structures labeled Antennae extends from the head.  On both the left and right sides of the Thorax is a pair of wings. One of the upper wings is labeled Fore Wing. One of the lower wings is labeled Hind Wing.  There are three pairs of legs. The first pair attaches to the middle of the thorax behind the wings. One of these is labeled Fore Leg. The second pair attaches to the bottom of the thorax near the abdomen. One of these is labeled Middle Leg. The third pair attaches to the top of the abdomen. One of these is labeled Hind Leg.");
    }

}
