package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioDiagram3Behavior extends SlideBehavior {

    public AudioDiagram3Behavior(SlideController sc) {
        super(R.drawable.diagram3, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        sa.speak("A ladybug. The top section is labeled Head. A pair of short, curved structures extends from the head, one of which is labeled Antenna. The head has two Eyes, one of which is labeled.  Just below the head is a slightly larger section labeled Pronotum. Below the pronotum is a very large, rounded section labeled Abdomen. Three pairs of legs are attached to this section.  One Leg is labeled. The abdomen is covered by an outer shell labeled Elytra. The elytra splits down the center.  One side of the elytra is moved aside to show a Wing underneath, which is labeled.");
    }
}