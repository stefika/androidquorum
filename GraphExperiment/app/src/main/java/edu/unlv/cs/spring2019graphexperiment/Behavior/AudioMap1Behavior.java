package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioMap1Behavior extends SlideBehavior {

    public AudioMap1Behavior(SlideController sc) {
        super(R.drawable.map1,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        sa.speak("A floorplan. The plan is rectangular, oriented horizontally. A small alcove or elevator extends out from the center top.  In the upper left corner is a row of three rooms. From left to right, they are labeled Patient Intake and Discharge, Men, and Women.  A narrow hallway runs horizontally beneath these rooms. To the right of Women, a wide hallway runs horizontally along the remainder of the top edge.  On the left side of the floorplan are two large rooms.  At the center left is Adult Emergency Room and at the lower left is Children’s Emergency Room.  A vertical hallway separates the left side of the floorplan from the right side.  Beneath the top wide hallway, the right side of the floorplan contains two columns of small rooms separated by another narrow vertical hallway.  The left column contains, from top to bottom: Room 1, Room 2, and Room 3.  The right column contains, from top to bottom: Room 4, Room 5, and Room 6. The right column also borders the right edge of the building."
        );
    }
}