package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioMap2Behavior extends SlideBehavior {

    public AudioMap2Behavior(SlideController sc) {
        super(R.drawable.floorplan2, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        sa.speak("A floorplan. The plan is rectangular, oriented horizontally. A small area labeled Exit extends from the top edge, slightly leftward of center.  A wide hallway runs horizontally across the left side of the top edge. The right end of the hallway is labeled Water Fountain.  The right side of the top edge has a row of three rooms. From left to right they are Men, Women, and Storage.   Beneath the top wide hallway, the left side of the floorplan contains two columns of small rooms separated by a narrow vertical hallway.  The first column contains, from top to bottom: Kay Jewelry, Starbucks, and Ace.  The second column contains, from top to bottom: Zales, Fresh Start, and Jared Jewelry. A wide vertical hallway separates the left side of the floorplan from the right side.  On the right side, beneath the restrooms and Storage, is a horizontal hallway that extends to the right edge.  Beneath this hallway are two large rooms labeled Dillards and Macy’s.  Macy’s is in the bottom right corner of the floorplan." );
    }
}