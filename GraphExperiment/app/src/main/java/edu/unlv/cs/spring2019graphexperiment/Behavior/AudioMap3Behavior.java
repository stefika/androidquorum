package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioMap3Behavior extends SlideBehavior {

    public AudioMap3Behavior(SlideController sc) {
        super(R.drawable.map3, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        sa.speak("A floorplan. The plan is rectangular except for the bottom left corner which is cut off. From top to bottom, the rooms along the left edge are Cafeteria, Office, and Entrance.  From left to right, the rooms along the top edge are Cafeteria, Art Room, Science Lab, and Gymnasium. The Cafeteria is a large, vertically-oriented room that extends over half the length of the building’s left edge. From left to right along the bottom edge are rooms Entrance, Social Studies and History, an open space labeled Water Fountains, and a room labeled Restrooms. From top to bottom, the right edge consists of Gymnasium, a vertical hallway, and Restrooms. In the upper center of the floorplan is a large Courtyard. The right side of the upper edge of Courtyard borders the left side of the bottom edge of Gymnasium. Under Courtyard, from left to right, is an open space labeled Bulletin Boards and two rooms: Mathematics Gr. 1-3, and Home Economics; these three areas span the length of the Courtyard.  A hallway runs around most of the interior of the floorplan as follows: Horizontally from Gymnasium, below Science Lab and Art Room, and ending in a turn at the Cafeteria. Vertically, to the right of Cafeteria, Office, and Entrance, and ending in a turn at Social Studies. Horizontally, above Social Studies, History, Water Fountains, and Restrooms.  Vertically along the right edge of the floorplan, stopping at Gymnasium.  The section of hallway to the right of Cafeteria is labeled Lockers 1-50. The section of hallway along the right edge of the floorplan is labeled Lockers 51-150.");
    }
}