package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioTrig1Behavior extends SlideBehavior{

    public AudioTrig1Behavior(SlideController sc) {
        super(R.drawable.trig1, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();

        sa.speak("A quadrilateral.  Clockwise from top left, the corners are labeled W, X, Y, and Z.  Side W X is labeled 23 cm. Side X Y is labeled 6 cm. Side W Z is labeled 16 cm. The angles inside corners Y and Z are right angles.");
    }
}
