package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class AudioTrig3Behavior extends SlideBehavior {

    public AudioTrig3Behavior(SlideController sc) {
        super(R.drawable.trig3,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        sa.speak("A quadrilateral. Clockwise from upper right, the sides are labeled ay, b, d, and see. The angle between sides see and ay is a right angle. The angle between sides b and d is a right angle. A central line labeled 20 feet is drawn, dividing the quadrilateral into two triangles. The central line begins at the corner between sides see and d, and ends at the corner between sides ay and b. The angle between side see and the central line is labeled ay and 15 degrees. The angle between side d and the central line is labeled B. The angle between side ay and the central line is labeled see. The angle between side b and the central line is labeled D.");
    }
}