package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class EmbossedBarchart2Behavior extends SlideBehavior {

    public EmbossedBarchart2Behavior(SlideController sc) {
        super(R.drawable.embossedbar2,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        sa.speak("embossed bargraph");
    }
}
