package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class EmbossedDiagram3Behavior extends SlideBehavior {

    public EmbossedDiagram3Behavior(SlideController sc) {
        super(R.drawable.embosseddia3,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //TODO: vibration handling

        sa.speak("haptic diagram 3");
    }
}
