package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class EmbossedTrig3Behavior extends SlideBehavior {

    public EmbossedTrig3Behavior(SlideController sc) {
        super(R.drawable.embossedtrig3,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        sa.speak("trig 3 embossed");
    }
}

