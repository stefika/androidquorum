package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Bar1Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticBarchart1Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticBarchart1Behavior(SlideController sc) {
        super(R.drawable.barchart1, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }


    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                Bar1Polygons poly = new Bar1Polygons();
//                a.drawPolygon(poly.QUESTION1_LABEL);
//                a.drawPolygon(poly.QUESTION2_LABEL);
//                a.drawPolygon(poly.Q1_YES_BAR);
//                a.drawPolygon(poly.Q1_NO_BAR);
//                a.drawPolygon(poly.Q2_NO_BAR);
//                a.drawPolygon(poly.Q2_YES_BAR);
//                a.drawPolygon(poly.TITLE);
//                a.drawPolygon(poly.SEVENTYFIVE_LINE_LARGE);
//                a.drawPolygon(poly.FIFTY_LINE_LARGE);
//                a.drawPolygon(poly.TWENTYFIVE_LINE_LARGE);
//                a.drawPolygon(poly.ZERO_LINE_LARGE);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();

        int pixelColor = bitmap.getPixel(x,y);

        Bar1Polygons poly = new Bar1Polygons();

        if (poly.inPolygon(poly.QUESTION1_LABEL, x, y, width)) {
            String s = "Vote in 2016";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.QUESTION2_LABEL, x, y, width)) {
            String s = "Vote in 2020";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q1_NO_BAR, x, y, width)) {
            String s = "No, Vote in 2016";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q1_YES_BAR, x, y, width)) {
            String s = "Yes, Vote in 2016";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q2_NO_BAR, x, y, width)) {
            String s = "No, Vote in 2020";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q2_YES_BAR, x, y, width)) {
            String s = "Yes, Vote in 2020";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s  = "0 percent";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s  = "25 percent";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s  = "50 percent";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s  = "75 percent";
            sc.reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.ZERO, x, y, width)) {
//                    String s  = "0 percent";
//                    reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.TWENTYFIVE, x, y, width)) {
//                    String s  = "25 percent";
//                    reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.FIFTY, x, y, width)) {
//                    String s  = "50 percent";
//                    reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.SEVENTYFIVE, x, y, width)) {
//                    String s  = "75 percent";
//                    reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.HUNDRED, x, y, width)) {
//                    String s  = "100 percent";
//                    reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s  = "Nevada Voting Results";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.AXIS_Y, x, y, width)) {
            String s  = "Percentage";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        float yf = y;
        if (y - 158 >= 0 ) {
            float ypos = yf - 158.0f;
            float zeroline = 1130.0f - 158.0f;
            int percent = Math.round((1- ypos /  zeroline) * 75);
            sc.reactSpeakYPosition(x, y, sa, "" + percent + " percent");
        }
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
      //  sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();

        int pixelColor = bitmap.getPixel(x, y);

        Bar1Polygons poly = new Bar1Polygons();

        if (poly.inPolygon(poly.QUESTION1_LABEL, x, y, width)) {
            String s = "Vote in 2016";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.QUESTION2_LABEL, x, y, width)) {
            String s = "Vote in 2020";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Q1_NO_BAR, x, y, width)) {
            String sAdd = "in Bar No, Vote in 2016";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "No, Vote in 2016";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q1_YES_BAR, x, y, width)) {
            String sAdd = "in Bar Yes, Vote in 2016";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "Yes, Vote in 2016";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q2_NO_BAR, x, y, width)) {
            String sAdd = "in Bar No, Vote in 2020";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "No, Vote in 2020";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q2_YES_BAR, x, y, width)) {
            String sAdd = "in Bar Yes, Vote in 2020";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "Yes, Vote in 2020";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "0 percent";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "25 percent";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "50 percent";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "75 percent";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
//                } else if (poly.inPolygon(poly.ZERO, x, y, width)) {
//                    String s = "0 percent";
//                    reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
//                } else if (poly.inPolygon(poly.TWENTYFIVE, x, y, width)) {
//                    String s = "25 percent";
//                    reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
//                } else if (poly.inPolygon(poly.FIFTY, x, y, width)) {
//                    String s = "50 percent";
//                    reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
//                } else if (poly.inPolygon(poly.SEVENTYFIVE, x, y, width)) {
//                    String s = "75 percent";
//                    reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
//                } else if (poly.inPolygon(poly.HUNDRED, x, y, width)) {
//                    String s = "100 percent";
//                    reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s = "Nevada Voting Results";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.AXIS_Y, x, y, width)) {
            String s  = "Percentage";
            sc.reactAreaThenLineBargraphNoRepeat(x,y,bitmap,sa,action,s);
        } else {
            sc.reactAreaThenLineBargraphNoSpeak(x, y, bitmap, sa, action);
        }
    }

    private boolean handleGrids(String sAdd, int x, int y, int width, Bar1Polygons poly, SlideActivity sa, String action) {
        if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "0 percent";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "25 percent";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "50 percent";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "75 percent";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        }
        return false;
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Nevada Voting Results Barchart, axis intersection on the bottom left side");
        floorplan.add(topview);

        return floorplan;
    }
}