package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Bar3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticBarchart3Behavior extends SlideBehaviorPolygon implements ListNavigation{

    public HapticBarchart3Behavior(SlideController sc) {
        super(R.drawable.barchart3,sc);
    }


    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Bar3Polygons.QUESTION1_LABEL);
//                a.drawPolygon(Bar3Polygons.QUESTION2_LABEL);
//
//                a.drawPolygon(Bar3Polygons.HUNDRED_LINE_LARGE);
//                a.drawPolygon(Bar3Polygons.SEVENTYFIVE_LINE_LARGE);
//                a.drawPolygon(Bar3Polygons.FIFTY_LINE_LARGE);
//                a.drawPolygon(Bar3Polygons.TWENTYFIVE_LINE_LARGE);
//                a.drawPolygon(Bar3Polygons.ZERO_LINE_LARGE);
//
//                a.drawPolygon(Bar3Polygons.ZERO);
//                a.drawPolygon(Bar3Polygons.TWENTYFIVE);
//                a.drawPolygon(Bar3Polygons.Y_AXIS);
//                a.drawPolygon(Bar3Polygons.X_AXIS);
//                a.drawPolygon(Bar3Polygons.SEVENTYFIVE);
//                a.drawPolygon(Bar3Polygons.HUNDRED);
//
//                a.drawPolygon(Bar3Polygons.TITLE);
//
//                a.drawPolygon(Bar3Polygons.Q1_BOY_FULL_BAR);
//                a.drawPolygon(Bar3Polygons.Q2_BOY_FULL_BAR);
//                a.drawPolygon(Bar3Polygons.Q1_GIRL_FULL_BAR);
//                a.drawPolygon(Bar3Polygons.Q2_GIRL_FULL_BAR);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        Bar3Polygons poly = new Bar3Polygons();

        if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 25";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HUNDRED_LINE_LARGE, x, y, width)) {
            String s = "count 100";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.QUESTION1_LABEL, x, y, width)) {
            String s = "Grain products";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.QUESTION2_LABEL, x, y, width)) {
            String s = "Milk products";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q1_BOY_FULL_BAR, x, y, width)) {
            String s = "Boys, Grain Products";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q1_GIRL_FULL_BAR, x, y, width)) {
            String s = "Girls, Grain Products";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q2_BOY_FULL_BAR, x, y, width)) {
            String s = "Boys, Milk Products";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q2_GIRL_FULL_BAR, x, y, width)) {
            String s = "Girls, Milk Products";
            sc.reactTapReadAgain(x, y, sa, s);
//                } else if (poly.inPolygon(poly.ZERO, x, y, width)) {
//                    String s = "count 0";
//                    reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE, x, y, width)) {
            String s = "count 25";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Y_AXIS, x, y, width)) {
            String s = "Count 50";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE, x, y, width)) {
            String s = "count 75";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HUNDRED, x, y, width)) {
            String s = "count 100";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s = "Breakfast Results";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.X_AXIS, x, y, width)) {
            String s = "Breakfast Item";
            sc.reactTapReadAgain(x, y, sa, s);
        }

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        float yf = y;
        if (y - 117 >=0 ) {
            float ypos =  yf - 117.0f;
            float zeroline = 1397.0f - 117.0f;
            System.out.println("Y: " + yf + ", ypos: " + ypos + ", zeroline: " + zeroline);
            int percent = Math.round((1- ypos /  zeroline) * 100);
            System.out.println("percent: " + percent);
            sc.reactSpeakYPosition(x,y, sa,"count " + percent);
        }

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Bar3Polygons poly = new Bar3Polygons();

        if (poly.inPolygon(poly.QUESTION1_LABEL, x, y, width)) {
            String s = "Grain products";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.QUESTION2_LABEL, x, y, width)) {
            String s = "Milk products";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Q1_BOY_FULL_BAR, x, y, width)) {
            String sAdd = "in Bar Boys grain product";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "Boys, Grain Products";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q1_GIRL_FULL_BAR, x, y, width)) {
            String sAdd = "in Bar Girls grain product";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "Girls, Grain Products";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q2_BOY_FULL_BAR, x, y, width)) {
            String sAdd = "in Bar Boys Milk product";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "Boys, Milk Products";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q2_GIRL_FULL_BAR, x, y, width)) {
            String sAdd = "in Bar Girls Milk product";
            boolean isInsideGrid = handleGrids(sAdd, x, y, width, poly, sa, action);
            if (!isInsideGrid) {
                String s = "Girls, Milk Products";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            System.out.println("testsetetsetests");
            String s = "count 25";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.HUNDRED_LINE_LARGE, x, y, width)) {
            String s = "count 100";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s = "Breakfast Results";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Y_AXIS, x, y, width)) {
            String s = "Count";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.X_AXIS, x, y, width)) {
            String s = "Breakfast Item";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineBargraphNoSpeak(x,y,bitmap,sa,action);
        }
    }

    private boolean handleGrids(String sAdd, int x, int y, int width, Bar3Polygons poly, SlideActivity sa, String action) {
        if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 25";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.HUNDRED_LINE_LARGE, x, y, width)) {
            String s = "count 100";
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        }
        return false;
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Breakfast Results Barchart, axis intersection on the bottom left side");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
      //  sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }

}
