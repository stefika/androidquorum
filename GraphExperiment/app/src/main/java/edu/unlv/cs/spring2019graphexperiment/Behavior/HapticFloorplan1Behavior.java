package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan1Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan2Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticFloorplan1Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticFloorplan1Behavior(SlideController sc) {
        super(R.drawable.floorplan1, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Floorplan2Polygons poly = new Floorplan2Polygons();
//        a.drawPolygon(poly.EXIT);
//        a.drawPolygon(poly.KAY_JEWELRY);
//        a.drawPolygon(poly.STARBUCKS);
//        a.drawPolygon(poly.ACE);
//        a.drawPolygon(poly.ZALES);
//        a.drawPolygon(poly.FRESH_START);
//        a.drawPolygon(poly.JARED_JEWELRY);
//        a.drawPolygon(poly.WATER_FOUNTAIN);
//        a.drawPolygon(poly.MEN);
//        a.drawPolygon(poly.WOMEN);
//        a.drawPolygon(poly.STORAGE);
//        a.drawPolygon(poly.DILLARDS);
//        a.drawPolygon(poly.MACYS);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan1Polygons poly = new Floorplan1Polygons();

        if (poly.inPolygon(poly.EXIT, x, y, width)) {
            String s = "Entrance, top";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.PATIENT_INTAKE, x, y, width)) {
            String s = "Patient Intake and Discharge, upper left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.MEN, x, y, width)) {
            String s = "Men's Room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WOMEN, x, y, width)) {
            String s = "Women's Room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ADULT_EMERGENCY, x, y, width)) {
            String s = "Adult Emergency Room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.CHILDRENS_EMERGENCY, x, y, width)) {
            String s = "Children's Emergency Room, lower left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_1, x, y, width)) {
            String s = "Room 1";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_2, x, y, width)) {
            String s = "Room 2";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_3, x, y, width)) {
            String s = "Room 3";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_4, x, y, width)) {
            String s = "Room 4";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_5, x, y, width)) {
            String s = "Room 5";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_6, x, y, width)) {
            String s = "Room 6, lower right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan1Polygons poly = new Floorplan1Polygons();
        if (poly.inPolygon(poly.OUTSIDE_CONTOUR, x, y, width)) {
            sa.playClick();
        }
        if (poly.inPolygon(poly.EXIT, x, y, width)) {
            String s = "Entrance, top";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.PATIENT_INTAKE, x, y, width)) {
            String s = "Patient Intake and Discharge, upper left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MEN, x, y, width)) {
            String s = "Men's Room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WOMEN, x, y, width)) {
            String s = "Women's Room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ADULT_EMERGENCY, x, y, width)) {
            String s = "Adult Emergency Room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.CHILDRENS_EMERGENCY, x, y, width)) {
            String s = "Children's Emergency Room, lower left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_1, x, y, width)) {
            String s = "Room 1";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_2, x, y, width)) {
            String s = "Room 2";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_3, x, y, width)) {
            String s = "Room 3";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_4, x, y, width)) {
            String s = "Room 4";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_5, x, y, width)) {
            String s = "Room 5";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_6, x, y, width)) {
            String s = "Room 6, lower right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineFloorPlanNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Floor Plan of a Hospital, entrance at the top right");
        floorplan.add(topview);



        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
       // sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}