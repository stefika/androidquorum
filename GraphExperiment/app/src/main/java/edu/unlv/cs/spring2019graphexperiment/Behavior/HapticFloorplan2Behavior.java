package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan2Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticFloorplan2Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticFloorplan2Behavior(SlideController sc) {
        super(R.drawable.floorplan2, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Floorplan2Polygons poly = new Floorplan2Polygons();
//        a.drawPolygon(poly.EXIT);
//        a.drawPolygon(poly.KAY_JEWELRY);
//        a.drawPolygon(poly.STARBUCKS);
//        a.drawPolygon(poly.ACE);
//        a.drawPolygon(poly.ZALES);
//        a.drawPolygon(poly.FRESH_START);
//        a.drawPolygon(poly.JARED_JEWELRY);
//        a.drawPolygon(poly.WATER_FOUNTAIN);
//        a.drawPolygon(poly.MEN);
//        a.drawPolygon(poly.WOMEN);
//        a.drawPolygon(poly.STORAGE);
//        a.drawPolygon(poly.DILLARDS);
//        a.drawPolygon(poly.MACYS);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan2Polygons poly = new Floorplan2Polygons();

        if (poly.inPolygon(poly.EXIT, x, y, width)) {
            String s = "Entrance, top left";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.KAY_JEWELRY, x, y, width)) {
            String s = "Kay Jewelry, top left";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.STARBUCKS, x, y, width)) {
            String s = "Starbucks";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ACE, x, y, width)) {
            String s = "Ace, bottom left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.JARED_JEWELRY, x, y, width)) {
            String s = "Jared Jewelry";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.FRESH_START, x, y, width)) {
            String s = "Fresh Start";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ZALES, x, y, width)) {
            String s = "SALES";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAIN, x, y, width)) {
            String s = "Water Fountain";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.MEN, x, y, width)) {
            String s = "Men's room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WOMEN, x, y, width)) {
            String s = "Women's room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.STORAGE, x, y, width)) {
            String s = "Storage, upper right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.DILLARDS, x, y, width)) {
            String s = "Dillards";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.MACYS, x, y, width)) {
            String s = "Macy's, bottom right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan2Polygons poly = new Floorplan2Polygons();

        if (poly.inPolygon(poly.OUTSIDE_CONTOUR, x, y, width)) {
            sa.playClick();
        }
        if (poly.inPolygon(poly.EXIT, x, y, width)) {
            String s = "Entrance, top left";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.KAY_JEWELRY, x, y, width)) {
            String s = "Kay Jewelry, top left";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.STARBUCKS, x, y, width)) {
            String s = "Starbucks";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ACE, x, y, width)) {
            String s = "Ace, bottom left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.JARED_JEWELRY, x, y, width)) {
            String s = "Jared Jewelry";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.FRESH_START, x, y, width)) {
            String s = "Fresh Start";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ZALES, x, y, width)) {
            String s = "SALES";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAIN, x, y, width)) {
            String s = "Water Fountain";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MEN, x, y, width)) {
            String s = "Men's room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WOMEN, x, y, width)) {
            String s = "Women's room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.STORAGE, x, y, width)) {
            String s = "Storage, upper right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.DILLARDS, x, y, width)) {
            String s = "Dillards";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MACYS, x, y, width)) {
            String s = "Macy's, bottom right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineFloorPlanNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Floor Plan of a Mall, entrance at the top left");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}