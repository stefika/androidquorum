package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan2Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticFloorplan3Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticFloorplan3Behavior(SlideController sc) {
        super(R.drawable.floorplan3, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Floorplan2Polygons poly = new Floorplan2Polygons();
//        a.drawPolygon(poly.EXIT);
//        a.drawPolygon(poly.KAY_JEWELRY);
//        a.drawPolygon(poly.STARBUCKS);
//        a.drawPolygon(poly.ACE);
//        a.drawPolygon(poly.ZALES);
//        a.drawPolygon(poly.FRESH_START);
//        a.drawPolygon(poly.JARED_JEWELRY);
//        a.drawPolygon(poly.WATER_FOUNTAIN);
//        a.drawPolygon(poly.MEN);
//        a.drawPolygon(poly.WOMEN);
//        a.drawPolygon(poly.STORAGE);
//        a.drawPolygon(poly.DILLARDS);
//        a.drawPolygon(poly.MACYS);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan3Polygons poly = new Floorplan3Polygons();

        if (poly.inPolygon(poly.CAFETERIA, x, y, width)) {
            String s = "Cafeteria, top left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.OFFICE, x, y, width)) {
            String s = "Office";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ENTRANCE, x, y, width)) {
            String s = "Entrance, bottom left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SOCIAL_STUDIES, x, y, width)) {
            String s = "Social Studies";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HISTORY, x, y, width)) {
            String s = "History";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAINS, x, y, width)) {
            String s = "Water Fountains";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.RESTROOMS, x, y, width)) {
            String s = "Restrooms, bottom right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ART_ROOM, x, y, width)) {
            String s = "Art room";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SCIENCE_LAB, x, y, width)) {
            String s = "Science lab";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.GYMNASIUM, x, y, width)) {
            String s = "Gymnasium, top right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.COURTYARD, x, y, width)) {
            String s = "Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BULLETIN_BOARD, x, y, width)) {
            String s = "Bulleting Board";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.MATHEMATIC_GROUP, x, y, width)) {
            String s = "Mathematics group 1 to 3";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HOME_ECONOMICS, x, y, width)) {
            String s = "Home economics";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.LOCKERS_1_50, x, y, width)) {
            String s = "Lockers 1 to 50";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.LOCKERS_51_150, x, y, width)) {
            String s = "Lockers 51 to 150";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan3Polygons poly = new Floorplan3Polygons();
        if (poly.inPolygon(poly.OUTER_CONTOUR, x, y, width)) {
            sa.playClick();
        }
        if (poly.inPolygon(poly.CAFETERIA, x, y, width)) {
            String s = "Cafeteria, top left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.OFFICE, x, y, width)) {
            String s = "Office";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ENTRANCE, x, y, width)) {
            String s = "Entrance, bottom left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SOCIAL_STUDIES, x, y, width)) {
            String s = "Social Studies";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HISTORY, x, y, width)) {
            String s = "History";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAINS, x, y, width)) {
            String s = "Water Fountains";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.RESTROOMS, x, y, width)) {
            String s = "Restrooms, bottom right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ART_ROOM, x, y, width)) {
            String s = "Art room";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SCIENCE_LAB, x, y, width)) {
            String s = "Science lab";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.GYMNASIUM, x, y, width)) {
            String s = "Gymnasium, top right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.COURTYARD, x, y, width)) {
            String s = "Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BULLETIN_BOARD, x, y, width)) {
            String s = "Bulleting Board";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MATHEMATIC_GROUP, x, y, width)) {
            String s = "Mathematics group 1 to 3";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HOME_ECONOMICS, x, y, width)) {
            String s = "Home economics";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.LOCKERS_1_50, x, y, width)) {
            String s = "Lockers 1 to 50";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.LOCKERS_51_150, x, y, width)) {
            String s = "Lockers 51 to 150";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineFloorPlanNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Floor Plan of a School, Entrance at the bottom left corner");
        floorplan.add(topview);



        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}