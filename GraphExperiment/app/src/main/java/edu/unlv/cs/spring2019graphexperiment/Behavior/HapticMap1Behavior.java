package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticMap1Behavior extends SlideBehaviorPolygon {

    public HapticMap1Behavior(SlideController sc) {
        super(R.drawable.map1, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Floorplan3Polygons poly = new Floorplan3Polygons();
//        a.drawPolygon(poly.CAFETERIA);
//        a.drawPolygon(poly.OFFICE);
//        a.drawPolygon(poly.ENTRANCE);
//        a.drawPolygon(poly.GYMNASIUM);
//        a.drawPolygon(poly.COURTYARD);
//        a.drawPolygon(poly.ART_ROOM);
//        a.drawPolygon(poly.SCIENCE_LAB);
//        a.drawPolygon(poly.MATHEMATIC_GROUP);
//        a.drawPolygon(poly.HOME_ECONOMICS);
//        a.drawPolygon(poly.SOCIAL_STUDIES);
//        a.drawPolygon(poly.HISTORY);
//        a.drawPolygon(poly.WATER_FOUNTAINS);
//        a.drawPolygon(poly.RESTROOMS);
//        a.drawPolygon(poly.BULLETIN_BOARD);
//        a.drawPolygon(poly.LOCKERS_1_50);
//        a.drawPolygon(poly.LOCKERS_51_150);
//        a.drawPolygon(poly.HALLWAY);
//        a.drawPolygon(poly.HALLWAY2);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan3Polygons poly = new Floorplan3Polygons();

        if (poly.inPolygon(poly.CAFETERIA, x, y, width)) {
            String s = "Cafeteria, east of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.OFFICE, x, y, width)) {
            String s = "Office, south of Cafeteria";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ENTRANCE, x, y, width)) {
            String s = "Entrance, south-east corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SOCIAL_STUDIES, x, y, width)) {
            String s = "Social Studies, west of Entrance";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HISTORY, x, y, width)) {
            String s = "History, west of Entrance";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAINS, x, y, width)) {
            String s = "Water Fountains, between History and Restrooms";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.RESTROOMS, x, y, width)) {
            String s = "Restrooms, south of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ART_ROOM, x, y, width)) {
            String s = "Art room, west of Cafeteria";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SCIENCE_LAB, x, y, width)) {
            String s = "Science lab, east of Gymnasium";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.GYMNASIUM, x, y, width)) {
            String s = "Gymnasium, north-west corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.COURTYARD, x, y, width)) {
            String s = "Courtyard, south-east of Gymnasium";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BULLETIN_BOARD, x, y, width)) {
            String s = "Bulleting Board, south of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.MATHEMATIC_GROUP, x, y, width)) {
            String s = "Mathematics group 1 to 3, south of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HOME_ECONOMICS, x, y, width)) {
            String s = "Home economics, south of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.LOCKERS_1_50, x, y, width)) {
            String s = "Lockers 1 to 50, west of Cafeteria";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.LOCKERS_51_150, x, y, width)) {
            String s = "Lockers 51 to 150, west of Courtyard";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Floorplan3Polygons poly = new Floorplan3Polygons();

        if (poly.inPolygon(poly.CAFETERIA, x, y, width)) {
            String s = "Cafeteria, east of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.OFFICE, x, y, width)) {
            String s = "Office, south of Cafeteria";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ENTRANCE, x, y, width)) {
            String s = "Entrance, south-east corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SOCIAL_STUDIES, x, y, width)) {
            String s = "Social Studies, west of Entrance";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HISTORY, x, y, width)) {
            String s = "History, west of Entrance";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WATER_FOUNTAINS, x, y, width)) {
            String s = "Water Fountains, between History and Restrooms";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.RESTROOMS, x, y, width)) {
            String s = "Restrooms, south of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ART_ROOM, x, y, width)) {
            String s = "Art room, west of Cafeteria";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SCIENCE_LAB, x, y, width)) {
            String s = "Science lab, east of Gymnasium";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.GYMNASIUM, x, y, width)) {
            String s = "Gymnasium, north-west corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.COURTYARD, x, y, width)) {
            String s = "Courtyard, south-east of Gymnasium";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BULLETIN_BOARD, x, y, width)) {
            String s = "Bulleting Board, south of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MATHEMATIC_GROUP, x, y, width)) {
            String s = "Mathematics group 1 to 3, south of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HOME_ECONOMICS, x, y, width)) {
            String s = "Home economics, south of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.LOCKERS_1_50, x, y, width)) {
            String s = "Lockers 1 to 50, west of Cafeteria";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.LOCKERS_51_150, x, y, width)) {
            String s = "Lockers 51 to 150, west of Courtyard";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineFloorPlanNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
       // sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}