package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Map2Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticMap2Behavior extends SlideBehaviorPolygon {

    public HapticMap2Behavior(SlideController sc) {
        super(R.drawable.map2, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {

    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        Map2Polygons poly = new Map2Polygons();

        if (sa.getTouchManagement().circleContains(-16711681,x,y,bitmap)) {
            String s = "Powder River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (sa.getTouchManagement().circleContains(-65281,x,y,bitmap)) {
            String s = "North Platte River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (sa.getTouchManagement().circleContains(-16733795,x,y,bitmap)) {
            String s = "Green River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (sa.getTouchManagement().circleContains(-256,x,y,bitmap)) {
            String s = "Bighorn River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (sa.getTouchManagement().circleContains(-16711936,x,y,bitmap)) {
            String s = "Snake River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (sa.getTouchManagement().circleContains(-16776961,x,y,bitmap)) {
            String s = "Yellowstone River";
            sc.reactAreaThenLineWyomingRiver(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.YELLOWSTONE_NATL_PARK,x,y,width)) {
            String s = "Yellowstone National Park";
            sc.reactAreaThenLineWyomingNationalPark(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.GRAND_TETON_NATIONAL_PARK,x,y,width)) {
            String s = "Grand Teton National Park";
            sc.reactAreaThenLineWyomingNationalPark(x, y, bitmap, sa, action, s);
        }  else if (sa.getTouchManagement().circleContains(-65536,x,y,bitmap)) {
            if (poly.inPolygon(poly.ROCK_SPRINGS, x, y, width)) {
                String s = "Rock Springs";
                sc.reactAreaThenLineWyomingCity(x, y, bitmap, sa, action, s);
            } else if (poly.inPolygon(poly.CASPER, x, y, width)) {
                String s = "Casper";
                sc.reactAreaThenLineWyomingCity(x, y, bitmap, sa, action, s);
            } else if (poly.inPolygon(poly.SHERIDAN, x, y, width)) {
                String s = "Sheridan";
                sc.reactAreaThenLineWyomingCity(x, y, bitmap, sa, action, s);
            }
        } else if (sa.getTouchManagement().circleContains(-891614,x,y,bitmap)) {
            if (poly.inPolygon(poly.CHEYENNE, x, y ,width)) {
                String s = "Cheyenne";
                sc.reactAreaThenLineWyomingCity(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            sa.speak("Wyoming");
        } else {
            sc.reactAreaThenLineWyomingNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}