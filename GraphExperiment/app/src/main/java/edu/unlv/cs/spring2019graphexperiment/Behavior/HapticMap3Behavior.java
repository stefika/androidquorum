package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Map3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticMap3Behavior extends SlideBehaviorPolygon {

    public HapticMap3Behavior(SlideController sc) {
        super(R.drawable.map3,sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Map3Polygons poly = new Map3Polygons();

        if (sa.getTouchManagement().circleContains(-65536,x,y,bitmap)) {
            String s = "North America";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-16711936,x,y,bitmap)) {
            String s = "South America";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-256,x,y,bitmap)) {
            String s = "Africa";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-65281,x,y,bitmap)) {
            String s = "Europe";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-16776961,x,y,bitmap)) {
            String s = "Asia";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-10080879,x,y,bitmap)) {
            String s = "Australia";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else if(sa.getTouchManagement().circleContains(-891614,x,y,bitmap)) {
            String s = "Antartica";
            sc.reactAreaThenLineMap(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineMapNoSpeak(x, y, bitmap, sa, action);
        }
        // vibration floor 3
    }

    public void drawPolygons(SlideActivity a) {

    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}