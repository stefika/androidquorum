package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram4Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticScience1Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticScience1Behavior(SlideController sc) {
        super(R.drawable.science1, sc);
    }

    private Node gnat;
    private Node head;
    private Node thorax;
    private Node abdomen;

    final static int HEAD_COLOR = -16771073;
    final static int ANTENNAE_COLOR = -65282;
    final static int PROBOSCIS_COLOR = -10722925;
    final static int EYES_COLOR = -15064470;
    final static int THORAX_COLOR = -16711930;
    final static int ABDOMEN_COLOR = -65536;
    final static int WINGS_COLOR = -16434682;
    final static int FORE_LEGS_COLOR = -10242673;
    final static int MIDDLE_LEGS_COLOR = -1642456;
    final static int HIND_LEGS_COLOR = -5412688;

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Diagram2Polygons.LEGS);
//                a.drawPolygon(Diagram2Polygons.WINGS);
//                a.drawPolygon(Diagram2Polygons.THORAX);
//                a.drawPolygon(Diagram2Polygons.ABDOMEN);
//                a.drawPolygon(Diagram2Polygons.HEAD);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram4Polygons wp = new Diagram4Polygons();
        if (!sc.isLocked()) {
//            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
//                String s = "Head";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
//                String s = "Thorax";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
//                String s = "Abdomen";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
//                String s = "Antennae";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
//                String s = "Eyes";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Fore Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(WINGS_COLOR, x, y, bitmap)){
//                String s = "Wings";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(PROBOSCIS_COLOR, x, y, bitmap)){
//                String s = "Proboscis";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Middle Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
//                String s = "Hind Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            }
        } else {
            sc.lockedVibration(x, y, bitmap, sa, action);
            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
                String s = "Head";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
                String s = "Thorax";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
                String s = "Abdomen";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
                String s = "Antennae";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
                String s = "Eyes";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
                String s = "Fore Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(PROBOSCIS_COLOR, x, y, bitmap)){
                String s = "Proboscis";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(WINGS_COLOR, x, y, bitmap)){
                String s = "Wings";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
                String s = "Middle Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
                String s = "Hind Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            }
        }
    }


    /*
        Create overview list for navigation
     */
    @Override
    public List<Node> getOverviewStructure() {
        List<Node> flyList = new ArrayList<>();

        Node topview = new Node("Top view of a Fly, no items, Head at the top middle");


        Node head = new Node("Head, three items", HEAD_COLOR);
        Node antennae = new Node("Antennae, one of three", ANTENNAE_COLOR);
        Node eyes = new Node("Eyes, two of three",EYES_COLOR);
        Node proboscis = new Node("Proboscis, three of three",PROBOSCIS_COLOR);
        head.addChild(antennae);
        head.addChild(eyes);
        head.addChild(proboscis);


        Node thorax = new Node("Thorax, three items", THORAX_COLOR);
        Node foreLegs = new Node("Fore legs, one of three", FORE_LEGS_COLOR);
        Node foreWings = new Node("Wings, two of three", WINGS_COLOR);
        Node middleLegs = new Node("Middle Legs, three of three", MIDDLE_LEGS_COLOR);
        thorax.addChild(foreLegs);
        thorax.addChild(foreWings);
        thorax.addChild(middleLegs);

        Node abdomen = new Node("Abdomen, one item", ABDOMEN_COLOR);
        Node hindLegs = new Node("Hind Legs, one of one", HIND_LEGS_COLOR);
        abdomen.addChild(hindLegs);

    //    Node endview = new Node("End of Top View");

        flyList.add(topview);
        flyList.add(head);
        flyList.add(thorax);
        flyList.add(abdomen);
       // beeList.add(endview);

        this.gnat = topview;
        this.head = head;
        this.thorax = thorax;
        this.abdomen = abdomen;


        return flyList;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
       // sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
        sc.lockNode(sa);
    }
}
