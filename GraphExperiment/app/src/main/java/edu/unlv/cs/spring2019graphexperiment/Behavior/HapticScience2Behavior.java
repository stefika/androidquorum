package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram4Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;
import edu.unlv.cs.spring2019graphexperiment.TreeNode;

public class HapticScience2Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticScience2Behavior(SlideController sc) {
        super(R.drawable.science2, sc);
    }

    private Node bee;
    private Node head;
    private Node thorax;
    private Node abdomen;

    final static int HEAD_COLOR = -11776;
    final static int ANTENNAE_COLOR = -589569;
    final static int EYES_COLOR = -65536;
    final static int THORAX_COLOR = -15235558;
    final static int FORE_LEGS_COLOR = -16711720;
    final static int FORE_WINGS_COLOR = -16773121;
    final static int HIND_WINGS_COLOR = -10598145;
    final static int MIDDLE_LEGS_COLOR = -16754357;
    final static int ABDOMEN_COLOR = -16434682;
    final static int HIND_LEGS_COLOR = -11365495;
    final static int STINGER_COLOR = -1507584;

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Diagram2Polygons.LEGS);
//                a.drawPolygon(Diagram2Polygons.WINGS);
//                a.drawPolygon(Diagram2Polygons.THORAX);
//                a.drawPolygon(Diagram2Polygons.ABDOMEN);
//                a.drawPolygon(Diagram2Polygons.HEAD);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram4Polygons wp = new Diagram4Polygons();
        if (!sc.isLocked()) {
//            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
//                String s = "Head";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
//                String s = "Thorax";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
//                String s = "Abdomen";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
//                String s = "Antennae";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
//                String s = "Eyes";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Fore Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(FORE_WINGS_COLOR, x, y, bitmap)){
//                String s = "Fore Wings";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(HIND_WINGS_COLOR, x, y, bitmap)){
//                String s = "Hind Wings";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Middle Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
//                String s = "Hind Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(STINGER_COLOR, x, y, bitmap)){
//                String s = "Stinger";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            }
        } else {
            sc.lockedVibration(x, y, bitmap, sa, action);
            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
                String s = "Head";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
                String s = "Thorax";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
                String s = "Abdomen";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
                String s = "Antennae";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
                String s = "Eyes";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
                String s = "Fore Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(FORE_WINGS_COLOR, x, y, bitmap)){
                String s = "Fore Wings";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(HIND_WINGS_COLOR, x, y, bitmap)){
                String s = "Hind Wings";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
                String s = "Middle Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
                String s = "Hind Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(STINGER_COLOR, x, y, bitmap)){
                String s = "Stinger";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            }
        }
    }


    /*
        Create overview list for navigation
     */
    @Override
    public List<Node> getOverviewStructure() {
        List<Node> beeList = new ArrayList<>();

        Node topview = new Node("Top view of a Bee, no items, Head at the top middle");


        Node head = new Node("Head, two items", HEAD_COLOR);
        Node antennae = new Node("Antennae, one of two", ANTENNAE_COLOR);
        Node eyes = new Node("Eyes, two of two",EYES_COLOR);
        head.addChild(antennae);
        head.addChild(eyes);


        Node thorax = new Node("Thorax, four items", THORAX_COLOR);
        Node foreLegs = new Node("Fore legs, one of four", FORE_LEGS_COLOR);
        Node foreWings = new Node("Fore Wings, two of four", FORE_WINGS_COLOR);
        Node hindWings = new Node("Hind Wings, three of four", HIND_WINGS_COLOR);
        Node middleLegs = new Node("Middle Legs, four of four", MIDDLE_LEGS_COLOR);
        thorax.addChild(foreLegs);
        thorax.addChild(foreWings);
        thorax.addChild(hindWings);
        thorax.addChild(middleLegs);

        Node abdomen = new Node("Abdomen, two items", ABDOMEN_COLOR);
        Node hindLegs = new Node("Hind Legs, one of two", HIND_LEGS_COLOR);
        Node stinger = new Node("Stinger, two of two", STINGER_COLOR);
        abdomen.addChild(hindLegs);
        abdomen.addChild(stinger);

    //    Node endview = new Node("End of Top View");

        beeList.add(topview);
        beeList.add(head);
        beeList.add(thorax);
        beeList.add(abdomen);
       // beeList.add(endview);

        this.bee = topview;
        this.head = head;
        this.thorax = thorax;
        this.abdomen = abdomen;


        return beeList;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
      //  sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
        sc.lockNode(sa);
    }

}
