package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram4Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticScience3Behavior extends SlideBehaviorPolygon implements ListNavigation {

    public HapticScience3Behavior(SlideController sc) {
        super(R.drawable.science3, sc);
    }

    private Node ladybug;
    private Node head;
    private Node thorax;
    private Node abdomen;

    final static int HEAD_COLOR = -47616;
    final static int ANTENNAE_COLOR = -9921638;
    final static int EYES_COLOR = -16711712;
    final static int PRONOTUM_COLOR = -12843984;
    final static int PRONOTUM_SPOT_COLOR = -6156157;
    final static int FORE_LEGS_COLOR = -16764523;
    final static int ELYTRA_COLOR = -1894633;
    final static int ELYTRA_SPOT_COLOR = -22291;
    final static int WINGS_COLOR = -16515328;
    final static int MIDDLE_LEGS_COLOR = -14286699;
    final static int ABDOMEN_COLOR = -11776;
    final static int HIND_LEGS_COLOR = -14729354;


    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Diagram2Polygons.LEGS);
//                a.drawPolygon(Diagram2Polygons.WINGS);
//                a.drawPolygon(Diagram2Polygons.THORAX);
//                a.drawPolygon(Diagram2Polygons.ABDOMEN);
//                a.drawPolygon(Diagram2Polygons.HEAD);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram4Polygons wp = new Diagram4Polygons();
        if (!sc.isLocked()) {
//            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
//                String s = "Head";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(PRONOTUM_COLOR, x, y, bitmap)) {
//                String s = "Pronotum";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(PRONOTUM_SPOT_COLOR, x, y, bitmap)) {
//                String s = "Pronotum";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
//                String s = "Abdomen";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
//                String s = "Antennae";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
//                String s = "Eyes";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Fore Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ELYTRA_COLOR, x, y, bitmap)){
//                String s = "Elytra";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ELYTRA_SPOT_COLOR, x, y, bitmap)){
//                String s = "Elytra";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(WINGS_COLOR, x, y, bitmap)){
//                String s = "Wing";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
//                String s = "Middle Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
//                String s = "Hind Legs";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            }
        } else {
            sc.lockedVibration(x, y, bitmap, sa, action);
            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
                String s = "Head";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(PRONOTUM_COLOR, x, y, bitmap)) {
                String s = "Pronotum";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(PRONOTUM_SPOT_COLOR, x, y, bitmap)) {
                String s = "Pronotum";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
                String s = "Abdomen";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
                String s = "Antennae";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(EYES_COLOR, x, y, bitmap)){
                String s = "Eyes";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(FORE_LEGS_COLOR, x, y, bitmap)){
                String s = "Fore Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ELYTRA_COLOR, x, y, bitmap)){
                String s = "Elytra";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ELYTRA_SPOT_COLOR, x, y, bitmap)){
                String s = "Elytra";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(WINGS_COLOR, x, y, bitmap)){
                String s = "Wing";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(MIDDLE_LEGS_COLOR, x, y, bitmap)){
                String s = "Middle Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(HIND_LEGS_COLOR, x, y, bitmap)){
                String s = "Hind Legs";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            }
        }
    }


    /*
        Create overview list for navigation
     */
    @Override
    public List<Node> getOverviewStructure() {
        List<Node> ladybugList = new ArrayList<>();

        Node topview = new Node("Top view of a Ladybug, no items, Head at the top middle");

        Node head = new Node("Head, two items", HEAD_COLOR);
        Node antennae = new Node("Antennae, one of two", ANTENNAE_COLOR);
        Node eyes = new Node("Eyes, two of two", EYES_COLOR);
        head.addChild(antennae);
        head.addChild(eyes);

        Node pronotum = new Node("Pronotum, no items", PRONOTUM_COLOR, PRONOTUM_SPOT_COLOR);


        Node abdomen = new Node("Abdomen, five items", ABDOMEN_COLOR);
        Node foreLegs = new Node("Fore legs, one of five", FORE_LEGS_COLOR);
        Node elytra = new Node("Elytra, two of five", ELYTRA_COLOR, ELYTRA_SPOT_COLOR);
        Node wings = new Node("Wings, three of five", WINGS_COLOR);
        Node middleLegs = new Node("Middle Legs, four of five", MIDDLE_LEGS_COLOR);
        Node hindLegs = new Node("Hind Legs, five of five", HIND_LEGS_COLOR);
        abdomen.addChild(foreLegs);
        abdomen.addChild(elytra);
        abdomen.addChild(wings);
        abdomen.addChild(middleLegs);
        abdomen.addChild(hindLegs);

        //    Node endview = new Node("End of Top View");

        ladybugList.add(topview);
        ladybugList.add(head);
        ladybugList.add(pronotum);
        ladybugList.add(abdomen);
        // beeList.add(endview);

        this.ladybug = topview;
        this.head = head;
        this.thorax = pronotum;
        this.abdomen = abdomen;


        return ladybugList;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
    //    sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
        sc.lockNode(sa);
    }
}
