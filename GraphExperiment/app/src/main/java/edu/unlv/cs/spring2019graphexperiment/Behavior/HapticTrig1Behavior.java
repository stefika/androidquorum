package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Trig1Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticTrig1Behavior extends SlideBehaviorPolygon implements ListNavigation{

    public HapticTrig1Behavior(SlideController sc) {
        super(R.drawable.trig1, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Trig2Polygons.XY);
//                a.drawPolygon(Trig2Polygons.YZ);
//                a.drawPolygon(Trig2Polygons.ZW);
//                a.drawPolygon(Trig2Polygons.WX);
//                a.drawPolygon(Trig2Polygons.XY);
//                a.drawPolygon(Trig2Polygons.W);
//                a.drawPolygon(Trig2Polygons.X);
//                a.drawPolygon(Trig2Polygons.Y);
//                a.drawPolygon(Trig2Polygons.Z);
//                a.drawPolygon(Trig2Polygons.Y_RIGHT_ANGLE);
//                a.drawPolygon(Trig2Polygons.Z_RIGHT_ANGLE);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Trig1Polygons poly = new Trig1Polygons();

        if (poly.inPolygon(poly.Y_RIGHT_ANGLE, x, y, width)) {
            String s = "Ninety degree angle";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Z_RIGHT_ANGLE, x, y, width)) {
            String s = "Ninety degree angle";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.XY, x, y, width)) {
            String s = "vertical, X, Y, 6 centimeters";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.YZ, x, y, width)) {
            String s = "horizontal, Y, Z";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ZW, x, y, width)) {
            String s = "vertical, Z, W, 16 centimeters";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.WX, x, y, width)) {
            String s = "diagonal, W, ex, 23 centimeters";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.W, x, y, width)) {
            String s = "vertex W";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.X, x, y, width)) {
            String s = "vertex X";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Y, x, y, width)) {
            String s = "vertex Y";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Z, x, y, width)) {
            String s = "vertex Z";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
//                float yf = y;
//                if (y - 112 >=0 ) {
//                    float ypos = yf - 286.0f;
//                    float zeroline = 1120.0f;
//                    int percent = Math.round((1- ypos /  zeroline) * 100);
//                    reactSpeakYPosition(x,y, sa, "" + percent + " percent");
//                }
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Trig1Polygons poly = new Trig1Polygons();

        if (poly.inPolygon(poly.Y_RIGHT_ANGLE, x, y, width)) {
            String s = "Ninety degree angle";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Z_RIGHT_ANGLE, x, y, width)) {
            String s = "Ninety degree angle";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.XY, x, y, width)) {
            String s = "vertical, X, Y, 6 centimeters";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.YZ, x, y, width)) {
            String s = "horizontal, Y, Z";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ZW, x, y, width)) {
            String s = "vertical, Z, W, 16 centimeters";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WX, x, y, width)) {
            String s = "diagonal, W, ex, 23 centimeters";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.W, x, y, width)) {
            String s = "vertex W";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.X, x, y, width)) {
            String s = "vertex X";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Y, x, y, width)) {
            String s = "vertex Y";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.Z, x, y, width)) {
            String s = "vertex Z";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.INNER, x, y, width)) {
            String s = "INNER";
            sc.reactInnerArea(x,y,bitmap,sa,action,s);
        } else {
            sc.reactAreaThenLineTrigNoArea(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Quadrilateral, Point W on top left");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}
