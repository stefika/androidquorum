package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Trig2Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticTrig2Behavior extends SlideBehaviorPolygon implements ListNavigation{

    public HapticTrig2Behavior(SlideController sc) {
        super(R.drawable.trig2, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Trig2Polygons.A);
//                a.drawPolygon(Trig2Polygons.B);
//                a.drawPolygon(Trig2Polygons.C);
//                a.drawPolygon(Trig2Polygons.A_LINE);
//                a.drawPolygon(Trig2Polygons.B_LINE);
//                a.drawPolygon(Trig2Polygons.C_LINE);
//                a.drawPolygon(Trig2Polygons.A_RIGHT_ANGLE);
//                a.drawPolygon(Trig2Polygons.B_ANGLE);
//                a.drawPolygon(Trig2Polygons.C_ANGLE);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x, y);

        System.out.println("Color: " + pixelColor);

        Trig2Polygons poly = new Trig2Polygons();

        if (poly.inPolygon(poly.A_LINE, x, y, width)) {
            String s = "horizontal ay, 4 and one tenth, inches";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.B_LINE, x, y, width)) {
            String s = "diagonal B";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.C_LINE, x, y, width)) {
            String s = "diagonal see";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.A, x, y, width)) {
            String s = "Vertex ay, angle ay, ninety degrees";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.B, x, y, width)) {
            String s = "Vertex B, angle B";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.C, x, y, width)) {
            String s = "Vertex see, angle see, twentynine degrees";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.A_RIGHT_ANGLE, x, y, width)) {
            String s = "Angle ay, ninety degrees, between line B and line see";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.B_ANGLE, x, y, width)) {
            String s = "Angle B, between line ay and line see";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.C_ANGLE, x, y, width)) {
            String s = "Angle see, twentynine degrees, between line ay and line B";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Trig2Polygons poly = new Trig2Polygons();

        if (poly.inPolygon(poly.A_LINE, x, y, width)) {
            String s = "horizontal ay, 4 and one tenth, inches";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.B_LINE, x, y, width)) {
            String s = "diagonal B";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.C_LINE, x, y, width)) {
            String s = "diagonal see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.A, x, y, width)) {
            String s = "Vertex ay, angle ay, ninety degrees";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.B, x, y, width)) {
            String s = "Vertex B, angle b";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.C, x, y, width)) {
            String s = "Vertex see, angle see, twentynine degrees";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.A_RIGHT_ANGLE, x, y, width)) {
            String s = "Angle ay, ninety degrees, between line B and line see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.B_ANGLE, x, y, width)) {
            String s = "Angle B, between line ay and line see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.C_ANGLE, x, y, width)) {
            String s = "Angle see, twentynine degrees, between line ay and line B";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.INNER, x, y, width)) {
            String s = "INNER";
            sc.reactInnerArea(x,y,bitmap,sa,action,s);
        } else {
            sc.reactAreaThenLineTrigNoArea(x,y,bitmap,sa,action);
        }

    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Triangle, right angle on the top right from the middle");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}
