package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;
import android.transition.Slide;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Trig3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class HapticTrig3Behavior extends SlideBehaviorPolygon implements ListNavigation{

    int tapCount = 0;

    public HapticTrig3Behavior(SlideController sc) {
        super(R.drawable.trig3,sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Trig3Polygons.SMALL_A);
//                a.drawPolygon(Trig3Polygons.SMALL_B);
//                a.drawPolygon(Trig3Polygons.SMALL_C);
//                a.drawPolygon(Trig3Polygons.SMALL_D);
//                a.drawPolygon(Trig3Polygons.BIG_A);
//                a.drawPolygon(Trig3Polygons.BIG_B);
//                a.drawPolygon(Trig3Polygons.BIG_C);
//                a.drawPolygon(Trig3Polygons.BIG_D);
//                a.drawPolygon(Trig3Polygons.SMALL_E);
//                a.drawPolygon(Trig3Polygons.A_C_VERTEX);
//                a.drawPolygon(Trig3Polygons.A_B_E_VERTEX);
//                a.drawPolygon(Trig3Polygons.C_D_E_VERTEX);
//                a.drawPolygon(Trig3Polygons.B_D_VERTEX);
//                a.drawPolygon(Trig3Polygons.NINETY_TOP);
//                a.drawPolygon(Trig3Polygons.NINETY_BOTTOM);
//                a.drawPolygon(Trig3Polygons.INNER);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);
        tapCount++;

        Trig3Polygons poly = new Trig3Polygons();
        if (poly.inPolygon(poly.SMALL_A, x, y, width)) {
            String s = "diagonal ay";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_B, x, y, width)) {
            String s = "diagonal B";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_E, x, y, width)) {
            String s = "horizontal E, 20 feet";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_C, x, y, width)) {
            String s = "diagonal see";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_D, x, y, width)) {
            String s = "diagonal D";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BIG_A, x, y, width)) {
            String s = "angle ay 15 degrees, between line see and line e";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BIG_B, x, y, width)) {
            String s = "angle B, between line d and line e";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BIG_C, x, y, width)) {
            String s = "angle see, between line ay and line e";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.BIG_D, x, y, width)) {
            String s = "angle D, between line b and line e";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.A_C_VERTEX, x, y, width)) {
            String s = "Vertex between line ay and line see";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.A_B_E_VERTEX, x, y, width)) {
            String s = "Vertex between line ay, line B, and line E";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.C_D_E_VERTEX, x, y, width)) {
            String s = "Vertex between line see, line D, and line E";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.B_D_VERTEX, x, y, width)) {
            String s = "Vertex between line B and line D";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.NINETY_TOP, x, y, width)) {
            String s = "angle, between line ay and line see, Ninety degrees";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.NINETY_BOTTOM, x, y, width)) {
            String s = "angle, between line b and line d, Ninety degrees";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("COLOR: " + pixelColor);

        Trig3Polygons poly = new Trig3Polygons();

        if (poly.inPolygon(poly.SMALL_A, x, y, width)) {
            String s = "diagonal ay";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_B, x, y, width)) {
            String s = "diagonal B";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_E, x, y, width)) {
            String s = "horizontal E, 20 feet";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_C, x, y, width)) {
            String s = "diagonal see";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_D, x, y, width)) {
            String s = "diagonal D";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BIG_A, x, y, width)) {
            String s = "angle ay 15 degrees, between line see and line e";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BIG_B, x, y, width)) {
            String s = "angle B, between line d and line e";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BIG_C, x, y, width)) {
            String s = "angle see, between line ay and line e";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BIG_D, x, y, width)) {
            String s = "angle D, between line b and line e";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.A_C_VERTEX, x, y, width)) {
            String s = "Vertex between line ay and line see";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.A_B_E_VERTEX, x, y, width)) {
            String s = "Vertex between line ay, line B, and line E";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.C_D_E_VERTEX, x, y, width)) {
            String s = "Vertex between line see, line D, and line E";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.B_D_VERTEX, x, y, width)) {
            String s = "Vertex between line B and line D";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.NINETY_TOP, x, y, width)) {
            String s = "angle, between line ay and line see, Ninety degrees";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.NINETY_BOTTOM, x, y, width)) {
            String s = "angle, between line b and line d, Ninety degrees";
            System.out.println("TOUCH: " + s + ", x: " + x + ", " + y);
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.INNER, x, y, width)) {
            String s = "INNER";
            sc.reactInnerArea(x,y,bitmap,sa,action,s);
        } else {
            sc.reactAreaThenLineTrigNoArea(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Quadrilateral, side a on right hand top side");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {//sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}