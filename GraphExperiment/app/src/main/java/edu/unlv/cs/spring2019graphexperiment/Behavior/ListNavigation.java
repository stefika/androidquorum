package edu.unlv.cs.spring2019graphexperiment.Behavior;

import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.TreeNode;

public interface ListNavigation {

    public List<Node> getOverviewStructure();

}
