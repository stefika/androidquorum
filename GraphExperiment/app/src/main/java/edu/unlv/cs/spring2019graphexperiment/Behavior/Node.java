package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.icu.text.SymbolTable;

import java.util.ArrayList;
import java.util.List;

public class Node {

    private String name = "";

    private List<Node> children;
    private int index = -1;
    private int color = -1;
    private int colortwo = 1;

    public Node(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    public Node(String name, int color) {
        this.name = name;
        this.children = new ArrayList<>();
        this.color = color;
    }

    public Node(String name, int color, int colortwo) {
        this.name = name;
        this.children = new ArrayList<>();
        this.color = color;
        this.colortwo = colortwo;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColorTwo() { return colortwo;}

    public void setColorTwo(int colortwo) {this.colortwo = colortwo;}

    public String getName() {
        return this.name;
    }

    public void addChild(Node n) {
        children.add(n);
    }

    public boolean hasChildren() {
        if (children != null && children.size() > 0)
            return true;
        return false;
    }

    public Node getFirstChild() {
        index = 0;
        if (children != null && children.size() >= index) {
            return this.children.get(index);
        }
        return null;
    }

    public Node getLastChild() {
        index = children.size()-1;
        if (children != null && children.size() >= 1) {
            return this.children.get(index);
        }
        return null;
    }

    public Node getNextChild() {
        System.out.println("next");
        if (index == -1) {
            return getFirstChild();
        } else {
            index++;
            if (children != null && children.size() > index) {
                return this.children.get(index);
            } else {
                return this.getFirstChild();
            }
        }
    }

    public Node getCurrentChild() {
        if (index >= 0) {
            return this.children.get(index);
        }
        return this;
    }

    public Node getPreviousChild() {
        System.out.println("previous index: " + index);
        if (index == -1) {
            return getFirstChild();
        } else {
            index--;
            if (children != null && index >= 0) {
                return this.children.get(index);
            } else {
                return this.getLastChild();
            }
        }
    }


    public void resetStatus() {
        this.index = -1;
    }

    public List<Node> getChildren() {
        return this.children;
    }

}
