package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.Event;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram2Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class OldAnt extends SlideBehaviorPolygon {

    public OldAnt(SlideController sc) {
        super(R.drawable.diagram2, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Diagram2Polygons.LEGS);
//                a.drawPolygon(Diagram2Polygons.WINGS);
//                a.drawPolygon(Diagram2Polygons.THORAX);
//                a.drawPolygon(Diagram2Polygons.ABDOMEN);
//                a.drawPolygon(Diagram2Polygons.HEAD);
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
      //  sc.lockNode(sa);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram2Polygons wp = new Diagram2Polygons();
        if (wp.inPolygon(wp.LEGS, x, y, width)) {
            String s = "Legs";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.WINGS, x, y, width)) {
            String s = "Wings";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.ANTENNA, x, y, width)) {
            String s= "Antenna";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.MANDIBLE, x, y, width)) {
            String s= "Mandible";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.THORAX, x, y, width)) {
            String s= "Thorax";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.ABDOMEN, x, y, width)) {
            String s= "Abdomen";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.HEAD, x, y, width)) {
            String s= "Head";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram2Polygons wp = new Diagram2Polygons();
        if (wp.inPolygon(wp.LEGS, x, y, width)) {
            String s = "Legs";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.WINGS, x, y, width)) {
            String s= "Wings";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.ANTENNA, x, y, width)) {
            String s= "Antenna";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.MANDIBLE, x, y, width)) {
            String s= "Mandible";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.THORAX, x, y, width)) {
            String s= "Thorax";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.ABDOMEN, x, y, width)) {
            String s= "Abdomen";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.HEAD, x, y, width)) {
            String s= "Head";
            sc.reactAnimalNoRepeat(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAnimalOffArea(x, y, bitmap, sa, action, Event.AREA_NONE);
        }
    }
}
