package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram1Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class OldBee extends SlideBehaviorPolygon {

    public OldBee(SlideController sc) {
        super(R.drawable.diagram1, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Diagram1Polygons wp = new Diagram1Polygons();
//        a.drawPolygon(wp.LEGS_BOTTOM);
//        a.drawPolygon(wp.LEGS_TOP);
//        a.drawPolygon(wp.ANTENNA_TOP);
//        a.drawPolygon(wp.ANTENNA_DOWN);
//        a.drawPolygon(wp.HEAD);
//                a.drawPolygon(wp.THORAX);
//        a.drawPolygon(wp.ABDOMEN);
//        a.drawPolygon(wp.MANDIBLE);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();

        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);
        Diagram1Polygons wp = new Diagram1Polygons();

        if (wp.inPolygon(wp.LEGS_TOP, x, y, width)) {
            String s = "Legs";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.LEGS_BOTTOM, x, y, width)) {
            String s = "Legs";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.THORAX, x, y, width)) {
            String s = "Thorax";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.HEAD, x, y, width)) {
            String s = "Head";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.MANDIBLE, x, y, width)) {
            String s = "Mandible";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.ANTENNA_TOP, x, y, width)) {
            String s = "Antenna";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.ANTENNA_DOWN, x, y, width)) {
            String s = "Antenna";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (wp.inPolygon(wp.ABDOMEN, x, y, width)) {
            String s = "Abdomen";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
       // sc.lockNode(sa);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();

        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);
        Diagram1Polygons wp = new Diagram1Polygons();

        if (wp.inPolygon(wp.LEGS_TOP, x, y, width)) {
            String s = "Legs";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.LEGS_BOTTOM, x, y, width)) {
            String s = "Legs";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.THORAX, x, y, width)) {
            String s = "Thorax";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.HEAD, x, y, width)) {
            String s = "Head";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.MANDIBLE, x, y, width)) {
            String s = "Mandible";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.ANTENNA_TOP, x, y, width)) {
            String s = "Antenna";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.ANTENNA_DOWN, x, y, width)) {
            String s = "Antenna";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (wp.inPolygon(wp.ABDOMEN, x, y, width)) {
            String s = "Abdomen";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineDiagramNoSpeak(x, y, bitmap, sa, action);
        }

    }
}