package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram3Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class OldDiagram3 extends SlideBehaviorPolygon {

    public OldDiagram3(SlideController sc) {
        super(R.drawable.diagram3,sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {

    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
     //   sc.lockNode(sa);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        Diagram3Polygons poly = new Diagram3Polygons();

        if (poly.inPolygon(poly.ANTENNAE, x, y, width)) {
            String s = "Antenna";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HEAD, x, y, width)) {
            String s = "Head";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.MANDIBLE, x, y, width)) {
            String s = "Mandible";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.LEGS, x, y, width)) {
            String s = "Legs";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.BODY, x, y, width)) {
            String s = "Body";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.WINGS, x, y, width)) {
            String s = "Wings";
            sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineDiagramNoSpeak(x,y,bitmap,sa,action);
        }

    }

}