package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;
import android.transition.Slide;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class QuestionBehavior extends SlideBehavior {

    public QuestionBehavior(SlideController sc) {
        super(R.drawable.questions,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
       // sa.speak("questions?");
    }
}