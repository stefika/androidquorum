package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;
import android.transition.Slide;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;

public abstract class SlideBehavior {

    int drawableId = -1;

    SlideController sc;

    public SlideBehavior(int drawableId, SlideController sc) {
        this.drawableId = drawableId;
        this.sc = sc;
    }

    abstract public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action);

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }
}
