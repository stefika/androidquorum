package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;

public abstract class SlideBehaviorPolygon extends SlideBehavior {

    public SlideBehaviorPolygon(int drawableId, SlideController sc) {
        super(drawableId,sc);
    }

    abstract public List<String> getOverviewList();

    abstract public String getOverviewString();

    abstract public void drawPolygons(SlideActivity a);

    abstract public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa);

    abstract public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa);

    abstract public void doubleFingerTapReaction(SlideActivity sa);

    abstract public void doubleFingerDoubleTapReaction(SlideActivity sa);
}
