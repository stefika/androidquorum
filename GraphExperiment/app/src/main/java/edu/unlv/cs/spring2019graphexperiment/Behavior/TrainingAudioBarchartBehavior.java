package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingAudioBarchartBehavior extends SlideBehavior {

    public TrainingAudioBarchartBehavior(SlideController sc) {
        super(R.drawable.training_bargraph, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        sa.speak("A bar chart titled Food Results. Along the horizontal axis are two bars labeled Vegetables and Fruit. The vertical axis is labeled Count, and ranges from 0 to 75 in increments of 25. All data are approximate. The bar for Vegetables reaches 57. The bar for Fruit reaches 45.");
    }
}