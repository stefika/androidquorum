package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingAudioDiagramBehavior extends SlideBehavior {

    public TrainingAudioDiagramBehavior(SlideController sc) {
        super(R.drawable.training_science,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        sa.speak("A butterfly. At the center is a body, divided into three sections. The top section is labeled Head and the bottom section is labeled Abdomen.  Two thin structures extend out from the head, labeled Antennae (Clubs on Ends). On both the left and right sides of the body is a set of two wings.  The arc of each antenna extends partway along the curve of the edge of the upper wings. An upper wing is labeled Forewing and a lower wing is labeled Hind wing.  The edge of the upper left wing where it slopes down toward the head and body is labeled Discall Cell.  The edge of the upper right wing where it slopes down toward the head and body is labeled Coastal Margin.  The point on the upper right wing farthest from the body is labeled Apex. The outer edge of the upper left wing, at a point closer to the body, is labeled Outer Margin.   The point on the lower left wing farthest from the body is labeled Inner Margin.");
    }
}
