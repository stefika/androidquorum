package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingAudioMapBehavior extends SlideBehavior {

    public TrainingAudioMapBehavior(SlideController sc) {
        super(R.drawable.training_floorplan,sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        sa.speak("A floorplan. The plan is rectangular, oriented horizontally. An Elevator extends outward on the lower left, from the left edge.  In the upper left corner are Stairs.  An open area extends from the Stairs downward to the lower left corner. Beginning to the right of the Stairs and extending across  the top from left to right are the following rooms: Room 112, Room 113, Room 114, and Room 115. A narrow hallway runs across the floorplan beneath the top rooms.  To the right of the Elevator and open area and spanning the remainder of the bottom of the floorplan is a large Common Room.  In the upper right corner of the Common Room is a smaller area labeled Reception. The reception area is across the narrow hallway from Room 114 and Room 115. "
        );
    }
}