package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingAudioTrigBehavior extends SlideBehavior{

    public TrainingAudioTrigBehavior(SlideController sc) {
        super(R.drawable.training_triangle, sc);
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();

        sa.speak("A triangle. Counter-clockwise from top, the angles are labeled as follows: ay, 100 degrees. B, 40 degrees. see, 40 degrees. The base of the triangle, opposite angle ay, is horizontal.");
    }
}
