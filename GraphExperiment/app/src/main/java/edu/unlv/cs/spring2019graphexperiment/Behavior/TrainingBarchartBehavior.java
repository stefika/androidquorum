package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.BarTrainingPolygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingBarchartBehavior extends SlideBehaviorPolygon implements ListNavigation {

    public TrainingBarchartBehavior(SlideController sc) {
        super(R.drawable.training_bargraph, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();

        overviewList.add("Barchart of Food Results");
        overviewList.add("Y axis count");
        overviewList.add("X axis food category");
        overviewList.add("bar vegetables");
        overviewList.add("bar fruit");

        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "Barchart of Food Results; two bars - Fruit and Vegetables; y axis count";
    }

    @Override
    public void drawPolygons(SlideActivity a) {

//                a.drawPolygon(BarTrainingPolygons.SEVENTYFIVE_LINE_LARGE);
//                a.drawPolygon(BarTrainingPolygons.FIFTY_LINE_LARGE);
//                a.drawPolygon(BarTrainingPolygons.TWENTYFIVE_LINE_LARGE);
//                a.drawPolygon(BarTrainingPolygons.ZERO_LINE_LARGE);
//
//                a.drawPolygon(BarTrainingPolygons.VEGETABLES);
//                a.drawPolygon(BarTrainingPolygons.AXIS_Y);
//                a.drawPolygon(BarTrainingPolygons.FRUIT);
//
//
//                a.drawPolygon(BarTrainingPolygons.TITLE);
//
//                a.drawPolygon(BarTrainingPolygons.Q1_NO_BAR);
//                a.drawPolygon(BarTrainingPolygons.Q1_YES_BAR);

    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        BarTrainingPolygons poly = new BarTrainingPolygons();

        if (poly.inPolygon(poly.Q1_YES_BAR, x, y, width)) {
            String s = "VEGETABLES";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.Q1_NO_BAR, x, y, width)) {
            String s = "FRUIT";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 25";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.FRUIT, x, y, width)) {
            String s = "Fruit";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s = "Food Results";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.AXIS_Y, x, y, width)) {
            String s = "Count";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.VEGETABLES, x, y, width)) {
            String s = "Vegetables";
            sc.reactTapReadAgain(x, y, sa, s);
        }

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        float yf = y;
        if (y - 161 >= 0 ) {
            float ypos = yf - 161.0f;
            float zeroline = 1175.0f - 161.0f;
            int percent = Math.round((1- ypos /  zeroline) * 75);
            sc.reactSpeakYPosition(x, y, sa, "count " + percent);
        }
    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        BarTrainingPolygons poly = new BarTrainingPolygons();


        if (poly.inPolygon(poly.Q1_YES_BAR, x, y, width)) {
            String sAdd = "in Bar VEGETABLES";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "VEGETABLES";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.Q1_NO_BAR, x, y, width)) {
            String sAdd = "in Bar FRUIT";
            if (!handleGrids(sAdd, x, y, width, poly, sa, action)) {
                String s = "FRUIT";
                sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
            }
        } else if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 25";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.FRUIT, x, y, width)) {
            String s = "Fruit";
            sc.reactAreaThenLineBargraphGridOffArea(x, y, sa, action, s);
        } else if (poly.inPolygon(poly.TITLE, x, y, width)) {
            String s = "Food Results";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.AXIS_Y, x, y, width)) {
            String s = "Count";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.VEGETABLES, x, y, width)) {
            String s = "VEGETABLES";
            sc.reactAreaThenLineBargraphNoRepeat(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineBargraphNoSpeak(x,y,bitmap,sa,action);
        }
    }

    private boolean handleGrids(String sAdd, int x, int y, int width, BarTrainingPolygons poly, SlideActivity sa, String action) {
        if (poly.inPolygon(poly.ZERO_LINE_LARGE, x, y, width)) {
            String s = "count 0 " + sAdd ;
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.TWENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 25 " + sAdd;
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.FIFTY_LINE_LARGE, x, y, width)) {
            String s = "count 50 " + sAdd ;
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        } else if (poly.inPolygon(poly.SEVENTYFIVE_LINE_LARGE, x, y, width)) {
            String s = "count 75 " + sAdd ;
            sc.reactAreaThenLineBargraphGrid(x, y, sa, action, s);
            return true;
        }
        return false;
    }


    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Food Results Barchart, axis intersection on the bottom left side");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}
