package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan1Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Floorplan2Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.FloorplanTrainingPolygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingFloorplanBehavior extends SlideBehaviorPolygon implements ListNavigation {

    public TrainingFloorplanBehavior(SlideController sc) {
        super(R.drawable.training_floorplan, sc);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
        Floorplan2Polygons poly = new Floorplan2Polygons();
//        a.drawPolygon(poly.EXIT);
//        a.drawPolygon(poly.KAY_JEWELRY);
//        a.drawPolygon(poly.STARBUCKS);
//        a.drawPolygon(poly.ACE);
//        a.drawPolygon(poly.ZALES);
//        a.drawPolygon(poly.FRESH_START);
//        a.drawPolygon(poly.JARED_JEWELRY);
//        a.drawPolygon(poly.WATER_FOUNTAIN);
//        a.drawPolygon(poly.MEN);
//        a.drawPolygon(poly.WOMEN);
//        a.drawPolygon(poly.STORAGE);
//        a.drawPolygon(poly.DILLARDS);
//        a.drawPolygon(poly.MACYS);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        FloorplanTrainingPolygons poly = new FloorplanTrainingPolygons();
        if (poly.inPolygon(poly.ELEVATOR, x, y, width)) {
            String s = "Elevator, on the west side";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.COMMON_ROOM, x, y, width)) {
            String s = "Common Room, lower right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.RECEPTION, x, y, width)) {
            String s = "Reception";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.STAIRS, x, y, width)) {
            String s = "Stairs, upper left corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_112, x, y, width)) {
            String s = "Room 112";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_113, x, y, width)) {
            String s = "Room 113";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_114, x, y, width)) {
            String s = "Room 114";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ROOM_115, x, y, width)) {
            String s = "Room 115, upper right corner";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY1, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("Color: " + pixelColor);

        FloorplanTrainingPolygons poly = new FloorplanTrainingPolygons();

        if (poly.inPolygon(poly.OUTSIDE_CONTOUR, x, y, width)) {
            sa.playClick();
        }
        if (poly.inPolygon(poly.ELEVATOR, x, y, width)) {
            String s = "Elevator, on the west side";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.COMMON_ROOM, x, y, width)) {
            String s = "Common Room, lower right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.RECEPTION, x, y, width)) {
            String s = "Reception";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.STAIRS, x, y, width)) {
            String s = "Stairs, upper left corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_112, x, y, width)) {
            String s = "Room 112";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_113, x, y, width)) {
            String s = "Room 113";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_114, x, y, width)) {
            String s = "Room 114";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ROOM_115, x, y, width)) {
            String s = "Room 115, upper right corner";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY1, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.HALLWAY2, x, y, width)) {
            String s = "Hallway";
            sc.reactAreaThenLineFloorPlan(x, y, bitmap, sa, action, s);
        } else {
            sc.reactAreaThenLineFloorPlanNoSpeak(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Floor Plan of a dormitory, elevator at the left lower side");
        floorplan.add(topview);



        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
       // sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}