package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram4Polygons;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingScienceBehavior extends SlideBehaviorPolygon implements ListNavigation {

    public TrainingScienceBehavior(SlideController sc) {
        super(R.drawable.training_science, sc);
    }

    private Node butterfly;
    private Node head;
    private Node thorax;
    private Node abdomen;

    final static int HEAD_COLOR = -16759809;
    final static int ANTENNAE_COLOR = -15401216;
    final static int THORAX_COLOR = -12124089;
    final static int ABDOMEN_COLOR = -65282;
    final static int FORE_WINGS_COLOR = -16711685;
    final static int HIND_WINGS_COLOR = -16294564;

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();


        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "";
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(Diagram2Polygons.LEGS);
//                a.drawPolygon(Diagram2Polygons.WINGS);
//                a.drawPolygon(Diagram2Polygons.THORAX);
//                a.drawPolygon(Diagram2Polygons.ABDOMEN);
//                a.drawPolygon(Diagram2Polygons.HEAD);
    }

    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);


    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        //    sa.speak("A slightly different graph");
        int pixelColor = bitmap.getPixel(x,y);
        System.out.println("COLOR: " + pixelColor);

        Diagram4Polygons wp = new Diagram4Polygons();
        if (!sc.isLocked()) {
//            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
//                String s = "Head";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
//                String s = "Thorax";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
//                String s = "Abdomen";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
//                String s = "Antennae";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(FORE_WINGS_COLOR, x, y, bitmap)){
//                String s = "Fore Wings";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            } else if (sa.getTouchManagement().circleContains(HIND_WINGS_COLOR, x, y, bitmap)) {
//                String s = "Hind Wings";
//                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
//            }
        } else {
            sc.lockedVibration(x, y, bitmap, sa, action);
            if (sa.getTouchManagement().circleContains(HEAD_COLOR, x, y, bitmap)) {
                String s = "Head";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(THORAX_COLOR, x, y, bitmap)) {
                String s = "Thorax";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ABDOMEN_COLOR, x, y, bitmap)) {
                String s = "Abdomen";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(ANTENNAE_COLOR, x, y, bitmap)){
                String s = "Antennae";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(FORE_WINGS_COLOR, x, y, bitmap)){
                String s = "Fore Wings";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            } else if (sa.getTouchManagement().circleContains(HIND_WINGS_COLOR, x, y, bitmap)) {
                String s = "Hind Wings";
                sc.reactAreaThenLineDiagram(x, y, bitmap, sa, action, s);
            }
        }
    }


    /*
        Create overview list for navigation
     */
    @Override
    public List<Node> getOverviewStructure() {
        List<Node> flyList = new ArrayList<>();

        Node topview = new Node("Top view of a Butterfly, no items, head at the top middle");


        Node head = new Node("Head, one item", HEAD_COLOR);
        Node antennae = new Node("Antennae, one of one", ANTENNAE_COLOR);
        head.addChild(antennae);

        Node thorax = new Node("Thorax, two items", THORAX_COLOR);
        Node foreWings = new Node("Fore Wings, one of two", FORE_WINGS_COLOR);
        Node hindWings = new Node("Hind Wings, two of two", HIND_WINGS_COLOR);
        thorax.addChild(foreWings);
        thorax.addChild(hindWings);

        Node abdomen = new Node("Abdomen, no items", ABDOMEN_COLOR);

    //    Node endview = new Node("End of Top View");

        flyList.add(topview);
        flyList.add(head);
        flyList.add(thorax);
        flyList.add(abdomen);
       // beeList.add(endview);

        this.butterfly = topview;
        this.head = head;
        this.thorax = thorax;
        this.abdomen = abdomen;


        return flyList;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
       // sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {
        sc.lockNode(sa);
    }
}
