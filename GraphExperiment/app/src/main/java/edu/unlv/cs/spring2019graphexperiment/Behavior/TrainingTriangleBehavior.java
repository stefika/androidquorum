package edu.unlv.cs.spring2019graphexperiment.Behavior;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;
import edu.unlv.cs.spring2019graphexperiment.Polygons.TrigTrainingPolygon;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingTriangleBehavior extends SlideBehaviorPolygon implements ListNavigation {

    int tapCount = 0;

    public TrainingTriangleBehavior(SlideController sc) {
        super(R.drawable.training_triangle, sc);
    }

    @Override
    public void drawPolygons(SlideActivity a) {
//                a.drawPolygon(TrigTrainingPolygon.ANGLE_A);
//                a.drawPolygon(TrigTrainingPolygon.ANGLE_B);
//                a.drawPolygon(TrigTrainingPolygon.ANGLE_C);
//                a.drawPolygon(TrigTrainingPolygon.SMALL_A);
//                a.drawPolygon(TrigTrainingPolygon.SMALL_B);
//                a.drawPolygon(TrigTrainingPolygon.SMALL_C);
//                a.drawPolygon(TrigTrainingPolygon.VERTEX_A);
//                a.drawPolygon(TrigTrainingPolygon.VERTEX_B);
//                a.drawPolygon(TrigTrainingPolygon.VERTEX_C);
    }

    @Override
    public List<String> getOverviewList() {
        List<String> overviewList = new ArrayList<>();

        overviewList.add("Diagram of a triangle");
        overviewList.add("Angle A");
        overviewList.add("Angle B");
        overviewList.add("Angle C");
        overviewList.add("horizontal A");
        overviewList.add("diagonal C");
        overviewList.add("diagonal B");

        return overviewList;
    }

    @Override
    public String getOverviewString() {
        return "Diagram of a triangle; 3 angles – A,B, and C. 3 Lines – A, B, and C.";
    }


    @Override
    public void tapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);
        tapCount++;

        TrigTrainingPolygon poly = new TrigTrainingPolygon();
        if (poly.inPolygon(poly.SMALL_A, x, y, width)) {
            String s = "horizontal ay";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_B, x, y, width)) {
            String s = "diagonal B";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.SMALL_C, x, y, width)) {
            String s = "diagonal see";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ANGLE_A, x, y, width)) {
            String s = "angle ay, 100 degrees, between line b and line see";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ANGLE_B, x, y, width)) {
            String s = "angle B, 40 degrees, between line ay and line see";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.ANGLE_C, x, y, width)) {
            String s = "angle see, 40 degrees, between line ay and line b";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.VERTEX_A, x, y, width)) {
            String s = "Vertex ay, angle ay, 100 degrees";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.VERTEX_B, x, y, width)) {
            String s = "Vertex B, angle B, 40 degrees";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        } else if (poly.inPolygon(poly.VERTEX_C, x, y, width)) {
            String s = "Vertex see, angle see, 40 degrees";
            System.out.println("TAP: " + s + ", x: " + x + ", " + y + ", count: " + tapCount);
            sc.reactTapReadAgain(x, y, sa, s);
        }
    }

    @Override
    public void doubleTapReaction(int x, int y, Bitmap bitmap, SlideActivity sa) {

    }

    @Override
    public void touchReaction(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        int width = bitmap.getWidth();
        int pixelColor = bitmap.getPixel(x,y);

        System.out.println("COLOR: " + pixelColor);

        TrigTrainingPolygon poly = new TrigTrainingPolygon();

        if (poly.inPolygon(poly.SMALL_A, x, y, width)) {
            String s = "horizontal ay";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_B, x, y, width)) {
            String s = "diagonal B";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.SMALL_C, x, y, width)) {
            String s = "diagonal see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ANGLE_A, x, y, width)) {
            String s = "angle ay, 100 degrees, between line b and line see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ANGLE_B, x, y, width)) {
            String s = "angle B, 40 degrees, between line ay and line see";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.ANGLE_C, x, y, width)) {
            String s = "angle see, 40 degrees, between line ay and line b";
            sc.reactAreaThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.VERTEX_A, x, y, width)) {
            String s = "Vertex ay, angle ay, 100 degrees";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.VERTEX_B, x, y, width)) {
            String s = "Vertex B, angle B, 40 degrees";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.VERTEX_C, x, y, width)) {
            String s = "Vertex see, angle see, 40 degrees";
            sc.reactVertexThenLineTrig(x, y, bitmap, sa, action, s);
        } else if (poly.inPolygon(poly.INNER, x, y, width)) {
            String s = "INNER";
            sc.reactInnerArea(x,y,bitmap,sa,action,s);
        } else {
            sc.reactAreaThenLineTrigNoArea(x,y,bitmap,sa,action);
        }
    }

    @Override
    public List<Node> getOverviewStructure() {
        List<Node> floorplan = new ArrayList<>();

        Node topview = new Node("Triangle, Point A on top middle");
        floorplan.add(topview);

        return floorplan;
    }

    @Override
    public void doubleFingerTapReaction(SlideActivity sa) {
        //sa.stopAllSignals();
    }

    @Override
    public void doubleFingerDoubleTapReaction(SlideActivity sa) {

    }
}
