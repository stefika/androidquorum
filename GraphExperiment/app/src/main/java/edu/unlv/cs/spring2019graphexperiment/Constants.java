package edu.unlv.cs.spring2019graphexperiment;

public class Constants {
    public static final String EXTRA_SESSION = "edu.unvl.cs.message.SESSION";
    public static final String DATA_FILE = "data.csv";
    public static final int REQUEST_WRITE_PERMISSION = 42;

    public static final long[] VIB_PATTERN = new long[] {25,100};
    public static final long[] VIB_CONSTANT=  new long[]{0, 500};
    public static final long[] VIB_SHORT_BUZZ = new long[]{25, 75, 25, 75, 25, 75, 200};
    public static final long[] VIB_TRANSITION_BUMP = new long[]{25, 75, 25, 75, 400, 50, 200, 0};
    public static final long[] VIB_WEAPON = new long[]{100, 50, 100, 50, 100, 50};
    public static final long[] VIB_ENGINE = new long[]{9, 7, 9, 7, 9, 7, 9};

    public static final long[] VIB_CLICK = new long[]{0,100};
}
