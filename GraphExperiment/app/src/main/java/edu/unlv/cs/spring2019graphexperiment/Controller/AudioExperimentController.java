package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioDiagram2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioMap2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticFloorplan3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticScience3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.OldBee;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingAudioBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingAudioDiagramBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingAudioMapBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingAudioTrigBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingFloorplanBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingScienceBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingTriangleBehavior;

public class AudioExperimentController extends SlideController {

    List<SlideBehavior> session0 = new ArrayList<SlideBehavior>();

    int groupId = -1;

    public AudioExperimentController(int session, int group) {
        super(session);
        this.groupId = group;
        createBehaviors();
    }

    private void createBehaviors() {
        setUpSession0();
    }

    /*
    While this could have possibly been solved more automatically instead of having
    A ton of strings in an array, this was the fastest, most reliable way to do this.
     */
    public void setUpSession0() {

        int slideIndex = (groupId + 1) % 4; // adjusts to next row, as requested for audio description
        String currentOrdering = slideOrder[slideIndex];

        String[] orderingSplit = currentOrdering.split(",");
        System.out.println("ORDERING : groupid:" + groupId + ", split: " + orderingSplit.length);

        for (String s:
             orderingSplit) {
            if (s.equals("A")) {
                session0.add(new TrainingAudioTrigBehavior(this));
                session0.add(new AudioTrig2Behavior(this));
            } else if (s.equals("B")) {
                session0.add(new TrainingAudioBarchartBehavior(this));
                session0.add(new AudioBarchart2Behavior(this));
            } else if (s.equals("C")) {
                session0.add(new TrainingAudioDiagramBehavior(this));
                session0.add(new AudioDiagram2Behavior(this));
            } else if (s.equals("D")) {
                session0.add(new TrainingAudioMapBehavior(this));
                session0.add(new AudioMap2Behavior(this));
            } else {
                // This shouldn't happen, just adding something obviously wrong
                // to more easily spot issues
                System.out.println("AAAAAAAH");
                session0.add(new OldBee(this));
            }
        }
    }

    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        return session0;
    }


    String[] slideOrder = {
            "A,B,D,C",
            "B,C,A,D",
            "C,D,B,A",
            "D,A,C,B"
    };



}
