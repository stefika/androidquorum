package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBarchartBehavior;

public class BarchartController extends SlideController {


    List<SlideBehavior> session1 = new ArrayList<SlideBehavior>();

    public BarchartController(int session) {
        super(session);

        createBehaviors();
    }

    private void createBehaviors() {

        setUpSession1();
    }

    public void setUpSession1() {

        SlideBehaviorPolygon slide0 = new TrainingBarchartBehavior(this);

        SlideBehaviorPolygon slide1 = new HapticBarchart1Behavior(this);

        SlideBehaviorPolygon slide2 = new HapticBarchart2Behavior(this);

        SlideBehaviorPolygon slide3 = new HapticBarchart3Behavior(this);


        session1.add(slide0);
        session1.add(slide1);
        session1.add(slide2);
        session1.add(slide3);
     //   session1.add(slide2a);
    }

    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        return session1;
    }




}
