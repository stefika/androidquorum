package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.UUID;

public class Event {

    private String uuid;
    private long timeStamp;
    private float xPos;
    private float yPos;
    private String status;
    private String voiceArea;
    private String action;
    private final int slide;
    private int id;
    private int group;
    private int session;

    public static final String START_EVENT = "Slide start";
    public static final String PAUSE_EVENT = "Slide paused";
    public static final String CONTINUE_EVENT = "Slide continued";
    public static final String END_EVENT = "Slide end";
    public static final String ON_LINE = "On line";
    public static final String OFF_LINE = "Off line";
    public static final String TAP = "Tap";
    public static final String DOUBLE_TAP = "Double Tap";
    public static final String EVENT_UP = "Stop touch";
    public static final String SWIPE_NEXT = "Swipe Next";
    public static final String SWIPE_BACK = "Swipe Back";
    public static final String SWIPE_UP = "Swipe Up";
    public static final String SWIPE_DOWN = "Swipe Down";
    public static final String DOUBLE_FINGER_DOUBLE_TAP = "Double Finger Double Tap";

    public static final String ACTION_MOVE = "Action Move";
    public static final String ACTION_DOWN = "Action Down";
    public static final String ACTION_UP = "Action Up";
    public static final String ACTION_NONE = "NA";
    public static final String ACTION_TAP = "Action Tap";
    public static final String ACTION_DOUBLE_TAP = "Action Double Tap";
    public static final String ACTION_SWIPE_NEXT = "Action Swipe Next";
    public static final String ACTION_SWIPE_BACK = "Action Swipe Back";
    public static final String ACTION_SWIPE_UP = "Action Swipe Up";
    public static final String ACTION_SWIPE_DOWN = "Action Swipe Down";
    public static final String LOCK = "Lock";
    public static final String UNLOCK = "Unlock";

    public static final String AREA_NONE = "NA";

    public Event(float x, float y, String status, String voiceArea, String action, int slide, int id, int group, int session) {
        this.uuid = UUID.randomUUID().toString();
        this.timeStamp = System.currentTimeMillis();
        this.xPos = x;
        this.yPos = y;
        this.status = status;
        this.voiceArea = voiceArea;
        this.slide = slide;
        this.id = id;
        this.group = group;
        this.session = session;
        this.action = action;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public float getxPos() {
        return xPos;
    }

    public void setxPos(float xPos) {
        this.xPos = xPos;
    }

    public float getyPos() {
        return yPos;
    }

    public void setyPos(float yPos) {
        this.yPos = yPos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVoiceArea() {
        return voiceArea;
    }

    public void setVoiceArea(String voiceArea) {
        this.voiceArea = voiceArea;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public int getSlide() {
        return slide;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public static String makeCSVHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("UUID");
        sb.append(",");
        sb.append("Timestamp");
        sb.append(",");
        sb.append("ParticipantID");
        sb.append(",");
        sb.append("GroupID");
        sb.append(",");
        sb.append("SessionID");
        sb.append(",");
        sb.append("SlideNumber");
        sb.append(",");
        sb.append("Event");
        sb.append(",");
        sb.append("VoiceArea");
        sb.append(",");
        sb.append("Action");
        sb.append(",");
        sb.append("x");
        sb.append(",");
        sb.append("y");
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }


    public String toCSVRow() {
        StringBuilder sb = new StringBuilder();
        sb.append(getUuid());
        sb.append(",");
        sb.append(getTimeStamp());
        sb.append(",");
        sb.append(getId());
        sb.append(",");
        sb.append(getGroup());
        sb.append(",");
        sb.append(getSession());
        sb.append(",");
        sb.append(getSlide());
        sb.append(",");
        sb.append(getStatus());
        sb.append(",");
        sb.append(getVoiceArea());
        sb.append(",");
        sb.append(getAction());
        sb.append(",");
        sb.append(getxPos());
        sb.append(",");
        sb.append(getyPos());
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}

