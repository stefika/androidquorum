package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioDiagram2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioMap2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedDiagram1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedMap1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.OldDiagram3;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticMap3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBehavior;

public class Group1Controller extends SlideController {


    List<SlideBehavior> session1 = new ArrayList<SlideBehavior>();

    List<SlideBehavior> session2 = new ArrayList<SlideBehavior>();

    public Group1Controller(int session) {
        super(session);

        createBehaviors();
    }

    private void createBehaviors() {

        setUpSession1();
        setUpSession2();
    }

    public void setUpSession1() {
        SlideBehavior slide1 = new EmbossedBarchart1Behavior(this);

        SlideBehavior slide1a = new QuestionBehavior(this);

        SlideBehavior slide2 = new EmbossedTrig1Behavior(this);

        SlideBehavior slide2a = new QuestionBehavior(this);

        SlideBehavior slide3 = new AudioBarchart2Behavior(this);

        SlideBehavior slide3a = new QuestionBehavior(this);

        SlideBehavior slide4 = new AudioTrig2Behavior(this);

        SlideBehavior slide4a = new QuestionBehavior(this);


        SlideBehavior slide5 = new TrainingBehavior(this);

        SlideBehavior slide5a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide6 = new HapticBarchart3Behavior(this);

        SlideBehavior slide6a = new QuestionBehavior(this);


        SlideBehaviorPolygon slide7 = new HapticTrig3Behavior(this);

        SlideBehavior slide7a = new QuestionBehavior(this);


        session1.add(slide1);
        session1.add(slide1a);
        session1.add(slide2);
        session1.add(slide2a);
        session1.add(slide3);
        session1.add(slide3a);
        session1.add(slide4);
        session1.add(slide4a);
        session1.add(slide5);
        session1.add(slide5a);
        session1.add(slide6);
        session1.add(slide6a);
        session1.add(slide7);
        session1.add(slide7a);
    }

    public void setUpSession2() {
        SlideBehavior slide1 = new EmbossedDiagram1Behavior(this);

        SlideBehavior slide1a = new QuestionBehavior(this);

        SlideBehavior slide2 = new EmbossedMap1Behavior(this);

        SlideBehavior slide2a = new QuestionBehavior(this);

        SlideBehavior slide3 = new AudioDiagram2Behavior(this);

        SlideBehavior slide3a = new QuestionBehavior(this);

        SlideBehavior slide4 = new AudioMap2Behavior(this);

        SlideBehavior slide4a = new QuestionBehavior(this);

        SlideBehavior slide5 = new TrainingBehavior(this);

        SlideBehavior slide5a = new QuestionBehavior(this);

        SlideBehavior slide6 = new OldDiagram3(this);

        SlideBehavior slide6a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide7 = new HapticMap3Behavior(this);

        SlideBehavior slide7a = new QuestionBehavior(this);

        session2.add(slide1);
        session2.add(slide1a);
        session2.add(slide2);
        session2.add(slide2a);
        session2.add(slide3);
        session2.add(slide3a);
        session2.add(slide4);
        session2.add(slide4a);
        session2.add(slide5);
        session2.add(slide5a);
        session2.add(slide6);
        session2.add(slide6a);
        session2.add(slide7);
        session2.add(slide7a);
    }


    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        if (session == 1) {
            return session1;
        }
        return session2;
    }

}
