package edu.unlv.cs.spring2019graphexperiment.Controller;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioBarchart1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioDiagram1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioMap1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.AudioTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedDiagram3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedMap3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.OldAnt;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticMap2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBehavior;

public class Group2Controller extends SlideController{

    List<SlideBehavior> session1 = new ArrayList<SlideBehavior>();

    List<SlideBehavior> session2 = new ArrayList<SlideBehavior>();
    
    public Group2Controller(int session) {
        super(session);

        createBehaviors();
    }

    private void createBehaviors() {
        setUpSession1();
        setUpSession2();
    }

    private void setUpSession1 () {
        SlideBehavior slide1 = new AudioBarchart1Behavior(this);

        SlideBehavior slide1a = new QuestionBehavior(this);

        SlideBehavior slide2 = new AudioTrig1Behavior(this );

        SlideBehavior slide2a = new QuestionBehavior(this   );

        SlideBehavior slide3 = new TrainingBehavior(this);

        SlideBehavior slide3a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide4 = new HapticBarchart2Behavior(this);

        SlideBehavior slide4a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide5 = new HapticTrig2Behavior(this);

        SlideBehavior slide5a = new QuestionBehavior(this);

        SlideBehavior slide6 = new EmbossedBarchart3Behavior(this);

        SlideBehavior slide6a = new QuestionBehavior(this);

        SlideBehavior slide7 = new EmbossedTrig3Behavior(this);

        SlideBehavior slide7a = new QuestionBehavior(this);

        session1.add(slide1);
        session1.add(slide1a);
        session1.add(slide2);
        session1.add(slide2a);
        session1.add(slide3);
        session1.add(slide3a);
        session1.add(slide4);
        session1.add(slide4a);
        session1.add(slide5);
        session1.add(slide5a);
        session1.add(slide6);
        session1.add(slide6a);
        session1.add(slide7);
        session1.add(slide7a);
    }


    private void setUpSession2 () {
        SlideBehavior slide1 = new AudioDiagram1Behavior(this);

        SlideBehavior slide1a = new QuestionBehavior(this);

        SlideBehavior slide2 = new AudioMap1Behavior(this);

        SlideBehavior slide2a = new QuestionBehavior(this);

        SlideBehavior slide3 = new TrainingBehavior(this);

        SlideBehavior slide3a = new QuestionBehavior(this   );

        SlideBehaviorPolygon slide4 = new OldAnt(this);

        SlideBehavior slide4a = new QuestionBehavior(this);

        SlideBehavior slide5 = new HapticMap2Behavior(this);

        SlideBehavior slide5a = new QuestionBehavior(this);

        SlideBehavior slide6 = new EmbossedDiagram3Behavior(this);

        SlideBehavior slide6a = new QuestionBehavior(this);

        SlideBehavior slide7 = new EmbossedMap3Behavior(this);

        SlideBehavior slide7a = new QuestionBehavior(this);

        session2.add(slide1);
        session2.add(slide1a);
        session2.add(slide2);
        session2.add(slide2a);
        session2.add(slide3);
        session2.add(slide3a);
        session2.add(slide4);
        session2.add(slide4a);
        session2.add(slide5);
        session2.add(slide5a);
        session2.add(slide6);
        session2.add(slide6a);
        session2.add(slide7);
        session2.add(slide7a);
    }

        private void reactAreaThenLine(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
            sa.speak(s);
            if (sa.getTouchManagement().circleIsNotOnly(-1, x, y, bitmap)) {
                sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
                sa.startVibration();
            } else {
                sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
                sa.stopVibration();
            }
        }


    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        if (session == 1) {
            return session1;
        }
        return session2;
    }


















}
