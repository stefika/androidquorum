package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrainingTriangleBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticFloorplan3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticScience3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.OldBee;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingFloorplanBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingScienceBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingTriangleBehavior;

public class HapticExperimentController extends SlideController {

    List<SlideBehavior> session0 = new ArrayList<SlideBehavior>();

    int groupId = -1;

    public HapticExperimentController(int session, int group) {
        super(session);
        this.groupId = group;
        createBehaviors();
    }

    private void createBehaviors() {
        setUpSession0();
    }

    /*
    While this could have possibly been solved more automatically instead of having
    A ton of strings in an array, this was the fastest, most reliable way to do this.
     */
    public void setUpSession0() {

        String currentOrdering = slideOrder[groupId];

        String[] orderingSplit = currentOrdering.split(",");
        System.out.println("ORDERING : groupid:" + groupId + ", split: " + orderingSplit.length);

        for (String s:
             orderingSplit) {
            if (s.equals("A")) {
                session0.add(new TrainingTriangleBehavior(this));
                session0.add(new HapticTrig3Behavior(this));
            } else if (s.equals("B")) {
                session0.add(new TrainingBarchartBehavior(this));
                session0.add(new HapticBarchart3Behavior(this));
            } else if (s.equals("C")) {
                session0.add(new TrainingScienceBehavior(this));
                session0.add(new HapticScience3Behavior(this));
            } else if (s.equals("D")) {
                session0.add(new TrainingFloorplanBehavior(this));
                session0.add(new HapticFloorplan3Behavior(this));
            } else {
                // This shouldn't happen, just adding something obviously wrong
                // to more easily spot issues
                System.out.println("AAAAAAAH");
                session0.add(new OldBee(this));
            }
        }
    }

    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        return session0;
    }


    String[] slideOrder = {
            "A,B,D,C",
            "B,C,A,D",
            "C,D,B,A",
            "D,A,C,B"
    };



}
