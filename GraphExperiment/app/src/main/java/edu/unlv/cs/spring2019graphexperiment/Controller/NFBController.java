package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrainingTriangleBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.EmbossedTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticBarchart3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.OldBee;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticTrig2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingTriangleBehavior;

public class NFBController extends SlideController {

    List<SlideBehavior> session0 = new ArrayList<SlideBehavior>();

    int groupId = -1;

    public NFBController(int session, int group) {
        super(session);
        this.groupId = group;
        createBehaviors();
    }

    private void createBehaviors() {
        setUpSession0();
    }

    /*
    While this could have possibly been solved more automatically instead of having
    A ton of strings in an array, this was the fastest, most reliable way to do this.
     */
    public void setUpSession0() {

        String currentOrdering = slideOrder[groupId];

        String[] orderingSplit = currentOrdering.split(",");
        System.out.println("ORDERING : groupid:" + groupId + ", split: " + orderingSplit.length);

        for (String s:
             orderingSplit) {
            if (s.equals("TS-Training-TR")) {
                session0.add(new TrainingTriangleBehavior(this));
            } else if (s.equals("TS-Training-BC")) {
                session0.add(new TrainingBarchartBehavior(this));
            } else if (s.equals("EM-Training-TR")) {
                session0.add(new EmbossedTrainingTriangleBehavior(this));
            } else if (s.equals("EM-Training-BC")) {
                session0.add(new EmbossedTrainingBarchartBehavior(this));
            } else if (s.equals("pause")) {
                session0.add(new QuestionBehavior(this));
            } else if (s.equals("TS-TR-1")) {
                session0.add(new HapticTrig2Behavior(this));
            } else if (s.equals("TS-TR-2")) {
                session0.add(new HapticTrig1Behavior(this));
            } else if (s.equals("TS-BC-1")) {
                session0.add(new HapticBarchart3Behavior(this));
            } else if (s.equals("TS-BC-2")) {
                session0.add(new HapticBarchart2Behavior(this));
            } else if (s.equals("EM-TR-1")) {
                session0.add(new EmbossedTrig1Behavior(this));
            } else if (s.equals("EM-TR-2")) {
                session0.add(new EmbossedTrig2Behavior(this));
            } else if (s.equals("EM-BC-1")) {
                session0.add(new EmbossedBarchart1Behavior(this));
            } else if (s.equals("EM-BC-2")) {
                session0.add(new EmbossedBarchart2Behavior(this));
            } else {
                // This shouldn't happen, just adding something obviously wrong
                // to more easily spot issues
                System.out.println("AAAAAAAH");
                session0.add(new OldBee(this));
            }
        }
    }

    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        return session0;
    }


    String[] slideOrder = {
            "TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2",
            "EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2",
            "TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2",
            "EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2",
            "TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1",
            "EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1",
            "TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1",
            "EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1",
            "TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2",
            "EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2",
            "TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2",
            "EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2",
            "TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1",
            "EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1",
            "TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1",
            "EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1",
            "TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2",
            "EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2",
            "TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2",
            "EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2",
            "TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1",
            "EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1",
            "TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1",
            "EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1",
            "TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1",
            "EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1",
            "TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1",
            "EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1",
            "TS-Training-TR,pause,TS-TR-2,pause,TS-TR-1,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2",
            "EM-Training-TR,pause,EM-TR-2,pause,EM-TR-1,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2",
            "TS-Training-BC,pause,TS-BC-2,pause,TS-BC-1,EM-Training-BC,pause,EM-BC-1,pause,EM-BC-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2",
            "EM-Training-BC,pause,EM-BC-2,pause,EM-BC-1,TS-Training-BC,pause,TS-BC-1,pause,TS-BC-2,EM-Training-TR,pause,EM-TR-1,pause,EM-TR-2,TS-Training-TR,pause,TS-TR-1,pause,TS-TR-2"
    };



}
