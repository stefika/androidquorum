package edu.unlv.cs.spring2019graphexperiment.Controller;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticFloorplan2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticScience1Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticScience2Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.HapticScience3Behavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingScienceBehavior;

public class NFBExtraController extends SlideController {


    List<SlideBehavior> session1 = new ArrayList<SlideBehavior>();

    public NFBExtraController(int session) {
        super(session);

        createBehaviors();
    }

    private void createBehaviors() {

        setUpSession1();
    }

    public void setUpSession1() {

        SlideBehaviorPolygon slide0 = new TrainingScienceBehavior(this);

        SlideBehaviorPolygon slide1 = new HapticScience1Behavior(this);

        SlideBehaviorPolygon slide2 = new HapticScience2Behavior(this);

        SlideBehaviorPolygon slide3 = new HapticScience3Behavior(this);

        SlideBehavior slide1a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide4 = new HapticFloorplan2Behavior(this);

        SlideBehavior slide2a = new QuestionBehavior(this);


        session1.add(slide0);
        session1.add(slide1);
        session1.add(slide2);
        session1.add(slide3);
        session1.add(slide4);
     //   session1.add(slide2a);
    }

    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        return session1;
    }




}
