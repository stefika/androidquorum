package edu.unlv.cs.spring2019graphexperiment.Controller;

import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;

import edu.unlv.cs.spring2019graphexperiment.Behavior.Node;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Constants;
import edu.unlv.cs.spring2019graphexperiment.ExperimentManager;
import edu.unlv.cs.spring2019graphexperiment.TreeNode;

public abstract class SlideController {

    int session = 1;
    private int slideNumber = 0;
    private String currentArea = "";
    private String lastArea = "";
    private Node lastNode = null;
    private boolean locked = false;

    private List<Event> eventLog = new ArrayList<>();

    public void addStartEvent() {
        Event e = new Event(-1, -1, Event.START_EVENT, Event.AREA_NONE, Event.ACTION_NONE,
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    public void addPauseEvent() {
        Event e = new Event(-1, -1, Event.PAUSE_EVENT, Event.AREA_NONE, Event.ACTION_NONE,
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    public void addContinueEvent() {
        Event e = new Event(-1, -1, Event.CONTINUE_EVENT, Event.AREA_NONE, Event.ACTION_NONE,
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    public void addEndEvent() {
        Event e = new Event(-1, -1, Event.END_EVENT, Event.AREA_NONE, Event.ACTION_NONE,
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    public void addTouchEvent(float x, float y, String event, String area, String action) {
        Event e = new Event(x, y, event, stripStringOfComma(area), stripStringOfComma(action),
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    private String stripStringOfComma(String s) {
        String r = s.replaceAll("\\,","\\_");
        System.out.println(r);
        return r;
    }

    public void addTouchEventUp(float x, float y) {
        Event e = new Event(x, y, Event.EVENT_UP, Event.AREA_NONE, Event.ACTION_UP,
                slideNumber, ExperimentManager.getParticipantId(),
                ExperimentManager.getGroupId(), ExperimentManager.getSessionId());
        eventLog.add(e);
        Log.d("Data", e.toCSVRow());
    }

    public SlideController() {
        this.session = 1;
        this.slideNumber = 0;
    }

    public SlideController(int session) {
        this.session = session;
        this.slideNumber = 0;
    }

    public boolean nextSlide() {
        if (slideNumber < getSlideArray(session).size() - 1) {
            slideNumber = slideNumber + 1;
            return true;
        }
        return false;
    }

    public boolean backSlide() {
        if (slideNumber > 0) {
            slideNumber = slideNumber - 1;
            return true;
        }
        return false;
    }


    private void clearEventLog() {
        eventLog.clear();
    }

    private void saveCSVFile(String data) {
        String fileName = ExperimentManager.getParticipantId() + "_" + Constants.DATA_FILE;

        File folder = ExperimentManager.getPublicDataStorage("data");

        File file = new File(folder.getAbsolutePath() + File.separator + fileName);

        boolean needsHeader = false;

        Log.d("Data", data);

        FileWriter fw = null;
        try {
            if (!file.exists()) {
                file.createNewFile();
                needsHeader = true;
            }
            fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            if(needsHeader) {
                bw.write(Event.makeCSVHeader());
            }
            bw.write(data);
            bw.close();
            clearEventLog();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void reactAreaThenLineBargraph(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        sa.speak(s);
        if (sa.getTouchManagement().circleContains(-15223881, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-10921639, x, y, bitmap)) {
            sa.playClick();
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(2);
        } else if (sa.getTouchManagement().circleContains(-827814, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(5);
        } else if (sa.getTouchManagement().circleContains(-7171438, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactTapReadAgain(int x, int y, SlideActivity sa, String area) {
        sa.speak(area);
        sa.getController().addTouchEvent(x, y, Event.TAP, stripStringOfComma(area), Event.ACTION_TAP);
    }

    public void reactSpeakYPosition(int x, int y, SlideActivity sa, String s) {
        sa.speak(s);
        sa.getController().addTouchEvent(x, y, Event.DOUBLE_TAP, stripStringOfComma(s), Event.ACTION_DOUBLE_TAP);
    }


    public void reactAreaThenLineBargraphNoRepeat(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        checkAreaSpeak(s,sa);

        if (sa.getTouchManagement().circleContains(-16728124, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action); // girl
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-493971, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action); // boy
            sa.startVibration(1);
        } else if (sa.getTouchManagement().circleContains(-7171438, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAnimalNoRepeat(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        checkAreaSpeak(s, sa);
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action); // girl
            sa.startVibration(4);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAnimalOffArea(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (!currentArea.equals("")) {
            lastArea = currentArea;
        }
        currentArea = "";
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action); // girl
            sa.startVibration(4);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineBargraphGrid(int x, int y, SlideActivity sa, String action, String s) {
        System.out.println("TEST: in speak");
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            //sa.playClick();
            //sa.vibrateClick();

        if (!currentArea.equals(s)) {
            if (lastArea.equals(s)) {
                currentArea = s;
            } else {
                currentArea = s;
                sa.speakBlocking(s);
            }
        }
    }

    public void reactAreaThenLineBargraphGridOffArea(int x, int y, SlideActivity sa, String action, String s) {
        sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
       // sa.playClick();
       // sa.startVibration(1);
        //sa.vibrateClick();
        if (!currentArea.equals(s)) {
            if (lastArea.equals(s)) {
                currentArea = s;
            } else {
                currentArea = s;
                sa.speakBlocking(s);
            }
        }
    }

    void checkAreaSpeak(String area, SlideActivity sa) {
        if (!currentArea.equals(area)) {
            if (lastArea.equals(area)) {
                currentArea = area;
            } else {
                currentArea = area;
                sa.speak(area);
            }
        }
    }

    public void reactAreaThenLineBargraphNoSpeak(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        if (!currentArea.equals("")) {
            lastArea = currentArea;
        }
        currentArea = "";
        if (sa.getTouchManagement().circleContains(-15223881, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "Girl bar", action);
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-827814, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "Boy bar", action);
            sa.startVibration(1);
//        } else if (sa.getTouchManagement().circleContains(-7171438, x, y, bitmap)) {
//            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "Axis", action);
//            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineTrig(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (!currentArea.equals(s)) {
            if (lastArea.equals(s)) {
                currentArea = s;
            } else {
                currentArea = s;
                sa.speak(s);
            }
        }
        if (sa.getTouchManagement().circleContains(-16776961, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-65536, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            //sa.startVibration(1);
            sa.playToneQuick();
        } else if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(1);
        } else if (sa.getTouchManagement().circleContains(-16711936, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactInnerArea(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        sa.playClick();
        if (sa.getTouchManagement().circleContains(-16776961, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-65536, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(3);
        } else if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(1);
        } else if (sa.getTouchManagement().circleContains(-16711936, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactVertexThenLineTrig(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (!currentArea.equals(s)) {
            if (lastArea.equals(s)) {
                currentArea = s;
            } else {
                currentArea = s;
                sa.speak(s);
            }
        }
        //sa.playToneQuick();
        sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
        sa.startVibration(3);
    }

    public void reactAreaThenLineTrigNoArea(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        if (!currentArea.equals("")) {
            lastArea = currentArea;
        }
        currentArea = "";
        if (sa.getTouchManagement().circleContains(-16776961, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, Event.AREA_NONE, action);
            sa.startVibration(4);
        } else if (sa.getTouchManagement().circleContains(-65536, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, Event.AREA_NONE, action);
            sa.startVibration(1);
//        } else if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
//            sa.getController().addTouchEvent(x, y, Event.ON_LINE, Event.AREA_NONE, action);
//            sa.startVibration(1);
        } else if (sa.getTouchManagement().circleContains(-16711936, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, Event.AREA_NONE, action);
            sa.startVibration(0);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineDiagram(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (!currentArea.equals(s)) {
            if (lastArea.equals(s)) {
                currentArea = s;
            } else {
                currentArea = s;
                sa.speak(s);
            }
        }
        sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
       // if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
         //   sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
           // sa.startVibration(4);
        //} else {
      //      sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
          //  sa.stopVibration();
       // }
    }

    public void reactAreaThenLineDiagramNoSpeak(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        if (!currentArea.equals("")) {
            lastArea = currentArea;
        }
        currentArea = "";
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(4);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineMap(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (currentArea != s) {
            currentArea = s;
            sa.speak(s);
        } else {
            sa.playTone();
        }
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(4);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineFloorPlan(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (currentArea != s) {
            currentArea = s;
            sa.speak(s);
        }
         if (sa.getTouchManagement().circleContains(-589569, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(3);
         } else if (sa.getTouchManagement().circleContains(-65281, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(3);
         } else if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(1);

        } else if (sa.getTouchManagement().circleContains(-16776961,x,y,bitmap)) {
            //doorway
            System.out.println(" in reactAreaThenLineFloorPlan");
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.speak("door");
            sa.startVibration(1);
        }
        else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineFloorPlanNoSpeak(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(1);
        } else if (sa.getTouchManagement().circleContains(-65281, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(2);
        } else if (sa.getTouchManagement().circleContains(-589569, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(2);
        } else if (sa.getTouchManagement().circleContains(-16776961,x,y,bitmap)) {
            //doorway
            System.out.println(" in reactAreaThenLineFloorPlan");
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.speak("door");
            sa.startVibration(1);
        }
        else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
            sa.stopVibration();
        }
    }


    public void reactAreaThenLineMapNoSpeak(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        currentArea = "";
        if (sa.getTouchManagement().circleContains(-16777216, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(4);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineWyomingNationalPark(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (currentArea != s) {
            currentArea = s;
            sa.speak(s);
        } else {
         //   sa.playTone();
        }
        if (sa.getTouchManagement().circleContains(-10080879, x, y, bitmap)) {
            sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
            sa.startVibration(1);
        } else {
            sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
            sa.stopVibration();
        }
    }

    public void reactAreaThenLineWyomingNoSpeak(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {
        currentArea = "";
        sa.getController().addTouchEvent(x, y, Event.OFF_LINE, Event.AREA_NONE, action);
        sa.stopVibration();
    }

    public void setNode(Node node, SlideActivity sa) {
        if (lastNode != node) {
            System.out.println("set tree node: " + node.getName());
            sa.setCurrentNode(node);
            sa.speak(node.getName());
            lastNode = node;
        }
    }

    public void lockNode(SlideActivity sa) {
        System.out.println("locking");
        Node n = sa.getCurrentNode();
        System.out.println("locking after get node");

        if (n == null || n.getColor() == -1) {
            System.out.println("Unlocked");
            String nodeName = "None";
            if (lastNode != null && lastNode.getName() != null) {
                nodeName = lastNode.getName();
            }
            sa.getController().addTouchEvent(-1, -1, Event.DOUBLE_FINGER_DOUBLE_TAP, nodeName, Event.UNLOCK);
            unlock(sa);
        } else {
            System.out.println("locking in else");
            if (lastNode != n) {
                String s = "";
                if (lastNode != null) {
                    s = "unlocked " + lastNode.getName() + ", locked " + n.getName();
                    sa.getController().addTouchEvent(-1, -1, Event.DOUBLE_FINGER_DOUBLE_TAP, lastNode.getName(), Event.UNLOCK);
                } else {
                    s = "locked " + n.getName();
                }
                locked = true;
                System.out.println("lock tree node: " + n.getName());
                lastNode = n;
                sa.speak(s);
                sa.getController().addTouchEvent(-1, -1, Event.DOUBLE_FINGER_DOUBLE_TAP, n.getName(), Event.LOCK);
            } else {
                System.out.println("Unlocking same item");
                sa.speak("unlocked " + n.getName());
                sa.getController().addTouchEvent(-1, -1, Event.DOUBLE_FINGER_DOUBLE_TAP, n.getName(), Event.UNLOCK);
                unlock(sa);
            }
        }
    }

    public void unlock (SlideActivity sa) {
       // sa.speak("unlocked");
        lastNode = null;
        locked = false;
    }

    public boolean isLocked() {
        return locked;
    }

    public void lockedVibration(int x, int y, Bitmap bitmap, SlideActivity sa, String action) {

        if (sa.getTouchManagement().circleContains(lastNode.getColor(), x, y, bitmap)) {
            //  sa.getController().addTouchEvent(x, y, Event.ON_LINE, "line", action);
            sa.startVibration(1);
        } else  if (sa.getTouchManagement().circleContains(lastNode.getColorTwo(), x, y, bitmap)) {
            sa.startVibration(5);
        } else {
            sa.stopVibration();
        }
    }

    /*
    Forces read
     */
    public void tapTreeNode(Node node, SlideActivity sa) {
        if (lastNode != node) {
            System.out.println("tree node: " + node.getName());
            sa.setCurrentNode(node);
            sa.speak(node.getName());
        }
        lastNode = node;
    }

    private class DelayedSpeak implements Runnable {
        private SlideActivity sa;
        private String s;

        public DelayedSpeak (SlideActivity sa, String s) {
            this.sa = sa;
            this.s = s;
        }

        private SlideActivity getSa() {
            return sa;
        }

        @Override
        public void run() {
            sa.speak(s);
        }
    }

    public void reactAreaThenLineWyomingRiver(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (currentArea != s) {
            currentArea = s;
            sa.speak(s);
        }
        sa.getController().addTouchEvent(x, y, Event.ON_LINE, s, action);
        sa.startVibration(4);
    }

    public void reactAreaThenLineWyomingCity(int x, int y, Bitmap bitmap, SlideActivity sa, String action, String s) {
        if (currentArea != s) {
            currentArea = s;
            sa.playToneQuick();
            Handler handler = new Handler();
            handler.postDelayed(new DelayedSpeak(sa,s), 80);
        }
        sa.getController().addTouchEvent(x, y, Event.OFF_LINE, s, action);
        sa.stopVibration();
    }



    abstract public List<SlideBehavior> getSlideArray(int session);

    public SlideBehavior getSlideBehavior() {
        return getSlideArray(session).get(slideNumber);
    }

    public int getDrawableId() {
        return getSlideArray(session).get(slideNumber).getDrawableId();
    }

    public int getSlideNumber() {
        return slideNumber;
    }

    public void setSlideNumber(int slideNumber) {
        this.slideNumber = slideNumber;
    }

    public int getSession() {
        return session;
    }

    public void setSession(int session) {
        this.session = session;
    }

    public void commitData() {
        StringBuilder sb = new StringBuilder();
        for (Event event: eventLog) {
            sb.append(event.toCSVRow());
        }
        saveCSVFile(sb.toString());
    }


}
