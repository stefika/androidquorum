package edu.unlv.cs.spring2019graphexperiment.Controller;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.spring2019graphexperiment.Activities.SlideActivity;
import edu.unlv.cs.spring2019graphexperiment.Behavior.QuestionBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.SlideBehaviorPolygon;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingBarchartBehavior;
import edu.unlv.cs.spring2019graphexperiment.Behavior.TrainingTriangleBehavior;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Bar3Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.BarTrainingPolygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Diagram3Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Map3Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.Trig3Polygons;
import edu.unlv.cs.spring2019graphexperiment.Polygons.TrigTrainingPolygon;
import edu.unlv.cs.spring2019graphexperiment.R;

public class TrainingController extends SlideController {


    List<SlideBehavior> session1 = new ArrayList<SlideBehavior>();

    List<SlideBehavior> session2 = new ArrayList<SlideBehavior>();

    public TrainingController(int session) {
        super(session);

        createBehaviors();
    }

    private void createBehaviors() {

        setUpSession1();
        setUpSession2();
    }

    public void setUpSession1() {

        SlideBehaviorPolygon slide6 = new TrainingBarchartBehavior(this);

        SlideBehavior slide6a = new QuestionBehavior(this);

        SlideBehaviorPolygon slide7 = new TrainingTriangleBehavior(this);

        SlideBehavior slide7a = new QuestionBehavior(this);


        session1.add(slide6);
        session1.add(slide6a);
        session1.add(slide7);
        session1.add(slide7a);
    }

    public void setUpSession2() {

    }


    @Override
    public List<SlideBehavior> getSlideArray(int session) {
        if (session == 1) {
            return session1;
        }
        return session2;
    }




}
