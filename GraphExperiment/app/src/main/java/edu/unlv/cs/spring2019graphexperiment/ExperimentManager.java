package edu.unlv.cs.spring2019graphexperiment;

import android.os.Environment;
import android.util.Log;

import java.io.File;

import edu.unlv.cs.spring2019graphexperiment.Controller.SlideController;

public class ExperimentManager {

    private static SlideController controller;

    private static int participantId = -1;
    private static int groupId = -1;
    private static int sessionId = -1;

    public static void reset() {
        participantId = -1;
        groupId = -1;
        sessionId = -1;
        controller = null;
    }

    public static File getPublicDataStorage(String datafolder) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), datafolder);
        if (!file.mkdirs()) {
            Log.e("FileWriting", "Directory not created");
        }
        return file;
    }

    public static int getParticipantId() {
        return participantId;
    }

    public static void setParticipantId(int pId) {
        participantId = pId;
    }

    public static void setController(SlideController controller) {
        ExperimentManager.controller = controller;
    }

    public static SlideController getController() {
        return controller;
    }

    public static void setGroup(int group) {
        ExperimentManager.groupId = group;
    }

    public static int getGroupId() {
        return groupId;
    }

    public static int getSessionId() {
        return sessionId;
    }

    public static void setSessionId(int sessionId) {
        ExperimentManager.sessionId = sessionId;
    }
}
