package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Bar2Polygons extends Polygons {

    public static float[][] QUESTION1_LABEL = {{403.0f, 1186.9901f}, {394.0f, 1300.2491f},
            {800.0f, 1277.9541f},
            {859.0f, 1182.5311f}};


    public static float[][] QUESTION2_LABEL = {{1264.0f, 1181.6393f}, {1259.0f, 1262.7935f},
            {1702.0f, 1272.6033f},
            {1710.9456f, 1173.419f}};


    public static float[][] Q1_YES_BAR = {
            {400.0f, 1129.0f},
            {400.0f, 650.0f},
            {572.0f, 650.0f},
            {572.0f, 1129.0f}
    };

    public static float[][] Q1_NO_BAR = {
            {706.0f, 1123.0f},
            {706.0f, 324.0f},
            {886.0f, 324.0f},
            {886.0f, 1123.0f}
    };

    public static float[][] Q2_YES_BAR = {
            {1246.0f, 1130.0f},
            {1246.0f, 425.0f},
            {1421.0f, 425.0f},
            {1421.0f, 1130.0f}
    };

    public static float[][] Q2_NO_BAR = {
            {1551.0f, 1128.0f},
            {1551.0f, 549.0f},
            {1720.0f, 549.0f},
            {1720.0f, 1128.0f}
    };

    public static float[][] ZERO = {
            {25.0f, 1197.0f},
            {17.0f, 1274.3809f},
            {103.07683f, 1273.0f},
            {121.0f, 1218.0f}
    };

    public static float[][] TWENTYFIVE = {
            {10.0f, 996.0f},
            {94.0f, 997.0f},
            {82.0f, 921.0f},
            {10.0f, 915.0f}
    };

    public static float[][] FIFTY = {
            {10.0f, 741.0f},
            {92.0f, 745.0f},
            {83.0f, 611.0f},
            {10.0f, 622.0f}
    };

    public static float[][] SEVENTYFIVE = {
            {43.0f, 467.0f},
            {115.0f, 468.0f},
            {130.0f, 352.0f},
            {43.955425f, 367.04456f}
    };

    public static float[][] TITLE = {{825.0f, 32.99672f}, {790.0f, 131.09508f},
            {1347.0f, 140.0131f},
            {1385.0f, 12.485246f}};


    public static float[][] SEVENTYFIVE_LINE_LARGE = {
            {141.0f, 151.0f},
            {141.0f, 181.0f},
            {2037.0f, 181.0f},
            {2037.0f, 151.0f}
    };

    public static float[][] FIFTY_LINE_LARGE = {
            {141.0f, 465.0f},
            {141.0f, 495.0f},
            {2037.0f, 495.0f},
            {2037.0f, 465.0f}
    };

    public static float[][] TWENTYFIVE_LINE_LARGE = {
            {141.0f, 790.0f},
            {141.0f, 820.0f},
            {2037.0f, 820.0f},
            {2037.0f, 790.0f}
    };

    public static float[][] ZERO_LINE_LARGE = {
            {141.0f, 1116.0f},
            {141.0f, 1146.0f},
            {2037.0f, 1146.0f},
            {2037.0f, 1116.0f}
    };

    public static float[][] AXIS_Y = {{10.0f, 828.4852f}, {124.0f, 836.8579f},
            {145.0f, 466.41312f},
            {36.873352f, 449.3559f}};

    public static float[][] AXIS_X = {{861.0f, 1352.8656f}, {865.0f, 1221.7705f},
            {1221.0f, 1215.5278f},
            {1256.0f, 1360.0f}};


    public static final float[][] INNER = {

    };
}
