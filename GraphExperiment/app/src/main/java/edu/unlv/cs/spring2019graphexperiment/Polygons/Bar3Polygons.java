package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Bar3Polygons extends Polygons{

    public static float[][] X_AXIS =
            {{841.0f, 1355.541f}, {869.0f, 1217.3114f},
                    {1242.4381f, 1221.7705f},
                    {1304.0f, 1354.6492f}};

    public static float[][] Y_AXIS =
            {{10.0f, 784.78687f}, {148.02817f, 782.23706f},
                    {161.0f, 588.0982f},
                    {39.0f, 517.2459f}};

    public static float[][] QUESTION1_LABEL = {{429.0f, 1186.9901f}, {428.0f, 1263.0009f},
            {808.0f, 1275.2625f},
            {823.0f, 1175.3967f}};


    public static float[][] QUESTION2_LABEL = {{1297.0f, 1171.8295f}, {1298.0f, 1275.2787f},
            {1668.0f, 1276.1705f},
            {1664.0f, 1165.141f}};
    public static float[][] Q1_BOY_FULL_BAR = {
            {408.0f, 1134.0f},
            {408.0f, 432.0f},
            {587.0f, 432.0f},
            {587.0f, 1134.0f}
    };

    public static float[][] Q1_GIRL_FULL_BAR = {
            {708.0f, 1130.0f},
            {708.0f, 395.0f},
            {878.0f, 395.0f},
            {878.0f, 1130.0f}
    };

    public static float[][] Q2_BOY_FULL_BAR ={
            {1265.0f, 1129.0f},
            {1265.0f, 622.0f},
            {1426.0f, 622.0f},
            {1426.0f, 1129.0f}
    };

    public static float[][] Q2_GIRL_FULL_BAR ={
            {1554.0f, 1132.0f},
            {1554.0f, 649.0f},
            {1719.0f, 649.0f},
            {1719.0f, 1132.0f}
    };


    public static float[][] ZERO = {
            {18.0f, 1193.0f},
            {17.24171f, 1273.4834f},
            {120.10577f, 1256.5769f},
            {111.0f, 1178.0f}
    };

    public static float[][] TWENTYFIVE = {
            {13.0f, 1010.0f},
            {93.0f, 1018.0f},
            {91.0f, 936.0f},
            {12.431338f, 922.7843f}
    };

    public static float[][] SEVENTYFIVE = {
            {10.0f, 438.0f},
            {92.363976f, 440.182f},
            {102.0f, 362.0f},
            {10.0f, 368.0f}
    };

    public static float[][] HUNDRED = {
            {10.0f, 205.0f},
            {103.0f, 199.0f},
            {89.0f, 104.0f},
            {10.0f, 103.0f}
    };

    public static float[][] TITLE = {{827.0f, 63.31803f}, {827.0f, 125.74426f},
            {1308.0f, 134.6623f},
            {1298.0f, 49.940983f}};

    public static float[][] HUNDRED_LINE_LARGE = {
            {141.0f, 145.0f},
            {141.0f, 175.0f},
            {2037.0f, 175.0f},
            {2037.0f, 145.0f}
    };

    public static float[][] SEVENTYFIVE_LINE_LARGE = {
            {141.0f, 389.0f},
            {141.0f, 419.0f},
            {2037.0f, 419.0f},
            {2037.0f, 389.0f}
    };

    public static float[][] FIFTY_LINE_LARGE = {
            {141.0f, 625.0f},
            {141.0f, 655.0f},
            {2037.0f, 655.0f},
            {2037.0f, 625.0f}
    };

    public static float[][] TWENTYFIVE_LINE_LARGE = {
            {141.0f, 865.0f},
            {141.0f, 895.0f},
            {2037.0f, 895.0f},
            {2037.0f, 865.0f}
    };

    public static float[][] ZERO_LINE_LARGE = {
            {141.0f, 1114.0f},
            {141.0f, 1149.0f},
            {2037.0f, 1149.0f},
            {2037.0f, 1114.0f}
    };

}
