package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class BarTrainingPolygons extends Polygons {


    public static float[][] Q1_YES_BAR = {
            {467.0f, 1179.0f},
            {467.0f, 403.0f},
            {810.0f, 403.0f},
            {810.0f, 1179.0f}
    };

    public static float[][] Q1_NO_BAR = {
            {1321.0f, 1174.0f},
            {1321.0f, 569.0f},
            {1658.0f, 569.0f},
            {1658.0f, 1174.0f}
    };

    public static float[][] TITLE = {{825.0f, 32.99672f}, {790.0f, 131.09508f},
            {1347.0f, 140.0131f},
            {1385.0f, 12.485246f}};


    public static float[][] SEVENTYFIVE_LINE_LARGE = {
            {141.0f, 145.0f},
            {141.0f, 175.0f},
            {2037.0f, 175.0f},
            {2037.0f, 145.0f}
    };

    public static float[][] FIFTY_LINE_LARGE = {
            {141.0f, 485.0f},
            {141.0f, 515.0f},
            {2037.0f, 515.0f},
            {2037.0f, 485.0f}
    };

    public static float[][] TWENTYFIVE_LINE_LARGE = {
            {141.0f, 820.0f},
            {141.0f, 850.0f},
            {2037.0f, 850.0f},
            {2037.0f, 820.0f}
    };

    public static float[][] ZERO_LINE_LARGE = {
            {141.0f, 1153.0f},
            {141.0f, 1183.0f},
            {2037.0f, 1183.0f},
            {2037.0f, 1153.0f}
    };

    public static float[][] FRUIT = {
            {1308.0f, 1304.7081f},
            {1308.0f, 1221.7705f},
            {1699.0f, 1221.7705f},
            {1699.0f, 1304.7081f}
    };

    public static float[][] AXIS_Y = {
            {10.0f, 828.4852f},
            {124.0f, 836.8579f},
            {145.0f, 466.41312f},
            {36.873352f, 449.3559f}
    };

    public static float[][] VEGETABLES = {
            {410.0f, 1304.7081f},
            {410.0f, 1221.7705f},
            {824.0f, 1221.7705f},
            {824.0f, 1304.7081f}
    };


    public static final float[][] INNER = {

    };
}
