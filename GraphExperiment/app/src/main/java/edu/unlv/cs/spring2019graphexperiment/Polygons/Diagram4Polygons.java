package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Diagram4Polygons  extends Polygons {

    public static final float[][] HEAD = {{905.0f, 475.33115f}, {688.59875f, 487.81638f},
            {707.0f, 486.03278f}, {542.0f, 252.38033f},
            {569.0f, 236.32787f}, {941.0745f, 52.616394f},
            {948.0f, 65.99344f}, {1366.0f, 331.75082f},
            {1409.0f, 317.48196f}, {1044.0f, 510.11148f},
            {1044.0f, 494.9508f}, {878.0f, 477.11475f}};


    public static final float[][] TORSO = {{905.0f, 504.76065f}, {998.0f, 511.89508f},
            {995.0f, 522.59674f}, {1199.0f, 475.33115f},
            {1226.0f, 465.5213f}, {1539.7705f, 345.12787f},
            {1564.0f, 340.66885f}, {1769.0f, 379.9082f},
            {1804.0f, 379.0164f}, {1721.0f, 594.1159f},
            {1718.0f, 590.3738f}, {1572.0f, 764.2754f},
            {1591.0f, 742.87213f}, {1455.3557f, 819.40857f},
            {1457.0f, 815.10815f}, {1148.0f, 763.7993f},
            {1156.0f, 768.73444f}, {1009.0f, 727.7115f},
            {1026.0f, 734.8459f}, {893.0f, 716.11804f},
            {923.0f, 707.2f}, {613.0f, 819.5672f},
            {654.0f, 832.9443f}, {365.0f, 840.97046f},
            {369.0f, 865.941f}, {86.0f, 553.8098f},
            {106.0f, 581.45575f}, {35.0f, 429.40326f},
            {73.0f, 420.03934f}, {508.1462f, 439.7894f},
            {506.0f, 465.5213f}, {761.0f, 528.83936f},
            {756.0f, 530.6229f}, {895.0f, 520.8131f}};

    public static final float[][] ABDOMEN = {{927.0f, 733.06226f}, {1027.0f, 739.30493f},
            {1038.0f, 739.30493f}, {1084.0f, 905.1803f},
            {1098.0f, 909.63934f}, {1022.0f, 1171.8295f},
            {1039.0f, 1169.154f}, {941.0f, 1228.9049f},
            {961.0f, 1242.282f}, {842.0f, 1138.723f},
            {856.0f, 1140.6163f}, {785.0f, 943.52783f},
            {798.0f, 945.31146f}, {818.0f, 746.43933f},
            {810.0f, 753.5737f}, {944.7521f, 730.3869f}};

    }
