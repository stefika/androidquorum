package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Floorplan1Polygons extends Polygons{

    public static final float[][] CHILDRENS_EMERGENCY = {
            {71.0f, 1283.0f},
            {986.0f, 1283.0f},
            {986.0f, 952.0f},
            {71.0f, 952.0f}
    };

    public static final float[][] ADULT_EMERGENCY = {
            {72.0f, 919.0f},
            {987.0f, 919.0f},
            {987.0f, 567.0f},
            {72.0f, 567.0f}
    };

    public static final float[][] PATIENT_INTAKE = {
            {73.0f, 386.0f},
            {627.0f, 386.0f},
            {627.0f, 243.0f},
            {73.0f, 243.0f}
    };

    public static final float[][] MEN = {
            {666.0f, 384.0f},
            {803.0f, 384.0f},
            {803.0f, 243.0f},
            {666.0f, 243.0f}
    };

    public static final float[][] WOMEN = {
            {842.0f, 384.0f},
            {978.0f, 384.0f},
            {978.0f, 242.0f},
            {842.0f, 242.0f},
    };

    public static final float[][] ROOM_1 = {
            {1346.0f, 696.0f},
            {1592.0f, 696.0f},
            {1592.0f, 424.0f},
            {1346.0f, 424.0f}
    };

    public static final float[][] ROOM_2 = {
            {1353.0f, 994.0f},
            {1597.0f, 994.0f},
            {1597.0f, 732.0f},
            {1353.0f, 732.0f}
    };

    public static final float[][] ROOM_3 = {
            {1349.0f, 1282.0f},
            {1593.0f, 1282.0f},
            {1593.0f, 1034.0f},
            {1349.0f, 1034.0f}
    };

    public static final float[][] ROOM_4 = {
            {1738.0f, 694.0f},
            {1980.0f, 694.0f},
            {1980.0f, 424.0f},
            {1738.0f, 424.0f}
    };

    public static final float[][] ROOM_5 = {
            {1736.0f, 998.0f},
            {1981.0f, 998.0f},
            {1981.0f, 732.0f},
            {1736.0f, 732.0f},
    };

    public static final float[][] ROOM_6 = {
            {1738.0f, 1282.0f},
            {1979.0f, 1282.0f},
            {1979.0f, 1034.0f},
            {1738.0f, 1034.0f}
    };

    public static final float[][] EXIT = {
            {1386.0f, 251.0f},
            {1456.0f, 251.0f},
            {1456.0f, 178.0f},
            {1386.0f, 178.0f}
    };

    public static final float[][] HALLWAY = {
            {1978.0f, 242.5705f},
            {1978.0f, 387.0426f},
            {1698.0f, 387.0426f},
            {1693.0f, 1281.5212f},
            {1634.0f, 1282.4131f},
            {1634.0f, 387.93442f},
            {1313.0f, 388.82623f},
            {1307.0f, 1280.6295f},
            {1027.0f, 1281.5212f},
            {1028.0f, 535.082f},
            {71.0f, 533.29834f},
            {78.0f, 420.93115f},
            {1020.0f, 423.60654f},
            {1022.0f, 245.2459f}
    };

    public static final float[][] OUTSIDE_CONTOUR = {
            {2027.0f, 200.65573f},
            {2023.0f, 1325.2196f},
            {27.0f, 1320.7606f},
            {27.0f, 197.08852f},
            {1338.0f, 199.76393f},
            {1338.0f, 141.79672f},
            {1498.0f, 129.31148f},
            {1500.0f, 206.00656f}
    };
}

