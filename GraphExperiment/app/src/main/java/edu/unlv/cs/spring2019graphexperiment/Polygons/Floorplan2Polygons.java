package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Floorplan2Polygons extends Polygons{

public static final float[][] KAY_JEWELRY = {
        {312.0f, 693.0f},
        {312.0f, 428.0f},
        {72.0f, 428.0f},
        {72.0f, 693.0f},
};

    public static final float[][] STARBUCKS = {
            {72.0f, 731.0f},
            {72.0f, 994.0f},
            {310.0f, 994.0f},
            {310.0f, 731.0f},
    };

    public static final float[][] ACE = {
            {74.0f, 1037.0f},
            {74.0f, 1280.0f},
            {314.0f, 1280.0f},
            {314.0f, 1037.0f}
    };

    public static final float[][] ZALES = {
            {455.0f, 429.0f},
            {698.0f, 429.0f},
            {698.0f, 696.0f},
            {455.0f, 696.0f}
    };

    public static final float[][] FRESH_START = {
            {456.0f, 728.0f},
            {697.0f, 728.0f},
            {697.0f, 996.0f},
            {456.0f, 996.0f}
    };


    public static final float[][] JARED_JEWELRY = {
            {458.0f, 1029.0f},
            {702.0f, 1029.0f},
            {702.0f, 1280.0f},
            {458.0f, 1280.0f}
    };

    public static final float[][] WATER_FOUNTAIN = {
            {813.0f, 241.0f},
            {1029.0f, 241.0f},
            {1029.0f, 419.0f},
            {813.0f, 419.0f}
    };

    public static final float[][] MEN = {
            {1072.0f, 243.0f},
            {1208.0f, 243.0f},
            {1208.0f, 386.0f},
            {1072.0f, 386.0f}
    };

    public static final float[][] WOMEN = {
            {1247.0f, 241.0f},
            {1387.0f, 241.0f},
            {1387.0f, 384.0f},
            {1247.0f, 384.0f}
    };

    public static final float[][] STORAGE = {
            {1423.0f, 239.0f},
            {1977.0f, 239.0f},
            {1977.0f, 384.0f},
            {1423.0f, 384.0f}
    };

    public static final float[][] DILLARDS = {
            {1067.0f, 565.0f},
            {1980.0f, 565.0f},
            {1980.0f, 912.0f},
            {1067.0f, 912.0f}
    };

    public static final float[][] MACYS = {
            {1061.0f, 947.0f},
            {1976.0f, 947.0f},
            {1976.0f, 1282.0f},
            {1061.0f, 1282.0f}
    };

    public static final float[][] EXIT = {
            {514.0f, 97.0f},
            {747.0f, 97.0f},
            {747.0f, 238.0f},
            {514.0f, 238.0f}
    };

    public static final float[][] OUTSIDE_CONTOUR = {{2025.0f, 199.76393f},
            {2026.0f, 1327.895f},
            {32.0f, 1324.3279f},
            {33.0f, 195.30492f},
            {507.0f, 194.41312f},
            {509.0f, 91.855736f},
            {727.0f, 71.34426f},
            {766.0f, 189.06229f}
    };

    public static final float[][] HALLWAY = {
            {1971.0f, 428.95737f},
            {1971.0f, 529.73114f},
            {1023.0f, 527.0557f},
            {1016.0f, 1277.0623f},
            {747.0f, 1277.0623f},
            {745.0f, 387.0426f},
            {425.0f, 379.0164f},
            {414.0f, 1277.0623f},
            {358.0f, 1276.1705f},
            {360.0f, 389.71802f},
            {75.0f, 380.8f},
            {85.0f, 248.81311f},
            {795.0f, 251.48853f},
            {829.0f, 434.3082f}
    };
}
