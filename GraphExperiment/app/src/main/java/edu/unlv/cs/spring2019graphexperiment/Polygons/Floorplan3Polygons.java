package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Floorplan3Polygons extends Polygons{

    public static final float[][] CAFETERIA = {
            {79.0f, 715.577f},
            {79.0f, 68.66885f},
            {444.0f, 68.66885f},
            {444.0f, 715.577f}
    };

    public static final float[][] OFFICE = {
            {79.0f, 748.0f},
            {79.0f, 945.0f},
            {446.0f, 945.0f},
            {446.0f, 748.0f}};

    public static final float[][] ENTRANCE = {
            {442.0f, 980.5639f},
            {80.0f, 980.0098f},
            {79.0f, 1098.7017f},
            {322.0f, 1300.3835f},
            {442.0f, 1300.1672f}
    };

    public static final float[][] GYMNASIUM = {
            {1428.0f, 388.82623f},
            {1428.0f, 69.56065f},
            {1925.0f, 69.56065f},
            {1925.0f, 388.82623f}
    };

    public static final float[][] COURTYARD = {
            {638.0f, 719.4688f},
            {638.0f, 423.17377f},
            {1724.0f, 423.17377f},
            {1724.0f, 719.4688f}
    };

    public static final float[][] ART_ROOM = {
            {483.0f, 148.93115f},
            {483.0f, 68.66885f},
            {1001.0f, 68.66885f},
            {1001.0f, 148.93115f}
    };

    public static final float[][] SCIENCE_LAB = {
            {1040.0f, 148.60655f},
            {1040.0f, 65.10164f},
            {1393.0f, 65.10164f},
            {1393.0f, 148.60655f}
    };

    public static final float[][] MATHEMATIC_GROUP = {
            {839.0f, 853.15735f},
            {839.0f, 753.4506f},
            {1208.9249f, 753.4506f},
            {1208.9249f, 853.15735f}
    };

    public static final float[][] HOME_ECONOMICS = {
            {1250.0f, 853.69836f},
            {1250.0f, 750.6f},
            {1718.0f, 750.6f},
            {1718.0f, 853.69836f}
    };

    public static final float[][] SOCIAL_STUDIES = {
            {483.0f, 1290.2623f},
            {483.0f, 1210.3114f},
            {867.0f, 1210.3114f},
            {867.0f, 1290.2623f}
    };

    public static final float[][] HISTORY = {
            {906.0f, 1295.4918f},
            {906.0f, 1205.5278f},
            {1283.0f, 1205.5278f},
            {1283.0f, 1295.4918f}
    };

    public static final float[][] WATER_FOUNTAINS = {
            {1326.0f, 1300.1672f},
            {1326.0f, 1175.6525f},
            {1487.0f, 1175.6525f},
            {1487.0f, 1300.1672f}
    };

    public static final float[][] RESTROOMS = {
            {1531.0f, 1300.0f},
            {1531.0f, 1208.0f},
            {1924.8258f, 1208.0f},
            {1924.8258f, 1300.0f}
    };

    public static final float[][] BULLETIN_BOARD = {
            {598.0f, 880.7508f},
            {598.0f, 758.4918f},
            {792.0f, 758.4918f},
            {792.0f, 880.7508f}
    };

    public static final float[][] LOCKERS_1_50 = {
            {489.3268f, 326.69144f},
            {575.0f, 333.53442f},
            {580.0f, 576.1049f},
            {493.0f, 578.0977f}
    };

    public static final float[][] LOCKERS_51_150 = {
            {1922.0f, 1009.9672f},
            {1847.0f, 1018.43933f},
            {1819.0f, 705.4164f},
            {1925.0f, 706.30817f}
    };

    public static final float[][] HALLWAY = {
            {1385.0f, 188.17049f},
            {1390.0f, 386.15082f},
            {603.0f, 387.0426f},
            {583.0f, 313.02295f},
            {485.0f, 306.78033f},
            {489.0f, 185.30492f}
    };

    public static final float[][] HALLWAY2= {
            {491.0f, 888.07214f},
            {490.0f, 1170.2885f},
            {1917.0f, 1170.5049f},
            {1820.0f, 1030.9246f},
            {1813.0f, 888.63934f}
    };

    public static final float[][] OUTER_CONTOUR = {
            {1976.0f, 29.429508f},
            {1979.0f, 1342.164f},
            {301.0f, 1343.9475f},
            {46.0f, 1119.2131f},
            {28.0f, 32.10492f}
    };

}
