package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class FloorplanTrainingPolygons extends Polygons{

    public static final float[][] ELEVATOR = {
            {111.0f, 900.7213f},
            {396.0f, 900.7213f},
            {396.0f, 721.4688f},
            {11.0f, 721.4688f},
    };

    public static final float[][] STAIRS = {
            {329.0f, 351.37048f},
            {565.0f, 351.37048f},
            {565.0f, 159.63278f},
            {329.0f, 159.63278f},
    };

    public static final float[][] ROOM_112 = {
            {600.0f, 445.90164f},
            {887.0f, 445.90164f},
            {887.0f, 160.52458f},
            {600.0f, 160.52458f}
    };

    public static final float[][] ROOM_113 = {
            {930.0f, 443.22623f},
            {1216.0f, 443.22623f},
            {1216.0f, 160.52458f},
            {930.0f, 160.52458f}
    };

    public static final float[][] ROOM_114 = {
            {1263.0f, 442.3344f},
            {1570.0f, 442.3344f},
            {1570.0f, 160.52458f},
            {1263.0f, 160.52458f}
    };

    public static final float[][] ROOM_115 = {
            {1608.0f, 441.44263f},
            {1894.0f, 441.44263f},
            {1894.0f, 160.52458f},
            {1608.0f, 160.52458f}
    };

    public static final float[][] RECEPTION = {
            {1266.0f, 892.69507f},
            {1896.0f, 892.69507f},
            {1896.0f, 634.9639f},
            {1266.0f, 634.9639f}
    };

    public static final float[][] COMMON_ROOM = {
            {606.0f, 1203.0426f},
            {1894.0f, 1205.718f},
            {1895.0f, 933.718f},
            {1224.0f, 930.1508f},
            {1221.0f, 634.9639f},
            {604.0f, 634.9639f}
    };

    public static final float[][] HALLWAY1 = {
            {328.0f, 1195.9082f},
            {563.0f, 1201.259f},
            {563.0f, 336.20984f},
            {330.0f, 395.06885f}
    };

    public static final float[][] HALLWAY2 = {
            {567.0f, 585.02295f},
            {1887.0f, 584.13116f},
            {1895.0f, 485.141f},
            {567.0f, 491.3836f}
    };

    public static final float[][] OUTSIDE_CONTOUR = {
            {280.0f, 133.7705f},
            {281.0f, 672.4197f},
            {79.0f, 682.2295f},
            {74.0f, 938.17706f},
            {282.0f, 939.06885f},
            {279.0f, 1240.4983f},
            {1942.0f, 1240.4983f},
            {1937.0f, 121.28525f},
            {283.0f, 114.15082f}
    };



}

