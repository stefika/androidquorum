package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Map2Polygons extends Polygons {

    public static float[][] YELLOWSTONE_NATL_PARK = {{10.0f, 503.0f}, {406.0f, 501.0f},
            {426.2697f, 10.0f},
            {18.0f, 23.0f}};

    public static final float[][] GRAND_TETON_NATIONAL_PARK = {{367.0f, 661.0f}, {374.0f, 596.0f},
            {375.0f, 562.0288f},
            {323.0f, 535.0f},
            {258.0f, 526.0f},
            {161.0f, 556.0f},
            {108.13068f, 669.8693f},
            {111.0f, 765.0f},
            {162.0f, 777.2493f},
            {236.0f, 791.42285f},
            {352.0f, 796.0f},
            {397.5f, 744.0f},
            {359.53583f, 696.4642f},
            {331.0f, 687.0f},
            {373.0f, 610.0f}};

    public static final float[][] ROCK_SPRINGS = {{622.0f, 1138.0f}, {631.0f, 1225.0f},
            {750.0f, 1205.0f},
            {726.0f, 1102.0f}};

    public static final float[][] CASPER = {{1288.0f, 818.0f}, {1285.0f, 882.0f},
            {1386.0f, 906.0f},
            {1395.0f, 783.0f}};

    public static final float[][] CHEYENNE = {{1776.0f, 1257.0f}, {1759.0f, 1349.0f},
            {1988.9332f, 1323.0f},
            {1963.5f, 1225.0f}};

    public static final float[][] SHERIDAN = {{1307.0f, 22.0f}, {1310.0f, 117.0f},
            {1477.0f, 129.31158f},
            {1494.0f, 26.0f}};

    public static final float[][] TITLE = {{801.0f, 562.0f}, {820.0f, 657.0f},
            {1204.0f, 663.0f},
            {1199.0f, 540.0f}};

}
