package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Map3Polygons extends Polygons{

    public static float[][] MACYS = {{1422.0f, 62.0f}, {1431.0f, 401.0f},
         {1942.0f, 399.0f},
         {1944.0f, 55.0f},
            {1422.0f, 62.0f}};

//    {{1412.0f, 62.0f}, {1417.0f, 419.0f},
//        {1412.0f, 427.0f}, {1935.0f, 431.0f},
//        {1933.0f, 442.0f}, {1943.0f, 53.0f},
//        {1941.0f, 64.0f}, {1416.0f, 63.0f}};

    public static float[][] TORRID = {{1241.0f, 49.0f}, {1230.4366f, 158.0f},
            {1408.0f, 168.0f},
             {1400.0f, 72.0f},
            {1241.0f, 49.0f}};

    public static float[][] LOFT = {{1241.0f, 259.0f}, {1235.0f, 395.0f},
         {1425.0f, 398.0f},
        {1412.0f, 266.5f}};

    public static float[][] BUCKLE = {{1038.0f, 274.0f}, {1033.0f, 404.0f},
             {1238.5f, 407.0f},
             {1217.0f, 279.0f}};

    public static float[][] HOT_TOPIC = {{1051.0f, 35.0f}, {1025.0f, 146.0f},
            {1185.0731f, 155.96347f},
             {1217.0f, 40.0f}};

    public static float[][] CLAIRES = {{840.0f, 26.0f}, {823.0f, 146.0f},
             {1004.4172f, 148.58286f},
             {1010.4523f, 47.0f}};

    public static float[][] AE_OUTFITTERS = {{827.0f, 269.0f}, {820.0f, 392.0f},
            {1017.0f, 402.0f},
             {1025.0f, 278.0f}};

    public static float[][] SUN_SHOP = {{614.0f, 252.0f}, {607.57336f, 400.14673f},
             {827.0f, 399.0f},
             {801.0f, 256.0f},
            };

    public static float[][] PAYLESS_SHOES = {{490.0f, 32.0f}, {466.0f, 144.0f},
             {795.0f, 146.0f},
             {797.0f, 38.0f},
            };

    public static float[][] DILLARDS = {{66.0f, 44.0f}, {77.0f, 721.0f},
            {434.0f, 725.0f},
             {459.0f, 63.0f}};

    public static float[][] STARBUCKS_COFFEE = {{76.0f, 740.0f}, {69.0f, 968.0f},
            {447.97504f, 960.90015f},
            {459.0f, 743.0f},
            {80.0f, 738.0f}};

    public static float[][] BARNES_AND_NOBLE = {{77.0f, 985.0f}, {77.0f, 1146.0f},
            {313.0f, 1318.0f},
            {457.0f, 1315.5f},
            {455.0f, 985.0f}};

    public static float[][] JOURNEYS = {{616.0f, 758.0f}, {614.0f, 880.0f},
            {796.0159f, 871.0f},
            {800.0f, 734.0f}};

    public static float[][] BATH_AND_BODY_WORKS = {{470.0f, 1183.0f}, {466.0f, 1309.0f},
            {662.0f, 1311.0f},
            {679.3658f, 1198.6343f},
            {474.0f, 1203.0f}};

    public static float[][] CANDY_SHOPPE = {{815.0f, 734.0f}, {804.0f, 867.0f},
            {1020.0f, 876.0f},
             {1030.0f, 729.0f},
            };

    public static float[][] PANDORA = {{693.0f, 1192.0f}, {684.0f, 1310.0f},
            {883.0f, 1306.0f},
            {885.0f, 1208.0f}};

    public static float[][] BUILD_A_BEAR = {{905.0f, 1193.0f}, {895.0f, 1317.0f},
            {1095.0f, 1314.0f},
            {1084.0f, 1198.0f}};

    public static float[][] KAY_JEWELERS = {{1055.0f, 727.0f}, {1036.0f, 856.0f},
            {1239.0f, 878.0f},
            {1229.0f, 742.0f}
            };

    public static float[][] HOLLISTER_CO = {{1109.0f, 1193.0f}, {1098.0f, 1309.0f},
            {1274.0f, 1312.0f},
            {1303.3552f, 1204.6448f}
            };

    public static float[][] FOREVER21 = {{1233.0f, 749.0f}, {1239.0f, 872.0f},
            {1527.0f, 876.0f},
            {1524.1075f, 744.0f}
    };

    public static float[][] SEPHORA = {{1551.0f, 729.0f}, {1546.0f, 856.0f},
            {1758.2556f, 855.8722f},
            {1743.1265f, 720.0f}
};

    public static float[][] EARTHBOUND = {{1312.0f, 1186.0f}, {1307.0f, 1307.0f},
           {1510.0f, 1314.0f},
           {1500.0f, 1193.0f}
};

    public static float[][] YANKEE_CANDLE = {{1749.0f, 731.0f}, {1746.0f, 874.1533f},
           {1929.0f, 870.0f}, {1943.0f, 749.0f},
            };

    public static float[][] RESTROOMS = {{1515.0f, 1189.0f}, {1509.0f, 1310.0f},
            {1933.0f, 1312.0f},
            {1937.0f, 1195.0f},
            };
}
