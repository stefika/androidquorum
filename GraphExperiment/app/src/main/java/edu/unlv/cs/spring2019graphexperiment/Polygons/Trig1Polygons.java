package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Trig1Polygons extends Polygons{

    public static final float[][] XY = {
            {1723.0f, 790.0f},
            {1713.0f, 1146.5f},
            {1676.0f, 1140.0f},
            {1674.0f, 810.6003f}
    };

    public static final float[][] YZ = {
            {1671.0f, 1132.0f},
            {1674.0f, 1173.0f},
            {441.0f, 1174.0f},
            {456.0f, 1123.0f}
    };

    public static final float[][] ZW = {
            {447.0f, 1137.0f},
            {382.0f, 1140.0f},
            {388.0f, 331.0f},
            {443.0f, 296.0f}
    };

    public static final float[][] WX = {
            {454.0f, 311.0f},
            {487.0f, 264.0f},
            {1688.0f, 733.0f},
            {1668.0f, 759.0f}
    };

    public static final float[][] W ={
            {390.0f, 323.0f},
            {482.0f, 265.0f},
            {399.0f, 228.0f}
    };

    public static final float[][] X = {
            {1666.0f, 794.0f},
            {1714.5f, 786.0f},
            {1717.0f, 742.0f},
            {1690.0f, 739.0f},
            {1656.0f, 773.0f}
    };

    public static final float[][] Y = {
            {1676.0f, 1175.0f},
            {1713.5f, 1177.0f},
            {1716.0f, 1140.0f},
            {1678.0f, 1135.0f}
    };

    public static final float[][] Z = {
            {437.0f, 1129.0f},
            {431.0f, 1176.0f},
            {383.0f, 1183.0f},
            {379.0f, 1129.0f}
    };

    public static final float[][] Y_RIGHT_ANGLE = {
            {1596.0f, 1135.0f},
            {1674.0f, 1135.0f},
            {1675.0f, 1071.0f},
            {1598.0f, 1069.0f}
    };

    public static final float[][] Z_RIGHT_ANGLE = {
            {434.0f, 1133.0f},
            {505.0f, 1139.4222f},
            {507.0f, 1071.0f},
            {424.0f, 1058.0f}
    };

    public static final float[][] INNER = {
            {435.0f, 1133.4819f},
            {1674.0f, 1133.9279f},
            {1678.6155f, 778.54425f},
            {431.0f, 294.29507f}
    };
}
