package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Trig2Polygons extends Polygons {

    public static final float[][] A = {
            {1223.0f, 360.0f},
            {1358.0f, 359.0f},
            {1321.0f, 310.0f}
    };

    public static final float[][] B = {
            {1787.0f, 944.0f},
            {1707.0f, 834.0f},
            {1708.0f, 943.0f}
    };

    public static final float[][] C = {
            {223.0f, 941.0f},
            {363.0f, 941.0f},
            {358.0f, 863.0f}
    };

    public static final float[][] A_LINE = {
            {360.0f, 943.0f},
            {1702.0f, 945.0f},
            {1702.0f, 907.0f},
            {360.0f, 907.0f}};

    public static final float[][] B_LINE ={
            {363.0f, 858.0f},
            {1224.0f, 366.0f},
            {1287.0f, 369.0f},
            {363.0f, 902.0f}};

    public static final float[][] C_LINE = {
            {1363.0f, 364.0f},
            {1704.0f, 838.0f},
            {1704.0f, 895.0f},
            {1318.0f, 367.0f}
    };

    public static final float[][] A_RIGHT_ANGLE = {
            {1310.0f, 360.0f},
            {1352.0f, 419.0f},
            {1284.0f, 456.0f},
            {1243.0f, 394.0f}
    };

    public static final float[][] B_ANGLE = {
            {1700.0f, 900.0f},
            {1574.0f, 731.0f},
            {1511.0f, 897.0f}
    };

    public static final float[][] C_ANGLE = {
            {371.0f, 901.0f},
            {600.0f, 764.0f},
            {602.0f, 906.0f}
    };

    public static final float[][] INNER = {
            {364.0f, 915.88196f},
            {1303.0f, 364.74753f},
            {1704.0f, 910.5311f}
    };
}
