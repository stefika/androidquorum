package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class Trig3Polygons extends Polygons {

    public static final float[][] SMALL_A = {{1323.0f, 128.0f}, {1371.0f, 109.0f},
            {1753.0f, 625.0f},
            {1740.0f, 670f},
            {1346.0f, 142.0f}};

    public static final float[][] SMALL_B = {{1729.0f, 723.0f}, {1751.0f, 752.0f},
            {1371.0f, 1276.0f},
            {1339.0f, 1259.0f},
            {1714.0382f, 738.0f}};

    public static final float[][] SMALL_D = {
            {395.0f, 713.0f},
            {378.3561f, 749.0f},
            {1315.7583f, 1289.2417f},
            {1326.0f, 1260.0f}};

    public static final float[][] SMALL_C = {{1323.0f, 91.0f}, {1337.0f, 120.0f},
             {385.0f, 680.0f},{381.0f, 638.0f}};

    public static final float[][] BIG_A = {{402.0f, 672.0f}, {744.3675f, 481.0f},
            {753.0f, 662.0f}};

    public static final float[][] BIG_B = {{388.0f, 708.0f}, {756.0f, 719.0f},
            {657.0f, 862.0f}};

    public static final float[][] BIG_C = {{1494.0f, 669.0f}, {1734.0f, 671.0f},
            {1605.0f, 507.77618f}};

    public static final float[][] BIG_D = {{1724.0f, 716.0f}, {1472.0f, 719.0f},
            {1592.0f, 884.0f}};

    public static final float[][] SMALL_E = {{385, 670.0f}, {385.0f, 708.1823f},
            {1732.0f, 710.011f},
            {1732.0f, 675.0f}};

    public static final float[][] A_C_VERTEX = {{1344.0f, 76.0f},
            {1374.8177f, 109.0f},
            {1338.0f, 121.0f},
            {1314.0f, 97.0f}};

    public static final float[][] A_B_E_VERTEX = {{1774.0f, 634.0f}, {1737.0138f, 687.0414f},
            {1746.0f, 745.0f},
            {1766.1298f, 757.0f},
            {1806.0f, 691.0f}};

    public static final float[][] C_D_E_VERTEX = {{270.0f, 694.0f}, {376.0f, 639.0f},
            {375.0f, 711.0f},
            {360.0f, 758.1016f}};


    public static final float[][] B_D_VERTEX = {{1339.0f, 1262.0f},
            {1368.0f, 1287.0f},
            {1347.0f, 1307.0f},
            {1315.0f, 1293.0f}};

    public static final float[][] NINETY_TOP = {{1264.0f, 169.0f}, {1323.0f, 222.0f},
            {1380.0f, 202.0f},
            {1335.0f, 125.0f}};

    public static final float[][] NINETY_BOTTOM = {{1267.0f, 1221.0f}, {1340.0f, 1255.0f},
            {1377.0f, 1199.0f},
            {1316.0f, 1172.0f}};

    public static final float[][] INNER = {
            {385.0f, 689.36395f}, {1335.2213f, 140.0131f},
                    {1746.0f, 695.60657f},
                    {1334.0f, 1268.1443f},
                    {376.14905f, 715.49207f}
    };
}
