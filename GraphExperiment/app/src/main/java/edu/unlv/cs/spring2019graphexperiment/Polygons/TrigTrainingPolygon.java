package edu.unlv.cs.spring2019graphexperiment.Polygons;

public class TrigTrainingPolygon extends Polygons{

   public static float[][] SMALL_A = {
           {139.0f, 1236.9312f},
           {140.0f, 1186.9901f},
           {1918.0f, 1191.4492f},
           {1921.0f, 1239.6066f}
   };

    public static float[][] SMALL_B = {
            {1917.0f, 1183.423f},
            {1027.0f, 131.09508f},
            {1089.0f, 134.6623f},
            {1924.0f, 1123.6721f}
    };

    public static float[][] SMALL_C = {
            {140.0f, 1186.0984f},
            {137.0f, 1116.5377f},
            {968.0f, 131.98688f},
            {1021.0f, 135.5541f}
    };

    public static float[][] VERTEX_B = {
            {140.5f, 1239.1606f},
            {44.0f, 1239.6066f},
            {140.0f, 1124.564f}
    };

    public static float[][] VERTEX_A = {
            {968.0f, 130.20328f},
            {1087.0f, 131.09508f},
            {1025.0f, 63.31803f}
    };

    public static float[][] VERTEX_C = {
            {1919.0f, 1239.6066f},
            {1915.0f, 1120.9967f},
            {2016.0f, 1235.1475f}
    };

    public static float[][] ANGLE_B = {
            {142.0f, 1186.0984f},
            {412.0f, 886.45245f},
            {556.85504f, 1182.6604f}
    };

    public static float[][] ANGLE_A = {
            {1022.0f, 132.8787f},
            {850.0f, 354.0459f},
            {1204.0f, 354.0459f}
    };

    public static float[][] ANGLE_C = {
            {1911.0f, 1186.0984f},
            {1560.0f, 1184.3147f},
            {1704.0f, 942.63605f}
    };
    public static final float[][] INNER = {
            {146.0f, 1184.3147f},
            {1025.0f, 132.8787f},
            {1914.0f, 1185.2065f}
    };
}
