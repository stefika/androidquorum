package edu.unlv.cs.spring2019graphexperiment;

import android.graphics.Bitmap;

public class TouchManagement {

    public int touchRadius = 2;

    public void setTouchRadius(int touchRadius) {
        this.touchRadius = touchRadius;
    }

    public int getTouchRadius() {
        return touchRadius;
    }

    public boolean circleIsNotOnly(int color, int h, int k, Bitmap bitmap) {
        // radius in pixels
        int radius = touchRadius;
        int radiusSquared = radius * radius;

        for (int x = h-radius; x < h+radius; x++)
        {
            for (int y = k-radius; y < k+radius; y++)
            {
                double dx = x - h;
                double dy = y - k;
                double distanceSquared = dx * dx + dy * dy;

                if (distanceSquared <= radiusSquared)
                {
                    int pixelColor = bitmap.getPixel(x,y);
                    if (pixelColor != color) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean circleContains(int color, int h, int k, Bitmap bitmap) {
        // radius in pixels
        int radius = touchRadius;
        int radiusSquared = radius * radius;

        for (int x = h-radius; x < h+radius; x++)
        {
            for (int y = k-radius; y < k+radius; y++)
            {
                double dx = x - h;
                double dy = y - k;
                double distanceSquared = dx * dx + dy * dy;

                if (distanceSquared <= radiusSquared)
                {
                    if (y < 0 || x < 0) {
                        return false;
                    }
                    if (y >= bitmap.getHeight() || x >= bitmap.getWidth()){
                        return false;
                    }
                    int pixelColor = bitmap.getPixel(x,y);
                    if (pixelColor == color) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
