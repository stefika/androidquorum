package edu.unlv.cs.spring2019graphexperiment;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class TreeNode<T> implements Iterable<TreeNode<T>> {

    T data;
    TreeNode<T> parent;
    List<TreeNode<T>> children;
    int color = -1;

    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedList<TreeNode<T>>();
    }

    public TreeNode(T data, int color) {
        this.data = data;
        this.color = color;
        this.children = new LinkedList<TreeNode<T>>();
    }

    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parent = this;
        this.children.add(childNode);
        return childNode;
    }

    public TreeNode<T> addChild(T child, int color) {
        TreeNode<T> childNode = new TreeNode<T>(child, color);
        childNode.parent = this;
        this.children.add(childNode);
        return childNode;
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        return null;
    }


    public boolean hasParent() {
        if (parent != null) {
            return true;
        }
        return false;
    }

    public TreeNode<T> getFirstChild() {
        if (hasChildren()) {
            return children.get(0);
        }
        return null;
    }

    public boolean hasChildren () {
        if (children.size() > 0) {
            return true;
        }
        return false;
    }

    public T getData() {
        return data;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public TreeNode<T> previousChild(T child) {
        if (children.size() == 0) {
            return null;
        }
        if (children.size() == 1){
            return children.get(0);
        }
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).getData().equals(child)) {
                if (i == 0) {
                    return children.get(children.size()-1);
                }
                return children.get(i-1);
            }
        }
        return null;
    }

    public TreeNode<T> nextChild(T child) {
        if (children.size() == 0) {
            return null;
        }
        if (children.size() == 1){
            return children.get(0);
        }
        for (int i = 0; i < children.size(); i++) {
            if (children.get(i).getData().equals(child)) {
                if (i == children.size()-1) {
                    return children.get(0);
                }
                return children.get(i+1);
            }
        }
        return null;
    }

}
