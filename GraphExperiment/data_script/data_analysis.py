import csv
import os
import time 
from datetime import datetime

def extractData(path, filename, output):
    file_to_read = os.path.join(path, filename)
    print("File: ", file_to_read)
    with open(file_to_read) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        writer = csv.writer(output, delimiter=',')
        line_count = 0
        previous_pid = -1
        previous_groupid = -1
        previous_sessionid = -1
        previous_slide = -1
        count = 0
        start_timestamp = datetime.fromtimestamp(0)
        previous_timestamp = datetime.fromtimestamp(0)
        for row in csv_reader:
            if line_count == 0:
                writer.writerow(['pid', 'groupid', 'sessionid', 'slide', 'time_difference'])
                line_count += 1
            else:
                uuid = row[0]
                timestamp = row[1]
                converted_time = datetime.fromtimestamp(int(timestamp)/1000.0)
                pid = row[2]
                groupid = row[3]
                sessionid = row[4]
                slidenumber = row[5]
                event = row[6]
                voicearea = row[7]
                action = row[8]
                x = row[9]
                y = row[10]
                if previous_pid != pid or previous_slide != slidenumber:
                    time_difference = previous_timestamp - start_timestamp
                    start_timestamp = converted_time
                    if int(previous_pid) > -1:
                        writer.writerow([previous_pid, previous_groupid, previous_sessionid, previous_slide, time_difference.total_seconds()])
                    count = count + 1 
                    previous_pid = pid
                    previous_slide = slidenumber
                    previous_groupid = groupid
                    previous_sessionid = sessionid
                previous_timestamp = converted_time
                #if previous_slide != 
                line_count += 1
        time_difference = previous_timestamp - start_timestamp
        #print ("pid: ", previous_pid, " slidenumber: ", previous_slide, " time difference: ", time_difference)
        writer.writerow([previous_pid, previous_groupid, previous_sessionid, previous_slide, time_difference.total_seconds()])
        count = count + 1 
    print(f'Processed {line_count} lines.')
    print(f'Written {count} new lines.')

path = "data"
output_file = 'alltimes.csv'
with open(output_file, 'w', newline='') as output:
    for file in os.listdir(path):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"):
            extractData(path, filename, output)
            continue
        else:
            continue