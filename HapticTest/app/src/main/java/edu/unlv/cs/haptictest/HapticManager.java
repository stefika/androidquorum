package edu.unlv.cs.haptictest;

import android.os.Vibrator;
import android.util.Log;

/**
 * Created by merlin on 6/22/17.
 */

public class HapticManager {

    Vibrator vibrator;
    int timeslice;

    private static final String TAG = "HapticManager";

    public HapticManager(Vibrator vibrator) {
        this.vibrator = vibrator;
        timeslice = 20;
    }

    public HapticManager(Vibrator vibrator, int timeslice) {
        this.vibrator = vibrator;
        this.timeslice = timeslice;
    }

    public void stop() {
        if (vibrator.hasVibrator()){
            vibrator.cancel();
        }

    }

    public void playPattern(long[] pattern) {
        vibrator.vibrate(pattern, -1);
    }

    public void playPattern(long[] pattern, int repeat) {
        logVibrationPattern(pattern);
        if (vibrator.hasVibrator()) {
            vibrator.vibrate(pattern, repeat);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }

    public void playGeneratedPattern(float intensity, long duration) {
        playGeneratedPattern(intensity, duration, -1);
    }

    public void playGeneratedPattern(float intensity, long duration, int repeat) {
        long[] pattern = generateHapticPattern(intensity, duration);
        logVibrationPattern(pattern);
        vibrator.vibrate(pattern, repeat);
    }

    /**************************************************************************************
     * Linear Pattern
     **************************************************************************************/

    public void playLinearPattern(long duration) {
        playLinearPattern(duration, timeslice);
    }

    public void playLinearPattern(long duration, int timeslice) {
        long[] fullPattern = {};

        if (vibrator.hasVibrator()) {

            for (long i = 0; i <= duration / timeslice; i++) {

                float intensity = (float) line(i * timeslice, duration);
                long[] pattern = generateHapticPattern(intensity, timeslice);

                logVibrationPattern(pattern);
                if (pattern.length % 2 != 0) {
                    Log.e("CycleCount", "Half a cycle found!");
                }

                fullPattern = concat(fullPattern, pattern);
                fullPattern = addPause(fullPattern);
            }

            logVibrationPattern(fullPattern);
            vibrator.vibrate(fullPattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }

    public void playLinearPattern(long duration, int timeslice, int ratiocalc) {
        long[] fullPattern = {};

        if (vibrator.hasVibrator()) {

            for (long i = 0; i <= duration / timeslice; i++) {

                float intensity = (float) line(i * timeslice, duration);
                long[] pattern = generateHapticPattern(intensity, timeslice, ratiocalc, 1);

                logVibrationPattern(pattern);
                if (pattern.length % 2 != 0) {
                    Log.e("CycleCount", "Half a cycle found!");
                }

                fullPattern = concat(fullPattern, pattern);
                fullPattern = addPause(fullPattern);
            }

            logVibrationPattern(fullPattern);
            vibrator.vibrate(fullPattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }


    public void playLinearPatternStep(int i, long duration) {
        playLinearPatternStep(i, duration, timeslice);
    }

    public void playLinearPatternStep(int i, long duration, int timeslice) {
        if (vibrator.hasVibrator()) {

            float intensity = (float) line(i * timeslice, duration);
            long[] pattern = generateHapticPattern(intensity, timeslice);

            logVibrationPattern(pattern);
            if (pattern.length % 2 != 0) {
                Log.e("CycleCount", "Half a cycle found!");
            }

            vibrator.vibrate(pattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }


    /**************************************************************************************
     * Wave Pattern
     **************************************************************************************/

    public void playWavePattern(long duration) {
        playWavePattern(duration, timeslice);
    }

    public void playWavePatternOld(long duration) {
        playWavePatternOld(duration, timeslice);
    }

    public void playWavePatternOld(long duration, int timeslice) {

        long[] fullPattern = {};
        if (vibrator.hasVibrator()) {
            for (long i = 0; i <= duration / timeslice; i++) {

                float intensity = (float) pulse(i * timeslice, duration / 1000);
                Log.v("Intensity", "Int: " + intensity);
                long[] pattern = generateHapticPattern(intensity, timeslice, 2, 0);

                logVibrationPattern(pattern);
                if (pattern.length % 2 != 0) {
                    Log.e("CycleCount", "Half a cycle found!");
                }

                fullPattern = concat(fullPattern, pattern);
                //             fullPattern = addPause(fullPattern);
            }
            logVibrationPattern(fullPattern);
            vibrator.vibrate(fullPattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }


    public void playWavePattern(long duration, int timeslice) {

        long[] fullPattern = {};
        if (vibrator.hasVibrator()) {
            for (long i = 0; i <= duration / timeslice; i++) {

                float intensity = (float) pulse(i * timeslice, duration / 1000);
                Log.v("Intensity", "Int: " + intensity);
                long[] pattern = generateHapticPattern(intensity, timeslice);

                logVibrationPattern(pattern);
                if (pattern.length % 2 != 0) {
                    Log.e("CycleCount", "Half a cycle found!");
                }

                fullPattern = concat(fullPattern, pattern);
   //             fullPattern = addPause(fullPattern);
            }
            logVibrationPattern(fullPattern);
            vibrator.vibrate(fullPattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }

    public void playWavePatternStep(int i, long duration) {
        playWavePatternStep(i, duration, timeslice);
    }

    public void playWavePatternStep(int i, long duration, int timeslice) {

        if (vibrator.hasVibrator()) {

            float intensity = (float) pulse(i * timeslice, duration / 1000);
            long[] pattern = generateHapticPattern(intensity, timeslice);

            logVibrationPattern(pattern);
            if (pattern.length % 2 != 0) {
                Log.e("CycleCount", "Half a cycle found!");
            }

            vibrator.vibrate(pattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }

    /**************************************************************************************
     * Exponential Pattern
     **************************************************************************************/
    public void playExponentialPattern(int duration) {
        playWavePattern(duration, timeslice);
    }


    public void playExponentialPattern(long duration, int timeslice) {

        long[] fullPattern = {};
        if (vibrator.hasVibrator()) {
            for (long i = 0; i <= duration / timeslice; i++) {

                float intensity = (float) exponential(i * timeslice, duration);
                long[] pattern = generateHapticPattern(intensity, timeslice);

                logVibrationPattern(pattern);
                if (pattern.length % 2 != 0) {
                    Log.e("CycleCount", "Half a cycle found!");
                }

                fullPattern = concat(fullPattern, pattern);
                fullPattern = concat(fullPattern, new long[]{15l, 0l});
            }
            logVibrationPattern(fullPattern);
            vibrator.vibrate(fullPattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }

    public void playExponentialPatternStep(int i, long duration) {
        playExponentialPatternStep(i, duration, timeslice);
    }

    public void playExponentialPatternStep(int i, long duration, int timeslice) {

        if (vibrator.hasVibrator()) {

            float intensity = (float) exponential(i * timeslice, duration);
            long[] pattern = generateHapticPattern(intensity, timeslice);

            logVibrationPattern(pattern);
            if (pattern.length % 2 != 0) {
                Log.e("CycleCount", "Half a cycle found!");
            }

            vibrator.vibrate(pattern, -1);
        } else {
            Log.v(TAG, "No Vibration Device!");
        }
    }


    public long[] generateHapticPattern(float intensity, long duration) {
        return generateHapticPattern(intensity, duration, 2,1);
    }

    public long[] generateHapticPattern(float intensity, long duration, int ratiocalc, int cyclenumcalc) {

        // edge cases and wrong intensities
        if (intensity >= 1.0f) {
            long[] pattern = new long[2];
            pattern[0] = 0;
            pattern[1] = duration - 1;
            return pattern;
        } else if (intensity <= 0.0f) {
            long[] pattern = new long[2];
            pattern[0] = duration;
            pattern[1] = 0;
            return pattern;
        }

        double ratio = 0;
        int numberOfCycles = 0;
        if (cyclenumcalc == 0) {
            numberOfCycles = numberOfCyclesOld(intensity, duration);
        } else {
            numberOfCycles = numberOfCycles(intensity, duration);
        }

        System.out.println(numberOfCycles);
        if (ratiocalc == 0) {
            ratio = linearRatio(intensity);
        } else if (ratiocalc == 1) {
            ratio = initialRatio(intensity);
        } else if (ratiocalc == 2) {
            ratio = upCurveRatio(intensity);
        }

        double first = firstElement(duration, numberOfCycles, ratio);
        double second = secondElement(ratio, first);
        //Log.v("Vibration Function" , "" + first + ", " + second + "; ");
        long[] pattern = new long[numberOfCycles * 2];
        for (int i = 0; i < numberOfCycles * 2; i = i + 2) {

            pattern[i] = BiasedRound(first);
            pattern[i + 1] = BiasedRound(second);
        }

        return pattern;
    }

    private double secondElement(double ratio, double first) {
        return ratio * first;
    }

    private double firstElement(long duration, int numberOfCycles, double ratio) {
        return duration / (numberOfCycles * (1 + ratio));
    }

    private double linearRatio(float intensity) {
        return intensity;
    }

    private double upCurveRatio(float intensity) {
        return 20 * intensity / (1 + intensity);
    }

    private double initialRatio(float intensity) {
        return 1 / (1.000001 - intensity) - 1;
    }

    private int numberOfCycles(float intensity, long duration) {

/*        System.out.println("intensity: " + intensity + " duration: " + duration);
        float division = duration / 8;
        System.out.println("division: " + division);
        double abs = intensity - 0.5;

        double subtractor = (duration / 4 - 2) * (2*abs*abs);
        System.out.println("subtractor: " + subtractor);
        int result = (int) (division - subtractor);
        System.out.println("result: " + result);
        if (result <= 1) {
            return 1;
        }
*/
        int result = 1;

        if ((intensity > 0.25 && intensity <= 0.4)|| intensity > 0.6 && intensity <= 0.75) {
            result = 2;
        } else if (intensity > 0.4 && intensity <= 0.6) {
            result = 3;
        }

        return result;
    }

    private int numberOfCyclesOld(float intensity, long duration) {

        System.out.println("intensity: " + intensity + " duration: " + duration);
        float division = duration / 2;
        System.out.println("division: " + division);
        double abs = intensity - 0.5;

        double subtractor = (duration  - 2) * Math.abs(abs);
        System.out.println("subtractor: " + subtractor);
        int result = (int) (division - subtractor);
        System.out.println("result: " + result);
        if (result <= 1) {
            return 1;
        }

        return result;
    }


    /**************************************************************************************
     * Function Calculation
     **************************************************************************************/

    double pulse(double time, long duration) {
        final double pi = 3.14;
        final double frequency = 1d / duration; // Frequency in Hz
        return 1.0 * (1 + Math.sin(2 * pi * frequency * time));
    }

    double line(double time, long duration) {
        return time / duration;
    }

    double exponential(double time, long duration) {
        double result = (Math.pow(Math.E, 3 * (time / duration)) - 1) / 20;
        // Log.v("Moep", "Res: " + result + " time/duration: " + time/duration);
        return result;
    }

    /**************************************************************************************
     * Helpers
     **************************************************************************************/

    private long BiasedRound(double number) {
        return Math.round(number);
    }

    public long[] concat(long[] a, long[] b) {
        int aLen = a.length;
        int bLen = b.length;
        long[] c = new long[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    private void logVibrationPattern(long[] pattern) {
        String t = "";
        for (int i = 0; i < pattern.length; i++) {
            if (i < pattern.length - 1) {
                t += pattern[i] + ", ";
            } else {
                t += pattern[i];
            }
        }
        Log.v(TAG, "Playing Vibration: " + t);
    }

    private void logVariables(int timeslice, float intensity, long duration, long i) {
        Log.v(TAG, "timeslice: " + timeslice + ", intensity: " + intensity + ", duration: " + duration + ", i: " + i);
    }

    /*
    This seems to make the changes in intensity easier to distinguish
     */
    private long[] addPause(long[] pattern) {
        return concat(pattern, new long[]{10l, 0l});
    }


}
