package edu.unlv.cs.haptictest;

import android.content.Context;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Intensity test settings
    static final long[] LONGERHIGH = {0, 1000000};
    static final long[] LONGERLOWER = {1, 5, 500, 5, 500, 5};
    static final long[] LONGERLOW = {0, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25};
    static final long[] HIGH = {0, 50, 1, 50};
    static final long[] LOWER = {0, 10, 5, 10, 5, 10, 5, 10};
    static final long[] LOW = {0, 1, 100, 1, 100, 1, 100, 1, 100};
    static final long[] SHORTHIGH = {1, 12, 1};
    static final long[] SHORTLOWER = {0, 2000, 50, 50, 50, 50};
    static final long[] SHORTLOW = {0, 50, 50, 50, 50, 50, 50, 50, 50, 50};
    static final long[] HIGHLOW = {10, 1, 10, 1, 0};
    static final long[] HIGHMID = {10, 5, 10, 5, 0};
    private static final String TAG = "Vibrations";

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    long[][] vibrationPattern = {LONGERHIGH, LONGERLOWER, LONGERLOW, HIGH, LOWER, LOW, SHORTHIGH, SHORTLOWER, SHORTLOW, HIGHLOW, HIGHMID};
    EditText intensityInput;
    EditText durationInput;
    HapticManager manager;
    long lastTime = System.currentTimeMillis();
    Vibrator vibrator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
        final Button button1 = (Button) findViewById(R.id.b1);

        intensityInput = (EditText) findViewById(R.id.intensity);
        durationInput = (EditText) findViewById(R.id.duration);

        manager = new HapticManager(vibrator, 100);
        setUpButtons();

    }

    public void setUpButtons() {

        final Button button1 = (Button) findViewById(R.id.b1);
        button1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {

                long currentTime = System.currentTimeMillis();
                // wait half a second before playing because onTouch constantly fires
                if (currentTime - lastTime < 500) {
                    return true;
                }

                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    if (vibrator.hasAmplitudeControl()) {
                        VibrationEffect ve = VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE);
                        System.out.println("Default amplitude: " + VibrationEffect.DEFAULT_AMPLITUDE);
                        vibrator.vibrate(ve);
                    } else {
                        System.out.println("No amplitude control");
                    }
                } else{
                    System.out.println("Not O");

                    manager.playPattern(LONGERHIGH, 0);
                }


                return false;
            }
        });

        final Button button2 = (Button) findViewById(R.id.b2);
        button2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {

                long currentTime = System.currentTimeMillis();
                // wait half a second before playing because onTouch constantly fires
                if (currentTime - lastTime < 500) {
                    return true;
                }

                manager.stop();
                return true;
            }
        });

        final Button button3 = (Button) findViewById(R.id.b3);
        button3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(LONGERLOW);
                return true;
            }
        });

        final Button button4 = (Button) findViewById(R.id.b4);
        button4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(HIGH);
                return true;
            }
        });

        final Button button5 = (Button) findViewById(R.id.b5);
        button5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(LOWER);
                return true;
            }
        });

        final Button button6 = (Button) findViewById(R.id.b6);
        button6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(LOW);
                return true;
            }
        });

        final Button button7 = (Button) findViewById(R.id.b7);
        button7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(SHORTHIGH);
                return true;
            }
        });

        final Button button8 = (Button) findViewById(R.id.b8);
        button8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(SHORTLOWER);
                return true;
            }
        });

        final Button button9 = (Button) findViewById(R.id.b9);
        button9.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                playPattern(SHORTLOW);
                return true;
            }
        });

        final Button button10 = (Button) findViewById(R.id.b10);
        button10.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 2000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 2000 / 20) {
                        i = 0;
                    }
                    manager.playWavePatternOld(2000, 50);
                    i++;
                }
                return true;
            }
        });

        final Button button11 = (Button) findViewById(R.id.b11);
        button11.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 2000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 2000 / 20) {
                        i = 0;
                    }
                    manager.playWavePattern(2000, 50);
                    i++;
                }
                return true;
            }
        });

        final Button button12 = (Button) findViewById(R.id.b12);
        button12.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 1000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 2000 / 20) {
                        i = 0;
                    }
                    manager.playExponentialPattern(2000, 20);
                    i++;
                }
                return true;
            }
        });

        final Button button13 = (Button) findViewById(R.id.b13);
        button13.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                manager.playGeneratedPattern(getIntensityFromInput(), getDurationFromInput());
                return true;
            }
        });

        final Button button14 = (Button) findViewById(R.id.b14);
        button14.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 1000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 3000 / 20) {
                        i = 0;
                    }
                    manager.playWavePattern(1000, 1000);
                    i++;
                }
                return true;
            }
        });

        final Button button15 = (Button) findViewById(R.id.b15);
        button15.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 1000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 1000 / 20) {
                        i = 0;
                    }
                    manager.playLinearPattern(1000, 20, 2);
                    i++;
                }
                return true;
            }
        });

        final Button button16 = (Button) findViewById(R.id.b16);
        button16.setOnTouchListener(new View.OnTouchListener() {
            int i = 0;

            public boolean onTouch(View arg0, MotionEvent arg1) {
                long currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 1000) {
                    lastTime = System.currentTimeMillis();
                    if (i > 1000 / 20) {
                        i = 0;
                    }
                    manager.playLinearPattern(1000, 20, 1);
                    i++;
                }
                return true;
            }
        });
    }

    public float getIntensityFromInput() {
        String s = intensityInput.getText().toString();
        try {
            float intensity = Float.parseFloat(s);
            checkIntensityRangeViolated(intensity);
            return intensity;
        } catch (Exception e) {
            intensityInput.setText("1.0", TextView.BufferType.EDITABLE);
            return 1.0f;
        }
    }

    private void checkIntensityRangeViolated(float intensity) throws Exception {
        if (intensity > 1.0 || intensity < 0.0) {
            throw new Exception("Intensity between 0.0 and 1.0 required");
        }
    }

    public int getDurationFromInput() {
        String s = durationInput.getText().toString();
        try {
            int intensity = Integer.parseInt(s);
            return intensity;
        } catch (Exception e) {
            durationInput.setText("20", TextView.BufferType.EDITABLE);
            return 20;
        }
    }

    public void playPattern(long[] pattern) {
        long currentTime = System.currentTimeMillis();
        // wait half a second before playing because onTouch constantly fires
        if (currentTime - lastTime < 500) {
            return;
        }
        manager.playPattern(pattern);
        lastTime = System.currentTimeMillis();
    }

    public long[] concat(long[] a, long[] b) {
        int aLen = a.length;
        int bLen = b.length;
        long[] c = new long[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    double pulse(double time, long duration) {
        final double pi = 3.14;
        final double frequency = 1d / duration; // Frequency in Hz
        return 1 * (1 + Math.sin(2 * pi * frequency * time));
    }

    // intensity between 0.0 and 1.0
    // modified, based on: https://stackoverflow.com/questions/20808479/algorithm-for-generating-vibration-patterns-ranging-in-intensity-in-android/20821575#20821575
    public long[] genVibratorPattern(float intensity, long duration) {
        float dutyCycle = Math.abs((intensity * 2.0f) - 1.0f);
        long hWidth = (long) (dutyCycle * (duration - 1)) + 1;
        long lWidth = dutyCycle == 1.0f ? 0 : 1;

        int pulseCount = (int) (2.0f * ((float) duration / (float) (hWidth + lWidth)));
        long[] pattern = new long[pulseCount];

        for (int i = 0; i < pulseCount; i++) {
            pattern[i] = intensity < 0.5f ? (i % 2 == 0 ? hWidth : lWidth) : (i % 2 == 0 ? lWidth : hWidth);
        }

        return pattern;
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
