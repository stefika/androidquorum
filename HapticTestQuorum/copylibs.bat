REM !/bin/sh

copy Run\Default.jar ..\HapticTestQuorumAS\app\libs\Default.jar
copy ..\AndroidHaptic\dist\AndroidHaptic.jar ..\HapticTestQuorumAS\app\libs\AndroidHaptic.jar

cd ..\HapticTestQuorumAS

call gradlew.bat assembleDebug

call gradlew.bat installDebug

echo "Installation Complete!"