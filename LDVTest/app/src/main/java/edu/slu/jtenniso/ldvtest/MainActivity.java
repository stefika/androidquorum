package edu.slu.jtenniso.ldvtest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.speech.tts.TextToSpeech;

import com.immersion.uhl.Launcher;

import java.util.Calendar;
import java.util.Locale;

import edu.unlv.cs.vibration.VibrationManager;

public class MainActivity extends AppCompatActivity {

    private Launcher hapticLauncher;
    TextToSpeech t1;

    private VibrationManager vibrationManager;

    boolean running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Stop screen from turning off
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //Try to create a haptic feedback Launcher.
        try {
            hapticLauncher = new Launcher(this);
        } catch (Exception e) {
            Log.e("LineFollow", "Failed to create Launcher: " + e);
        }

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        final Vibrator vibrate = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        final VibrationPatternStorage vps = new VibrationPatternStorage();

        Button constant = (Button) findViewById(R.id.id1);
        constant.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vibrate.vibrate(vps.getConstant(),0);
            }
        });

        Button short_buzz = (Button) findViewById(R.id.id4);
        short_buzz.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                vibrate.vibrate(vps.getShortBuzz(),0);

            }
        });

        Button engine = (Button) findViewById(R.id.id5);
        engine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                vibrate.vibrate(vps.getEngine(),0);

            }
        });

        Button transitionBump = (Button) findViewById(R.id.id3);
        transitionBump.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                vibrate.vibrate(vps.getTransitionBump(),0);

            }
        });

        Button weapon = (Button) findViewById(R.id.id2);
        weapon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                vibrate.vibrate(vps.getWeapon(),0);

            }
        });


        //ANDROIDHAPTIC LIBRARY CALLS
        Button b2_5 = (Button) findViewById(R.id.button2_5);
        b2_5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("2.5 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b2_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz2_5 = vps.get2_5hz();
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
                vibrationManager.vibrateAtHz(2.5f, true);
            }
        });

        Button b5 = (Button) findViewById(R.id.button5);
        b5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("5 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz5 = vps.get5hz();
                vibrationManager.vibrateAtHz(5, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b10 = (Button) findViewById(R.id.button10);
        b10.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("10 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz10 = vps.get10hz();
                vibrationManager.vibrateAtHz(10, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b25 = (Button) findViewById(R.id.button25);
        b25.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("25 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b25.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz25 = vps.get25hz();
                vibrationManager.vibrateAtHz(25, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b50 = (Button) findViewById(R.id.button50);
        b50.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("50 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b50.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz50 = vps.get50hz();
                vibrationManager.vibrateAtHz(50, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b100 = (Button) findViewById(R.id.button100);
        b100.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("100 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b100.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz100 = vps.get100hz();
                vibrationManager.vibrateAtHz(100, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b250 = (Button) findViewById(R.id.button250);
        b250.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("250 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b250.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz250 = vps.get250hz();
                vibrationManager.vibrateAtHz(250, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        Button b500 = (Button) findViewById(R.id.button500);
        b500.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("500 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b500.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //VibrationPattern hz500 = vps.get500hz();
                vibrationManager.vibrateAtHz(500, true);
                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
            }
        });

        //200Hz - THE GOD BUTTON. IT DOES IT ALL!

        Button b200 = (Button) findViewById(R.id.button200);
        b200.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    t1.speak("200 Hertz", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b200.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //VibrationPattern hz200 = vps.get200hz();
                vibrate.vibrate(vps.getTestFreq(),0);
                //vibrationManager.vibrateAtHz(200, true);
                //t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
                //hapticLauncher.play(Launcher.TEXTURE5);
            }
        });


        //ANDROID NATIVE LIBRARY CALLS
//        Button b2_5 = (Button) findViewById(R.id.button2_5);
//        b2_5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("2.5 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b2_5.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz2_5 = vps.get2_5hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get2_5hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b5 = (Button) findViewById(R.id.button5);
//        b5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("5 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b5.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz5 = vps.get5hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get5hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b10 = (Button) findViewById(R.id.button10);
//        b10.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("10 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b10.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz10 = vps.get10hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get10hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b25 = (Button) findViewById(R.id.button25);
//        b25.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("25 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b25.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz25 = vps.get25hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get25hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b50 = (Button) findViewById(R.id.button50);
//        b50.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("50 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b50.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz50 = vps.get50hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get50hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b100 = (Button) findViewById(R.id.button100);
//        b100.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("100 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b100.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz100 = vps.get100hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get100hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b250 = (Button) findViewById(R.id.button250);
//        b250.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("250 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b250.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
////                VibrationPattern hz250 = vps.get250hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get250hz_android();
//                vibrate.vibrate(hz_android, 0);
//            }
//        });
//
//        Button b500 = (Button) findViewById(R.id.button500);
//        b500.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("500 Hertz", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b500.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                //VibrationPattern hz500 = vps.get500hz();
//                t1.speak("Start", TextToSpeech.QUEUE_FLUSH, null);
//                long[] hz_android = vps.get500hz_android();
//                vibrate.vibrate(hz_android, 0);
//
//            }
//        });


        //UHL LIBRARY
//        Button b_UHL = (Button) findViewById(R.id.button_uhl);
//        b_UHL.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("SHORT BUZZ", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.SHORT_BUZZ_100);
//
//            }
//        });
//
//        Button b_UHL2 = (Button) findViewById(R.id.button_uhl2);
//        b_UHL2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("LONG BUZZ", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL2.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.LONG_BUZZ_100);
//            }
//        });
//        Button b_UHL3 = (Button) findViewById(R.id.button_uhl3);
//        b_UHL3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("TRANSITION BUMP", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL3.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.TRANSITION_BUMP_100);
//
//            }
//        });
//
//        Button b_UHL4 = (Button) findViewById(R.id.button_uhl4);
//        b_UHL4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("TRANSITION BUMP", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL4.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.ALERT1);
//
//            }
//        });
//
//        Button b_UHL5 = (Button) findViewById(R.id.button_uhl5);
//        b_UHL5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("TRANSITION BUMP", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL5.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.ALERT10);
//
//            }
//        });
//
//        Button b_UHL6 = (Button) findViewById(R.id.button_uhl6);
//        b_UHL6.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    t1.speak("TRANSITION BUMP", TextToSpeech.QUEUE_FLUSH, null);
//                }
//            }
//        });
//        b_UHL6.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                hapticLauncher.play(Launcher.FAST_PULSING_33);
//
//            }
//        });



        //STOP BUTTON
        Button b_stop = (Button) findViewById(R.id.button_stop);
        b_stop.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    t1.speak("Stop", TextToSpeech.QUEUE_FLUSH, null);
                }
            }
        });
        b_stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vibrate.cancel();
            }
        });

    }

}
