package edu.slu.jtenniso.ldvtest;

import java.util.ArrayList;
import java.util.List;

import edu.unlv.cs.vibration.VibrationPattern;
import plugins.quorum.Libraries.Vibration.Vibration;

/*
This class exists to separate the creation of patterns from the rest of the program
Patterns can be described here, if new patterns should be added to the overall list,
just add them in the getListOfPatterns() method.
 */
public class VibrationPatternStorage {

    private List<VibrationPattern> patterns;
    private List<VibrationPattern> hzPatterns;
    private List<VibrationPattern> allHzPatterns;
    private List<VibrationPattern> hz10Trial;
    private List<VibrationPattern> hz25Trial;
    private List<VibrationPattern> hz50Trial;
    private List<VibrationPattern> hz100Trial;

    public List<VibrationPattern> getListOfPatterns() {
        if (patterns == null) {
            patterns = new ArrayList<>();

            patterns.add(getPattern1());
            patterns.add(getPattern2());
            patterns.add(getPattern3());
            patterns.add(getPattern4());
            patterns.add(getPattern5());
            patterns.add(getPattern6());
            patterns.add(getPattern7());
        }
        return patterns;
    }

    public VibrationPattern getPattern1() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(200, 0.5);

        return pattern;
    }

    public VibrationPattern getPattern2() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(100, 0.5);
        pattern.add(100, 0.0);

        return pattern;
    }

    public VibrationPattern getPattern3() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(50, 0.5);
        pattern.add(50, 0.0);
        pattern.add(100, 0.9);

        return pattern;
    }

    public VibrationPattern getPattern4() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(25, 0.5);
        pattern.add(25, 0.0);
        pattern.add(25, 0.5);
        pattern.add(25, 0.0);
        pattern.add(25, 0.5);

        return pattern;
    }


    public VibrationPattern getPattern5() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(50, 0.2);
        pattern.add(25, 0.0);
        pattern.add(25, 0.9);

        return pattern;
    }

    public VibrationPattern getPattern6() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(100, 0.0);
        pattern.add(25, 0.9);

        return pattern;
    }

    public VibrationPattern getPattern7() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(100, 0.9);
        pattern.add(25, 0.0);
        pattern.add(25, 0.2);

        return pattern;
    }

    public VibrationPattern getControlPattern() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.add(25, 0.5);

        return pattern;
    }

    public List<VibrationPattern> getHzPatternList() {
        if (hzPatterns == null) {
            hzPatterns = new ArrayList<>();

            hzPatterns.add(get10hz());
            hzPatterns.add(get25hz());
            hzPatterns.add(get50hz());
            hzPatterns.add(get100hz());
        }
        return hzPatterns;
    }

    public List<VibrationPattern> getAllHzPatternList() {
        if (allHzPatterns == null) {
            allHzPatterns = new ArrayList<>();

            allHzPatterns.add(get2_5hz());
            allHzPatterns.add(get5hz());
            allHzPatterns.add(get10hz());
            allHzPatterns.add(get25hz());
            allHzPatterns.add(get50hz());
            allHzPatterns.add(get100hz());
            allHzPatterns.add(get250hz());
//            allHzPatterns.add(get500hz());
        }
        return allHzPatterns;
    }

    public List<VibrationPattern> getHzPatternList(int index) {
        if (index == 0) {
            return getHz10Trial();
        } else if (index == 1){
            return getHz25Trial();
        } else if (index == 2) {
            return getHz50Trial();
        } else if (index == 3) {
            return getHz100Trial();
        }
        return null;
    }

    private List<VibrationPattern> getHz10Trial() {
        if (hz10Trial == null) {
            hz10Trial = new ArrayList<>();

            hz10Trial.add(get2_5hz());
            hz10Trial.add(get5hz());
            hz10Trial.add(get10hz());
            hz10Trial.add(get25hz());
        }
        return hz10Trial;
    }

    private List<VibrationPattern> getHz25Trial() {
        if (hz25Trial == null) {
            hz25Trial = new ArrayList<>();

            hz25Trial.add(get5hz());
            hz25Trial.add(get10hz());
            hz25Trial.add(get25hz());
            hz25Trial.add(get50hz());
        }
        return hz25Trial;
    }

    private List<VibrationPattern> getHz50Trial() {
        if (hz50Trial == null) {
            hz50Trial = new ArrayList<>();

            hz50Trial.add(get10hz());
            hz50Trial.add(get25hz());
            hz50Trial.add(get50hz());
            hz50Trial.add(get100hz());
        }
        return hz50Trial;
    }

    private List<VibrationPattern> getHz100Trial() {
        if (hz100Trial == null) {
            hz100Trial = new ArrayList<>();

            hz100Trial.add(get25hz());
            hz100Trial.add(get50hz());
            hz100Trial.add(get100hz());
            hz100Trial.add(get250hz());
        }
        return hz100Trial;
    }

    public VibrationPattern get2_5hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(2.5f);

        return pattern;
    }

    public long[] get2_5hz_android() {
        long[] pattern = new long[]{0, 200, 200};
        return pattern;
    }

    public long[] get3hz_android() {
        long[] pattern = new long[]{0, 167, 167};
        return pattern;
    }

    public VibrationPattern get5hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(5);

        return pattern;
    }

    public long[] get5hz_android() {
        long[] pattern = new long[]{0, 100, 100};
        return pattern;
    }

    public VibrationPattern get10hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(10);

        return pattern;
    }

    public long[] get10hz_android() {
        long[] pattern = new long[]{0, 50, 50};
        return pattern;
    }

    public VibrationPattern get25hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(25);

        return pattern;
    }

    public long[] getConstant() {
        long[] pattern = new long[]{0, 500};
        return pattern;
    }

    public long[] getShortBuzz() {
        long[] pattern = new long[]{25, 75, 25, 75, 25, 75, 1000};
        return pattern;
    }

    public long[] getTransitionBump() {
        long[] pattern = new long[]{25, 75, 25, 75, 400, 50, 1000, 0};
        return pattern;
    }

    public long[] getWeapon() {
        long[] pattern = new long[]{100, 50, 100, 50, 100, 50};
        return pattern;
    }

    public long[] getEngine() {
        long[] pattern = new long[]{9, 7, 9, 7, 9, 7, 9};
        return pattern;
    }

    public long[] get25hz_android() {
        long[] pattern = new long[]{0, 20, 20};
        return pattern;
    }

    public VibrationPattern get50hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(50);

        return pattern;
    }

    public long[] get50hz_android() {
        long[] pattern = new long[]{0, 10, 10};
        return pattern;
    }

    public VibrationPattern get100hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(100);

        return pattern;
    }

    public long[] get100hz_android() {
        long[] pattern = new long[]{0, 5, 5};
        return pattern;
    }

    public VibrationPattern get250hz() {
        VibrationPattern pattern = new VibrationPattern();
        pattern.frequencyPattern(250);

        return pattern;
    }

    public long[] get250hz_android() {
        long[] pattern = new long[]{0, 2, 2};
        return pattern;
    }

    public VibrationPattern get500hz() {
        VibrationPattern pattern = new VibrationPattern();

        pattern.frequencyPattern(500);

        return pattern;
    }

    public long[] get500hz_android() {
        long[] pattern = new long[]{0, 1, 1};
        return pattern;
    }

    public long[] getTestFreq() {
        long[] pattern = new long[] {0,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        return pattern;
    }

}
