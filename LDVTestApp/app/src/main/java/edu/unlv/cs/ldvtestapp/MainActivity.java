package edu.unlv.cs.ldvtestapp;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Button start = (Button) findViewById(R.id.b_start);
        start.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                EditText wait = (EditText)findViewById(R.id.e_rest);
                EditText run = (EditText) findViewById(R.id.e_run);

                int waitTime = Integer.parseInt(wait.getText().toString());
                int runTime = Integer.parseInt(run.getText().toString());

                vibrator.vibrate(new long[] {waitTime,runTime}, 0);
            }
        });

        Button stop = (Button) findViewById(R.id.b_end);
        stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vibrator.cancel();
            }
        });
    }


}
