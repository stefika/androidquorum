package com.myquorum.javaapitest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import edu.unlv.cs.vibration.VibrationManager;
import edu.unlv.cs.vibration.VibrationPattern;

public class MainActivity extends AppCompatActivity {
    VibrationManager vibrationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        setUpButtons();
    }

    private void setUpButtons() {
        final Button button1 = (Button) findViewById(R.id.button);
        button1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                vibrationTest();
                return true;
            }
        });

        final Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                vibrationTest2();
                return true;
            }
        });
    }

    public void vibrationTest() {
        VibrationPattern p = new VibrationPattern();
        p.add(200, 1.0);
        p.add(200, 0.0);
        p.add(200, 1.0);
        vibrationManager.vibratePattern(p);
    }

    public void vibrationTest2() {
        vibrationManager.vibrateWeapon();
        
    }
}
