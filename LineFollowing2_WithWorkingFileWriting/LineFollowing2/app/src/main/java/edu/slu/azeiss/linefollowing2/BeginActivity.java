package edu.slu.azeiss.linefollowing2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class BeginActivity extends AppCompatActivity {


    /**************************************************************************************
     * static OneColorController oneColorController = new OneColorController()
     * - Creates the OneColorController (called "oneColorController") for the entire program to use.
     * - oneColorController is STATIC, meaning only one copy can ever be made of it (in simple terms).
     * - This is to assure that we are keeping all of our data in one instance of the
     *  OneColorController object.
     * - oneColorController is PRIVATE, meaning no other class knows it's here and its data cannot
     *   be modified by accident.
     * - Other classes must specifically call getControlController() to get this specific oneColorController.
     *
     *  For example: OneColorController oneColorController = MainActivity.getControlController();
     **************************************************************************************/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.begin_activity);



        Button lineFollow = (Button) findViewById(R.id.btn_start);//btn_start is defined in begin_activity
        lineFollow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //On click, load the main menu.
                startActivity(new Intent(getApplicationContext(), SelectionMain.class));
            }
        });
    }


}
