package edu.slu.azeiss.linefollowing2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/* This class let's you choose the different trials
 */
public class ChoicesActivity  extends AppCompatActivity {

    private ChoicesController choicesController;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choice_activity);

        choicesController = new SelectionMain().getChoicesController();

        Button intensity0 = (Button) findViewById(R.id.intensity0);
        intensity0.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // sets the Hz
                choicesController.setChoiceID(0);
                startActivity(new Intent(getApplicationContext(), ChoiceSequenceActivity.class));
            }
        });
        Button intensity1 = (Button) findViewById(R.id.intensity1);
        intensity1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // sets the Hz
                choicesController.setChoiceID(1);
                startActivity(new Intent(getApplicationContext(), ChoiceSequenceActivity.class));
            }
        });
        Button intensity2 = (Button) findViewById(R.id.intensity2);
        intensity2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // sets the Hz
                choicesController.setChoiceID(2);
                startActivity(new Intent(getApplicationContext(), ChoiceSequenceActivity.class));
            }
        });
        Button intensity3 = (Button) findViewById(R.id.intensity3);
        intensity3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // sets the Hz

                choicesController.setChoiceID(3);
                startActivity(new Intent(getApplicationContext(), ChoiceSequenceActivity.class));
            }
        });


    }


}
