package edu.slu.azeiss.linefollowing2;

import android.os.Environment;
import android.util.Log;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.unlv.cs.vibration.VibrationPattern;

/*
This class controls all the vibrations and image stuff for the slides.s
 */
public class ChoicesController {

    private VibrationPatternStorage patternStorage;
    public static final String backgroundImg = "image_choice";
    private int choiceID = -1;
    private int participantNum;
    private File log;
    private BufferedWriter out;
    private FileOutputStream fos;
    private int slide = 0;
    private int trialNum = 0;
    private int orderIndex = 0;
    private int[] iterations;

    private ArrayList<Double> times = new ArrayList<Double>();

    private static int TOTAL = 4;

   // private int trialNum = 0;
    //private List<VibrationPattern> allVibrationPatterns;
   // private List<Integer> currentPatterns;

    private List<VibrationPatternChoice> correspondingVibrationPatterns;
    private List<Integer> currentPatterns;


    public ChoicesController(int participant) {
        participantNum = participant;
        patternStorage = new VibrationPatternStorage();
        iterations = new int[]{0,0,0,0,0,0,0,0};
    }

    // This is called when a new trial is started
    public void setChoiceID(int choiceID) {
        this.choiceID = choiceID;
        this.orderIndex = 0;
        iterations[choiceID]++; // one more repeat for this
        getCorrespondingVibrationPatterns(choiceID);
        setNewPatternOrder();
        setFilename(orderIndex);
        setBufferedWriter(log);
        this.slide = 0;
    }

    //Creates the buffer cache on the File called "file".
    public void setBufferedWriter(File file) {
        try {
            out = new BufferedWriter(new FileWriter(file.getAbsolutePath(), true));
        } catch (IOException e) {
            System.out.println("Error Cannot connect to filewriter!");
        }

    }

    // This just adds an entry to the times list
    public void logTimes(double diff) {
        times.add(diff);
    }

    // this is crucial to count up to the next slide
    public void nextVibration() {
        slide++;
    }

    // I just copied this
    public void writeData() {
        try {
            fos = new FileOutputStream(log);

            XSSFWorkbook wb = new XSSFWorkbook();
            Sheet dataSheet = wb.createSheet("Data");

            // writes the patterns used for this trial
            Row rowG = dataSheet.createRow(0);
            for (int i = 0; i < currentPatterns.size(); i++) {
                Cell cellG = rowG.createCell(i);
                cellG.setCellValue(currentPatterns.get(i) + this.choiceID);
            }

            // writes the x coordinates of points
            Row rowA = dataSheet.createRow(1);
            for (int i = 0; i < times.size(); i++) {
                Cell cellA = rowA.createCell(i);
                cellA.setCellValue(times.get(i));
            }


            wb.write(fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // This method creates a new list in a random order, creating the order in which vibrations are
    // shown.
    public void setNewPatternOrder() {

        int randNum;
        Boolean continueLoop = true;
        final Random rnd = new Random();
        List<Integer> patternOrder = new ArrayList<>();

        int count = 0;

        while(count < 4) {
            randNum = rnd.nextInt(TOTAL);

            if (!correspondingVibrationPatterns.get(randNum).isUsed()) {
                correspondingVibrationPatterns.get(randNum).setUsed(true);
                count ++;
                patternOrder.add(randNum);
            }
        }
//        for (int i = 0; i < patternOrder.size(); i ++) {
//            Log.d("patternorder = ", ""+patternOrder.get(i));
//        }
        currentPatterns = patternOrder;
    }

    public void setFilename(int num) {
       // String filename = "/Download/" + "p" + participantNum + "_choices_target_" + choiceID + "_iteration_" + iterations[choiceID] + "_trial_" + trialNum + ".xls";
        String filename = "/LF/" + "p" + participantNum + "_choices_target_" + choiceID + "_iteration_" + iterations[choiceID] + "_trial_" + trialNum + ".xls";
        Log.d("Filename = ", filename);
        log = new File(Environment.getExternalStorageDirectory(), filename);
//        Log.d("Set Filename to: ", log.getAbsolutePath());

//        setBufferedWriter(log);
    }

    // this gets a new pattern, alternating between control and treatment
    public VibrationPattern getCurrentVibrationPattern() {

        if (slide %2 == 0) {
            VibrationPattern vibrationPattern = getVibrationPattern(choiceID);
            VibrationPatternChoice c = new VibrationPatternChoice();
            c.setPattern(vibrationPattern);
            Log.d("ChoicesController", "Playing Pattern: " + c.getDebugString() );
            return vibrationPattern;
        }

        return getNextTestPattern();
    }

    // This gets the next pattern from the list of all patterns using an index from the randomized list
    private VibrationPattern getNextTestPattern() {
        VibrationPattern p = correspondingVibrationPatterns.get(currentPatterns.get(orderIndex)).getPattern();
        Log.d("ChoicesController", "Playing Pattern: " + correspondingVibrationPatterns.get(currentPatterns.get(orderIndex)).getDebugString() );
        orderIndex++;
        return p;
    }

    // gets all the patterns from the Vibration pattern storage and makes them into a local format
    // that can keep track of if it was used yet. (similar to the picture situation that was used before)
    public List<VibrationPatternChoice> getCorrespondingVibrationPatterns(int index) {
        List<VibrationPattern> temp = patternStorage.getHzPatternList(index);
        correspondingVibrationPatterns = new ArrayList<>();

        Log.d("ChoicesController", "Set the patterns to: " );
        for (VibrationPattern p : temp) {
            VibrationPatternChoice choice = new VibrationPatternChoice();
            choice.setPattern(p);
            choice.setUsed(false);
            Log.d("ChoicesController", "Pattern: " + choice.getDebugString() );
            correspondingVibrationPatterns.add(choice);
        }

        return correspondingVibrationPatterns;
    }

    public VibrationPattern getVibrationPattern (int index) {
        return patternStorage.getHzPatternList().get(index);
    }


    public int getChoiceID() {
        return choiceID;
    }


    public String getBackgroundImg(){
        return backgroundImg;
    }

    // Small class to keep track whether a vibration was
    // used
    private class VibrationPatternChoice {
        public VibrationPattern getPattern() {
            return pattern;
        }

        public void setPattern(VibrationPattern pattern) {
            this.pattern = pattern;
        }

        private VibrationPattern pattern;

        public boolean isUsed() {
            return isUsed;
        }

        public void setUsed(boolean used) {
            isUsed = used;
        }

        private boolean isUsed;

        public String getDebugString() {
            String patternString = "";
            for (int i = 0; i < this.pattern.getSize(); i++){
                patternString += "[" + this.pattern.get(i).getDuration()+ ", " + this.pattern.get(i).getIntensity() + "]" + ", ";
            }


            return patternString;
        }
    }

    // checks if there is another vibration left to use
    public boolean noMoreVibrations(){
        Log.d("orderIndex", "" + orderIndex);
       if (orderIndex < TOTAL)
           return false;
       return true;
    }

    public void closeBufferedWriter(){
        writeData();
    }
}
