package edu.slu.azeiss.linefollowing2;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.*;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import edu.unlv.cs.vibration.VibrationManager;
import edu.unlv.cs.vibration.VibrationPattern;

//import com.immersion.uhl.Launcher;


public class ControlActivity extends AppCompatActivity implements View.OnTouchListener{

    //Variables to be used throughout the class
    private ImageView img = null;
    private GraphicsView graphicsView;
    private BitmapDrawable bgImage;
    private static Context mContext;
    private String str = null;
    private Boolean cont;
    private Point start = new Point();
    private Point end = new Point();
 //   private Launcher hapticLauncher;
    private VibrationManager vibrationManager;
    private int color, action, pixelColor, closestColor;
    private float x,y;
    long tStart, tEnd, tDelta;
    double elapsedSeconds;
    
    //Jen's Note: OneColorController oneColorController = ... might be erasing the oneColorController object.
    //Fix by creating a oneColorController object THEN assigning it the oneColorController via getControlController()
    ControlController controlController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tStart = System.currentTimeMillis();
        controlController = SelectionMain.getControlController();
        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        mContext = getApplicationContext();
        img = (ImageView) findViewById(R.id.imgRandom);

        //Determines what background image shows
        setNewView();
    }

    /***************************************************************************************
     ***************************************************************************************
     ************************ GETTER METHODS. USED BY OTHER CLASSES ************************
     ***************************  TO ACCESS PRIVATE VARIABLES. *****************************
     ***************************************************************************************
     ***************************************************************************************/

    //Returns the current context. Currently don't use this, but could be helpful in the future...
    public static Context getContext() {
        return mContext;
    }

    //Don't edit this method
    protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
        final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + resName);
        }
        else {return ResourceID;}
    }

    /***************************************************************************************
     ***************************************************************************************
     ********************* MENU METHODS. PROVIDE MENU FUNCTIONALITY. ***********************
     ***************************************************************************************
     ***************************************************************************************/

    //Creates the menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /**************************************************************************************
             * switch(...)
             * - This switch-case chain determines what to do with the various menu buttons you
             *   could have in your menu bar. Right now, there is only a "NEXT" button.
             * - Pressing "NEXT" takes you to the next activity with a new background.
             * - The if-else chain checks to see if we've used all of our images or not. If it
             *   determines all of the images have been used (i.e.: noMoreImages() returns TRUE), a
             *   dialog box is created to let you know you should go back to main.
             * - DO NOT CLICK THE BACK BUTTON ON THE DEVICE ITSELF! THIS DOES NOT SAVE THE CURRENT
             *   IMAGE'S DATA.
             **************************************************************************************/
            case R.id.next:
                if(controlController.noMoreImages()) { //If there are no more images left.
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this);
                    alertDialogBuilder.setTitle("Section 1 Complete!");
                    alertDialogBuilder
                            .setMessage("Click OK to begin next section.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    controlController.closeBufferedWriter();
                                    controlController.clearIsUsed();

                                    //Erase the stack of activities. Start Next Section.
                                    Intent i = new Intent(ControlActivity.this, MainActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(new Intent(getApplicationContext(), SelectionMain.class));
                                }
                            })
                            .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });

                    //Create and show the dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else { //If there are still images to use.

                    //Save the current data and close the buffer cache.
                    controlController.closeBufferedWriter();

                    //Create a new intent of OneColorActivity
                    Intent i = new Intent(this, OneColorActivity.class);

                    //Change the background image via the oneColorController's setView() function.
                    controlController.setView();

                    //End the current activity.
                    finish();

                    //Start the new activity.
                    startActivity(i);
                }
                break;
        }
        return true;
    }

    /***************************************************************************************
     ***************************************************************************************
     *********************** UTILITY METHODS. PROVIDE SOME UTILITY. ************************
     ***************************************************************************************
     ***************************************************************************************/

    //This is a custom class to display the background images. Don't edit this.
    static public class GraphicsView extends View {
        public GraphicsView(Context context, Drawable drawable) {
            super(context);
            setBackground(drawable);
        }
    }

    /**************************************************************************************
     * setNewView()
     * - Determines via the oneColorController which image should be displayed in the background.
     * - Do not worry that getDrawable(...) is crossed out -- it will still work.
     **************************************************************************************/

    public void setNewView() {
        String temp;
        //Grabs the string name of which background should be displayed from oneColorController.
        temp = controlController.getBackgroundImg();

        //Declares bgImage to be the backgroundImg from above.
        bgImage = (BitmapDrawable) getResources().getDrawable(getResourceID(temp, "drawable", getApplicationContext()));

        //Creates a graphicsView to use
        graphicsView = new GraphicsView(this, bgImage);
        setContentView(graphicsView);
        graphicsView.setOnTouchListener(this);
    }

    /**************************************************************************************
     * onTouch(...)
     * - Handles all motion events (finger touches), including multitouch.
     * - On MotionEvent._ACTION_DOWN and ACTION_MOVE, the pixel colors are evaluated using
     *   evaluatePixelColor(x,y).
     * - X & Y coordinates are recorded into an Excel spreadsheet on the sdcard via OneColorController.
     * - Points 'end' and 'start' are used to keep track of their x & y coordinates
     *   more easily.
     **************************************************************************************/

    public boolean onTouch(View v, MotionEvent ev) {
        action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_MOVE:
                x = ev.getX();
                y = ev.getY();

                pixelColor = evaluatePixelColor((int) x, (int) y);
                closestColor = findClosestColor((int) x, (int) y);
                end = new android.graphics.Point((int)x, (int)y);
                tEnd = System.currentTimeMillis();
                tDelta = tEnd - tStart;
                elapsedSeconds = tDelta / 1000.0;

                controlController.logCoords(end.x, end.y, elapsedSeconds, pixelColor, closestColor);

                break;

            case MotionEvent.ACTION_DOWN:
                x = ev.getX();
                y = ev.getY();

                pixelColor = evaluatePixelColor((int) x, (int) y);
                closestColor = findClosestColor((int) x, (int) y);
                // TODO: What does start do here? It isn't used anywhere else
                start = new android.graphics.Point((int) x, (int) y);
                tEnd = System.currentTimeMillis();
                tDelta = tEnd - tStart;
                elapsedSeconds = tDelta / 1000.0;

                controlController.logCoords(end.x, end.y, elapsedSeconds, pixelColor, closestColor);

                break;

            case MotionEvent.ACTION_UP:
     //           hapticLauncher.stop();
                    vibrationManager.stop();
                break;
        }

        return true;
    }



    /**************************************************************************************
     * evaluatePixelColor(...)
     * - Handles the interpretation of the pixel the finger is currently on.
     * - Evaluates whether it should vibrate or not based on what color is being touched.
     **************************************************************************************/

    public int evaluatePixelColor(int x, int y) {
        color = bgImage.getBitmap().getPixel(x, y);
        Log.d("Color", "Color: " + color);
        return whichColor(color);
    }

    /*
        Simple algorithm to check for nearest colors.
        Only checks in 8 directions, might make it inaccurate, but checking every pixel in vicinity is computationally expensive
        might need distance limit to make faster (on white image this will take the longest)
     */
    public int findClosestColor(int x, int y) {
        int height = bgImage.getBitmap().getHeight();
        int width = bgImage.getBitmap().getWidth();
        int maxDistance = height;
        if (width > height) {
            maxDistance = width;
        }
        color = bgImage.getBitmap().getPixel(x, y);
//        long t1 = System.currentTimeMillis();
        //Log.d("Color", Integer.toString(color));

        int distance = 0;
        int closestColor = color;
      //  int translatedColor = whichColor(closestColor);
        while (closestColor == -1 && distance < maxDistance) {
           // Log.d("closestcolor", "closestColor: " + whichColor(closestColor) + ", distance: " + distance+ ", maxDistance: " + maxDistance);
            closestColor = checkCoordinate(x + distance, y , distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x + distance, y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x + distance, y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x , y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x , y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y , distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            distance = distance + 1;
        }
//        long t2 = System.currentTimeMillis();
 //       long difference = t2-t1;
       // Log.d("after Loop", "time: " + difference + " closestColor: " + whichColor(closestColor) + ", distance: " + distance+ ", maxDistance: " + maxDistance);
        return closestColor;
    }

    private int checkCoordinate (int x, int y, int distance, int height, int width) {
         if (height > y && y > 0 && width > x && x > 0 ) {
             int closestColor = bgImage.getBitmap().getPixel(x, y);
             return closestColor;
         }
         return 0;
    }

    private int whichColor (int color) {

        /**************************************************************************************
         * Troubleshooting:
         * - If the actions taken on Color.RED/WHITE/BLACK are not working, you can brute force
         *   the code by using the debugger log code. Use the debugger to get the color integer.
         *   As you can see above, I could not get Android to recognize the color black in my
         *   image, so I had to record the integer being used for black and use that instead.
         *
         *   Use this code:
         *          Log.d("Color: " + color, "");
         *          //Record whatever color your finger is touching and set appropriately:
         *          final int red = -65536;
         *          final int white = -1;
         *          final int black = -16777216;
         **************************************************************************************/
        switch (color) {
            case Color.RED:
                VibrationPattern p = new VibrationPattern().ShortSharp();
                vibrationManager.vibratePattern(p);
                return 1;
            case Color.WHITE:

                return 0;
            case Color.BLACK:
                return -1;
            case -16744448: //~green
                VibrationPattern p2 = new VibrationPattern().DoubleClick();
                vibrationManager.vibratePattern(p2);
                return 2;
        }
        return 0;
    }

}