package edu.slu.azeiss.linefollowing2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static int number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button lineFollow = (Button) findViewById(R.id.btn_linefollow);//btn_linefollow is defined in activity_main
        lineFollow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Get the textbox with the participant number.
                final EditText textbox = (EditText)findViewById(R.id.txt_participantNumber);

                //Parse the textbox input into a String variable.
                String partipantNumString = textbox.getText().toString();

                //Convert the String variable to an integer variable.
                number = Integer.parseInt(partipantNumString);

                //On click, load the ControlActivity.java class.
                startActivity(new Intent(getApplicationContext(), SelectionMain.class));
            }
        });
    }

    public static int getNumber() {
        return number;
    }
}
