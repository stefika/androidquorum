package edu.slu.azeiss.linefollowing2;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.*;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import edu.unlv.cs.vibration.VibrationManager;
import edu.unlv.cs.vibration.VibrationPattern;

//import com.immersion.uhl.Launcher;


public class OneColorActivity extends AppCompatActivity implements View.OnTouchListener{

    private static Context mContext;
    private OneColorController oneColorController;
    private OneColorActivity.GraphicsView graphicsView;
    private long tStart;

    private VibrationManager vibrationManager;
    private ImageView img;
    private VibrationPattern pattern;
    private BitmapDrawable bgImage;
    private boolean vibrating = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tStart = System.currentTimeMillis();
        oneColorController = SelectionMain.getOneColorController();

        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        mContext = getApplicationContext();
        img = (ImageView) findViewById(R.id.imgRandom);


        //Determines what background image shows
        setNewView();
    }

    public void setNewView() {
        String temp;
        //Grabs the string name of which background should be displayed from oneColorController.
        temp = oneColorController.getBackgroundImg();

        //Declares bgImage to be the backgroundImg from above.
        bgImage = (BitmapDrawable) getResources().getDrawable(getResourceID(temp, "drawable", getApplicationContext()));

        //Creates a graphicsView to use
        graphicsView = new OneColorActivity.GraphicsView(this, bgImage);
        setContentView(graphicsView);
        graphicsView.setOnTouchListener(this);

        pattern = oneColorController.getCurrentVibrationPattern();
    }

    public boolean onTouch(View v, MotionEvent ev) {
        int action = ev.getAction();

        switch (action) {
            case MotionEvent.ACTION_MOVE:
                react(ev);
                break;
            case MotionEvent.ACTION_DOWN:
                react(ev);
                break;
            case MotionEvent.ACTION_UP:
                vibrationManager.stop();
                break;
        }
        return true;
    }

    private void react(MotionEvent ev) {
        float eventX;
        float eventY;
        eventX = ev.getX();
        eventY = ev.getY();

//        float[] eventXY = new float[] {eventX, eventY};
//        Matrix invertMatrix = new Matrix();
//        ((ImageView)graphicsView).getImageMatrix().invert(invertMatrix);
//
//        invertMatrix.mapPoints(eventXY);
        int x = Integer.valueOf((int)eventX);
        int y = Integer.valueOf((int)eventY);
        Bitmap bitmap = bgImage.getBitmap();
//        Log.d("position", "X: " + x + ", y:" + y);
        if (0 < x && x <=bitmap.getWidth() && 0 < y && y < bitmap.getHeight() ) {
            int color = bitmap.getPixel((int) x, (int) y);
            Log.d("vibration", "color: " + color + " x: " + x + " y:" + y);
            switch (color) {
                case Color.BLACK:
                    if (!vibrating) {
                        vibrating = true;
                        vibrationManager.vibratePattern(pattern, true);
                    }
                    break;
                default:
                    vibrating = false;
                    vibrationManager.stop();
                    break;
            }
        }
    }

    protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
        final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + resName);
        }
        else {return ResourceID;}
    }

    //This is a custom class to display the background images. Don't edit this.
    static public class GraphicsView extends ImageView {
        public GraphicsView(Context context, Drawable drawable) {
            super(context);
            setBackground(drawable);
        }
    }

    //Creates the menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /**************************************************************************************
             * switch(...)
             * - This switch-case chain determines what to do with the various menu buttons you
             *   could have in your menu bar. Right now, there is only a "NEXT" button.
             * - Pressing "NEXT" takes you to the next activity with a new background.
             * - The if-else chain checks to see if we've used all of our images or not. If it
             *   determines all of the images have been used (i.e.: noMoreImages() returns TRUE), a
             *   dialog box is created to let you know you should go back to main.
             * - DO NOT CLICK THE BACK BUTTON ON THE DEVICE ITSELF! THIS DOES NOT SAVE THE CURRENT
             *   IMAGE'S DATA.
             **************************************************************************************/
            case R.id.next:
                if(oneColorController.noMoreVibrations()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this);
                    alertDialogBuilder.setTitle("Section Complete!");
                    alertDialogBuilder
                            .setMessage("Click OK to begin next section.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    long tStop = System.currentTimeMillis();
                                    long tDiff = tStop - tStart;
                                    oneColorController.logTimes(tDiff/1000.0);
                                    oneColorController.closeBufferedWriter();

                                    //Erase the stack of activities. Start Next Section.
                                    Intent i = new Intent(OneColorActivity.this, MainActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(new Intent(getApplicationContext(), SelectionMain.class));
                                }
                            })
                            .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });

                    //Create and show the dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else {

                    //Create a new intent of OneColorActivity
                    Intent nextActivityIntent = new Intent(this, OneColorActivity.class);

                    long tStop = System.currentTimeMillis();
                    long tDiff = tStop - tStart;
//                    choicesController.log(tDiff);
                    oneColorController.logTimes(tDiff/1000.0);

                    //Change the background image via the oneColorController's setView() function.
                    oneColorController.nextVibration();

                    //End the current activity.
                    finish();

                    //Start the new activity.
                    startActivity(nextActivityIntent);
                }
                break;
        }
        return true;
    }

}