package edu.slu.azeiss.linefollowing2;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
//import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFRow;
//import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import edu.unlv.cs.vibration.VibrationPattern;

public class OneColorController {

    private VibrationPatternStorage patternStorage;
    public static final String backgroundImg = "beeanatomy";
    private int choiceID = -1;
    private int participantNum;
    private File log;
    private BufferedWriter out;
    private FileOutputStream fos;
    private int slide = 0;
    private int trialNum = 0;
    private int orderIndex = 0;


    private ArrayList<Double> times = new ArrayList<Double>();

    private static int TOTAL = 7;

    // private int trialNum = 0;
    //private List<VibrationPattern> allVibrationPatterns;
    // private List<Integer> currentPatterns;

    private List<VibrationPatternChoice> correspondingVibrationPatterns;
    private List<Integer> currentPatterns;

    public OneColorController(int participant) {
        participantNum = participant;
        patternStorage = new VibrationPatternStorage();
        this.orderIndex = 0;
        getCorrespondingVibrationPatterns();
        setNewPatternOrder();
        setFilename(orderIndex);
        setBufferedWriter(log);
    }

    public void setChoiceID(int choiceID) {
        this.choiceID = choiceID;

    }

    //Creates the buffer cache on the File called "file".
    public void setBufferedWriter(File file) {
        try {
            out = new BufferedWriter(new FileWriter(file.getAbsolutePath(), true));
        } catch (IOException e) {
            System.out.println("Error Cannot connect to filewriter!");
        }
    }

    public void logTimes(double diff) {
        times.add(diff);
    }

    public void nextVibration() {

        slide++;
    }

    public void writeData() {
        try {
            fos = new FileOutputStream(log);

            XSSFWorkbook wb = new XSSFWorkbook();
            Sheet dataSheet = wb.createSheet("Data");

            // writes the patterns used for this trial
            Row rowG = dataSheet.createRow(0);
            for (int i = 0; i < currentPatterns.size(); i++) {
                Cell cellG = rowG.createCell(i);
                cellG.setCellValue(currentPatterns.get(i));
            }

            // writes the x coordinates of points
            Row rowA = dataSheet.createRow(1);
            for (int i = 0; i < times.size(); i++) {
                Cell cellA = rowA.createCell(i);
                cellA.setCellValue(times.get(i));
            }


            wb.write(fos);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setNewPatternOrder() {

        int randNum;
        Boolean continueLoop = true;
        final Random rnd = new Random();
        List<Integer> patternOrder = new ArrayList<>();

        int count = 0;

        while(count < 70) {
            randNum = rnd.nextInt(TOTAL);

            if (!correspondingVibrationPatterns.get(randNum).maxUsed()) {
                correspondingVibrationPatterns.get(randNum).addUsed();
                count ++;
                patternOrder.add(randNum);
            }
        }
        Log.d("patternorder", patternOrder.toString());
        currentPatterns = patternOrder;
    }

    public void setFilename(int num) {
        // String filename = "/Download/" + "p" + participantNum + "_choices_target_" + choiceID + "_iteration_" + iterations[choiceID] + "_trial_" + trialNum + ".xls";
        String filename = "/LF/" + "p" + participantNum + "_choices_target_" + choiceID + "_trial_" + trialNum + ".xls";
        Log.d("Filename = ", filename);
        log = new File(Environment.getExternalStorageDirectory(), filename);
//        Log.d("Set Filename to: ", log.getAbsolutePath());

//        setBufferedWriter(log);
    }

    public VibrationPattern getCurrentVibrationPattern() {
        return getNextTestPattern();
    }

    private VibrationPattern getNextTestPattern() {
        VibrationPattern p = correspondingVibrationPatterns.get(currentPatterns.get(orderIndex)).getPattern();
        orderIndex++;
        return p;
    }

    public List<VibrationPatternChoice> getCorrespondingVibrationPatterns() {
        List<VibrationPattern> temp = patternStorage.getAllHzPatternList();
        correspondingVibrationPatterns = new ArrayList<>();

        for (VibrationPattern p: temp) {
            VibrationPatternChoice choice = new VibrationPatternChoice();
            choice.setPattern(p);
            choice.setTimesUsed(0);
            correspondingVibrationPatterns.add(choice);
        }

        return correspondingVibrationPatterns;
    }

    public VibrationPattern getVibrationPattern (int index) {
        return patternStorage.getHzPatternList().get(index);
    }


    public int getChoiceID() {
        return choiceID;
    }


    public String getBackgroundImg(){
        return backgroundImg;
    }

    private class VibrationPatternChoice {
        public VibrationPattern getPattern() {
            return pattern;
        }

        public void setPattern(VibrationPattern pattern) {
            this.pattern = pattern;
        }

        private VibrationPattern pattern;

        public int getTimesUsed() {
            return timesUsed;
        }

        public void setTimesUsed(int timesUsed) {
            this.timesUsed = timesUsed;
        }

        public boolean maxUsed() {
            if (timesUsed >= 10) {
                return true;
            }
            return false;
        }

        public void addUsed() {
            this.timesUsed = this.timesUsed + 1;
        }

        private int timesUsed;
    }

    public boolean noMoreVibrations(){

        if (orderIndex < 70) {
            Log.d("orderIndex", "" + orderIndex);
            return false;
        }
        return true;
    }

    public void closeBufferedWriter(){
        writeData();
    }


}
