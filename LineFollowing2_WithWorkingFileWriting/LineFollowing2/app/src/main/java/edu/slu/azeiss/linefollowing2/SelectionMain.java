package edu.slu.azeiss.linefollowing2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SelectionMain extends AppCompatActivity{

    static int num = MainActivity.getNumber();

    static ControlController controlController; //= new OneColorController(num);
    static GridController gridController; //= new GridController(num);
    static KeyPointController keyPointController; //= new KeyPointController(num);
    static ComboController comboController; //= new ComboController(num);
    static OneColorController oneColorController;
    static SixColorController sixColorController;
    static ChoicesController choicesController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection_main);

//        Button control = (Button) findViewById(R.id.control_button);
//        control.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                sixColorController = new OneColorController(num);
//                startActivity(new Intent(getApplicationContext(), OneColorActivity.class));
//            }
//        });
//
//        Button grid = (Button) findViewById(R.id.grid_button);
//        grid.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                gridController = new GridController(num);
//                startActivity(new Intent(getApplicationContext(), GridActivity.class));
//            }
//        });

        Button oneColor = (Button) findViewById(R.id.one_color_button);
        oneColor.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                oneColorController = new OneColorController(num);
                startActivity(new Intent(getApplicationContext(), OneColorActivity.class));
            }
        });

        Button choices = (Button) findViewById(R.id.choices_button);
        choices.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                choicesController = new ChoicesController(num);
                startActivity(new Intent(getApplicationContext(),ChoicesActivity.class));
            }
        });


//        Button test = (Button) findViewById(R.id.test_button);
//        test.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                startActivity(new Intent(getApplicationContext(),TestActivity.class));
//            }
//        });

//        Button sixColor = (Button) findViewById(R.id.six_color_button);
//        sixColor.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                sixColorController = new SixColorController(num);
//                startActivity(new Intent(getApplicationContext(), SixColorActivity.class));
//            }
//        });

//        Button key_point = (Button) findViewById(R.id.key_point_button);
//        key_point.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                keyPointController = new KeyPointController(num);
//                startActivity(new Intent(getApplicationContext(), KeyPointActivity.class));
//            }
//        });
//
//        Button combo = (Button) findViewById(R.id.combo_button);
//        combo.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                comboController = new ComboController(num);
//                startActivity(new Intent(getApplicationContext(), ComboActivity.class));
//            }
//        });

    }

    public static ChoicesController getChoicesController() {return choicesController;}
    public static SixColorController getSixColorController() {return sixColorController;}
    public static OneColorController getOneColorController() {return oneColorController;}
    public static ControlController getControlController() {return controlController;}
    public static GridController getGridController() {return gridController;}
    public static KeyPointController getKeyPointController() {return keyPointController;}
    public static ComboController getComboController() {
        return comboController;
    }

}
