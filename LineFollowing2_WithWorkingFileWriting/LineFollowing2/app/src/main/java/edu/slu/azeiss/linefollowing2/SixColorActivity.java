package edu.slu.azeiss.linefollowing2;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.*;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.List;

import edu.unlv.cs.vibration.VibrationManager;
import edu.unlv.cs.vibration.VibrationPattern;

//import com.immersion.uhl.Launcher;


public class SixColorActivity extends AppCompatActivity implements View.OnTouchListener{

    //Variables to be used throughout the class
    private ImageView img = null;
    private GraphicsView graphicsView;
    private BitmapDrawable bgImage;
    private static Context mContext;
    private String str = null;
    private Boolean cont;
    private Point start = new Point();
    private Point end = new Point();
 //   private Launcher hapticLauncher;
    private VibrationManager vibrationManager;
    private int color, action, pixelColor, closestColor;
    private float x,y;
    long tStart, tEnd, tDelta;
    double elapsedSeconds;
    long millisSinceLastRecord = 0;
    boolean delayLogging = false;

    private List<VibrationPattern> vibrationPatterns;
    
    //Jen's Note: OneColorController oneColorController = ... might be erasing the oneColorController object.
    //Fix by creating a oneColorController object THEN assigning it the oneColorController via getControlController()
    SixColorController sixColorController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tStart = System.currentTimeMillis();
        sixColorController = SelectionMain.getSixColorController();
        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        mContext = getApplicationContext();
        img = (ImageView) findViewById(R.id.imgRandom);

        vibrationPatterns = sixColorController.getVibrationPatterns();

        //Determines what background image shows
        setNewView();
    }

    /***************************************************************************************
     ***************************************************************************************
     ************************ GETTER METHODS. USED BY OTHER CLASSES ************************
     ***************************  TO ACCESS PRIVATE VARIABLES. *****************************
     ***************************************************************************************
     ***************************************************************************************/

    //Returns the current context. Currently don't use this, but could be helpful in the future...
    public static Context getContext() {
        return mContext;
    }

    //Don't edit this method
    protected final static int getResourceID(final String resName, final String resType, final Context ctx) {
        final int ResourceID = ctx.getResources().getIdentifier(resName, resType, ctx.getApplicationInfo().packageName);
        if (ResourceID == 0) {
            throw new IllegalArgumentException("No resource string found with name " + resName);
        }
        else {return ResourceID;}
    }

    /***************************************************************************************
     ***************************************************************************************
     ********************* MENU METHODS. PROVIDE MENU FUNCTIONALITY. ***********************
     ***************************************************************************************
     ***************************************************************************************/

    //Creates the menu.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            /**************************************************************************************
             * switch(...)
             * - This switch-case chain determines what to do with the various menu buttons you
             *   could have in your menu bar. Right now, there is only a "NEXT" button.
             * - Pressing "NEXT" takes you to the next activity with a new background.
             * - The if-else chain checks to see if we've used all of our images or not. If it
             *   determines all of the images have been used (i.e.: noMoreImages() returns TRUE), a
             *   dialog box is created to let you know you should go back to main.
             * - DO NOT CLICK THE BACK BUTTON ON THE DEVICE ITSELF! THIS DOES NOT SAVE THE CURRENT
             *   IMAGE'S DATA.
             **************************************************************************************/
            case R.id.next:
                if(sixColorController.noMoreImages()) { //If there are no more images left.
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder( this);
                    alertDialogBuilder.setTitle("Section 1 Complete!");
                    alertDialogBuilder
                            .setMessage("Click OK to begin next section.")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    sixColorController.closeBufferedWriter();
                                    sixColorController.clearIsUsed();

                                    //Erase the stack of activities. Start Next Section.
                                    Intent i = new Intent(SixColorActivity.this, MainActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(new Intent(getApplicationContext(), SelectionMain.class));
                                }
                            })
                            .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });

                    //Create and show the dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();

                } else { //If there are still images to use.

                    //Save the current data and close the buffer cache.
                    sixColorController.closeBufferedWriter();

                    //Create a new intent of OneColorActivity
                    Intent nextActivityIntent = new Intent(this, SixColorActivity.class);

                    //Change the background image via the oneColorController's setView() function.
                    sixColorController.setView();

                    //Get new patterns. This class only ever gets one and the same
                    vibrationPatterns = sixColorController.getVibrationPatterns();

                    //End the current activity.
                    finish();

                    //Start the new activity.
                    startActivity(nextActivityIntent);
                }
                break;
        }
        return true;
    }

    /***************************************************************************************
     ***************************************************************************************
     *********************** UTILITY METHODS. PROVIDE SOME UTILITY. ************************
     ***************************************************************************************
     ***************************************************************************************/

    //This is a custom class to display the background images. Don't edit this.
    static public class GraphicsView extends View {
        public GraphicsView(Context context, Drawable drawable) {
            super(context);
            setBackground(drawable);
        }
    }

    /**************************************************************************************
     * setNewView()
     * - Determines via the oneColorController which image should be displayed in the background.
     * - Do not worry that getDrawable(...) is crossed out -- it will still work.
     **************************************************************************************/

    public void setNewView() {
        String temp;
        //Grabs the string name of which background should be displayed from oneColorController.
        temp = sixColorController.getBackgroundImg();

        //Declares bgImage to be the backgroundImg from above.
        bgImage = (BitmapDrawable) getResources().getDrawable(getResourceID(temp, "drawable", getApplicationContext()));

        //Creates a graphicsView to use
        graphicsView = new GraphicsView(this, bgImage);
        setContentView(graphicsView);
        graphicsView.setOnTouchListener(this);
    }

    /**************************************************************************************
     * onTouch(...)
     * - Handles all motion events (finger touches), including multitouch.
     * - On MotionEvent._ACTION_DOWN and ACTION_MOVE, the pixel colors are evaluated using
     *   evaluatePixelColor(x,y).
     * - X & Y coordinates are recorded into an Excel spreadsheet on the sdcard via OneColorController.
     * - Points 'end' and 'start' are used to keep track of their x & y coordinates
     *   more easily.
     **************************************************************************************/

    public boolean onTouch(View v, MotionEvent ev) {
        action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                x = ev.getX();
                y = ev.getY();

                pixelColor = evaluatePixelColor((int) x, (int) y);
                closestColor = findClosestColor((int) x, (int) y);
                end = new android.graphics.Point((int) x, (int) y);
                tEnd = System.currentTimeMillis();
                tDelta = tEnd - tStart;
                elapsedSeconds = tDelta / 1000.0;

                logData();

                break;

            case MotionEvent.ACTION_DOWN:
                x = ev.getX();
                y = ev.getY();

                pixelColor = evaluatePixelColor((int) x, (int) y);
                closestColor = findClosestColor((int) x, (int) y);
                // TODO: What does start do here? It isn't used anywhere else
                // if it wasn't here, this could be easily put into a method
                start = new android.graphics.Point((int) x, (int) y);
                tEnd = System.currentTimeMillis();
                tDelta = tEnd - tStart;
                elapsedSeconds = tDelta / 1000.0;

                logData();

                break;

            case MotionEvent.ACTION_UP:
                //           hapticLauncher.stop();
                vibrationManager.stop();
                break;
        }


        return true;
    }

    private void logData(){
        if (delayLogging) {
            long currentMillis = System.currentTimeMillis();
            if (currentMillis - millisSinceLastRecord >= 500) {
                millisSinceLastRecord = currentMillis;
                sixColorController.logCoords(end.x, end.y, elapsedSeconds, pixelColor, closestColor);
            }
        } else {
            sixColorController.logCoords(end.x, end.y, elapsedSeconds, pixelColor, closestColor);
        }
    }


    /**************************************************************************************
     * evaluatePixelColor(...)
     * - Handles the interpretation of the pixel the finger is currently on.
     * - Evaluates whether it should vibrate or not based on what color is being touched.
     **************************************************************************************/

    public int evaluatePixelColor(int x, int y) {
        color = bgImage.getBitmap().getPixel(x, y);
        Log.d("Color", "Color: " + color);
        return whichColor(color);
    }

    /*
    Simple algorithm to check for nearest colors.
    Only checks in 8 directions, might make it inaccurate, but checking every pixel in vicinity is computationally expensive
    might need distance limit to make faster (on white image this will take the longest)
     */
    public int findClosestColor(int x, int y) {
        int height = bgImage.getBitmap().getHeight();
        int width = bgImage.getBitmap().getWidth();
        int maxDistance = height;
        if (width > height) {
            maxDistance = width;
        }
        color = bgImage.getBitmap().getPixel(x, y);

        int distance = 0;
        int closestColor = color;
        while (closestColor == -1 && distance < maxDistance) {
            closestColor = checkCoordinate(x + distance, y , distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x + distance, y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x + distance, y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x , y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x , y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y , distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y + distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            closestColor = checkCoordinate(x - distance, y - distance, distance, height, width);
            if(closestColor != -1) {
                return closestColor;
            }

            distance = distance + 1;
        }
        return closestColor;
    }

    private int checkCoordinate (int x, int y, int distance, int height, int width) {
         if (height > y && y > 0 && width > x && x > 0 ) {
             int closestColor = bgImage.getBitmap().getPixel(x, y);
             return closestColor;
         }
         return 0;
    }

    private int whichColor (int color) {

        /**************************************************************************************
         * Troubleshooting:
         * - If the actions taken on Color.RED/WHITE/BLACK are not working, you can brute force
         *   the code by using the debugger log code. Use the debugger to get the color integer.
         *   As you can see above, I could not get Android to recognize the color black in my
         *   image, so I had to record the integer being used for black and use that instead.
         *
         *   Use this code:
         *          Log.d("Color: " + color, "");
         *          //Record whatever color your finger is touching and set appropriately:
         *          final int red = -65536;
         *          final int white = -1;
         *          final int black = -16777216;
         **************************************************************************************/
        switch (color) {
            case Color.WHITE:
                // Do nothing
                return 0;
            case Color.RED:
                vibrationManager.vibratePattern(vibrationPatterns.get(0));
                return 1;
            case Color.BLACK: //-16777216
                vibrationManager.vibratePattern(vibrationPatterns.get(1));
                return 1;
            case -16744448: //~green
                vibrationManager.vibratePattern(vibrationPatterns.get(2));
                return 1;
            // -16777216 -13760240 -522496 -65522 -130816 -785401 -196871

        }
        return 0;
    }

}