package edu.slu.azeiss.linefollowing2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import edu.unlv.cs.vibration.VibrationManager;
import edu.unlv.cs.vibration.VibrationPattern;

public class TestActivity extends AppCompatActivity {

    private VibrationManager vibrationManager;

    boolean running = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        vibrationManager = new VibrationManager();
        vibrationManager.setActivity(this);
        vibrationManager.initialize();

        final Vibrator vibrate = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        final VibrationPatternStorage vps = new VibrationPatternStorage();

        Button b2_5 = (Button) findViewById(R.id.button2_5);
        b2_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz2_5 = vps.get2_5hz();
                vibrationManager.vibrateAtHz(2.5f, true);
            }
        });

        Button b3 = (Button) findViewById(R.id.button3);
        b3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz3 = vps.get3hz();
                vibrationManager.vibrateAtHz(3, true);
            }
        });

        Button b5 = (Button) findViewById(R.id.button5);
        b5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz5 = vps.get5hz();
                vibrationManager.vibrateAtHz(5, true);
            }
        });

        Button b10 = (Button) findViewById(R.id.button10);
        b10.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz10 = vps.get10hz();
                vibrationManager.vibrateAtHz(10, true);
            }
        });

        Button b25 = (Button) findViewById(R.id.button25);
        b25.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz25 = vps.get25hz();
                vibrationManager.vibrateAtHz(25, true);
            }
        });

        Button b50 = (Button) findViewById(R.id.button50);
        b50.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz50 = vps.get50hz();
                vibrationManager.vibrateAtHz(50, true);
            }
        });

        Button b100 = (Button) findViewById(R.id.button100);
        b100.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz100 = vps.get100hz();
                vibrationManager.vibrateAtHz(100, true);
            }
        });

        Button b250 = (Button) findViewById(R.id.button250);
        b250.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
//                VibrationPattern hz250 = vps.get250hz();
                vibrationManager.vibrateAtHz(250, true);
            }
        });

        Button b500 = (Button) findViewById(R.id.button500);
        b500.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //VibrationPattern hz500 = vps.get500hz();
                vibrationManager.vibrateAtHz(500, true);
            }
        });


        Button b2_5_2 = (Button) findViewById(R.id.button2_5_2);
        b2_5_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get2_5hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b3_2 = (Button) findViewById(R.id.button3_2);
        b3_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get3hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b5_2 = (Button) findViewById(R.id.button5_2);
        b5_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get5hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b10_2 = (Button) findViewById(R.id.button10_2);
        b10_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get10hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b25_2 = (Button) findViewById(R.id.button25_2);
        b25_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get25hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b50_2 = (Button) findViewById(R.id.button50_2);
        b50_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get50hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b100_2 = (Button) findViewById(R.id.button100_2);
        b100_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get100hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b250_2 = (Button) findViewById(R.id.button250_2);
        b250_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get250hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b500_2 = (Button) findViewById(R.id.button500_2);
        b500_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                long[] hz_android = vps.get500hz_android();
                vibrate.vibrate(hz_android, 0);
            }
        });

        Button b_stop = (Button) findViewById(R.id.button_stop);
        b_stop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                vibrate.cancel();
            }
        });

    }

}
