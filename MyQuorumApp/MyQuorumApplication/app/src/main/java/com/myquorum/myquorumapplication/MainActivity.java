package com.myquorum.myquorumapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


import quorum.Libraries.Game.AndroidApplication;
import plugins.quorum.Libraries.Game.GameStateManager;
import quorum.Main;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        Main main = new Main();
        plugins.quorum.Libraries.Game.AndroidApplication.SetActivity(this);

        main.Main();
//    TextView tv = (TextView) findViewById(R.id.sample_text);
//    tv.setText(stringFromJNI());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        ((AndroidApplication)GameStateManager.application).plugin_.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onPause()
    {
        // Call AndroidApplication
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        ((AndroidApplication)GameStateManager.application).plugin_.onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        // Call AndroidApplication
        super.onDestroy();
    }
}