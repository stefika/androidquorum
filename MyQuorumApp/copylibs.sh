#!/bin/bash

cp Run/Default.jar MyQuorumApplication/app/libs/Default.jar

cd MyQuorumApplication

./gradlew assembleDebug

./gradlew installDebug

echo "Installation Complete!"