library(tidyverse)

#setwd("~/Repositories/androidquorum/hapticexperiment/")
setwd("C://Users/Merlin/Repositories/androidquorum/hapticexperiment/")
d <- read.csv("ballot.csv")
d.nevada <- d %>% filter(state == "Nevada")
d.nevada <- d.nevada %>% filter(question != "Question 3")
d.nevada$answer <- factor(d.nevada$answer, levels=c("Yes", "No"))

d.meat <- read.csv("meat.csv")
d.meat$product <- factor (d.meat$product, levels =c("Vegetables", "Fruit"))

d.maine <- d %>% filter(state == "Maine")
d.maine <- d.maine %>% filter(question != "Question 3")
d.maine$answer <- factor(d.maine$answer, levels=c("Yes", "No"))

t <- theme(panel.grid.major = element_blank(), 
           panel.grid.minor = element_blank(), 
           panel.grid.major.y = element_line(size=.4, color="grey28"),
           panel.ontop = TRUE,
           panel.background = element_blank(), 
           axis.line = element_line(colour = "black"), 
           plot.title=element_text(color = "grey25", hjust=.5, size = 25), 
           axis.text.x = element_text(color="grey25", size = 23), 
           axis.text.y= element_text(color="grey25"), 
           legend.text= element_text(color="grey25"), 
           legend.title = element_text(color = "grey25"), 
           axis.title = element_text(color="grey25", size = 25),
           legend.position="none",
           plot.margin=unit(c(1,1,1.5,1.2),"cm"))

t.small <- theme(panel.grid.major = element_blank(), 
           panel.grid.minor = element_blank(), 
           panel.grid.major.y = element_line(size=.4, color="grey28"),
           panel.ontop = TRUE,
           panel.background = element_blank(), 
           axis.line = element_line(colour = "black"), 
           plot.title=element_text(color = "grey25", hjust=.5, size = 25), 
           axis.text.x = element_text(color="grey25", size = 25), 
           axis.text.y= element_text(color="grey25"), 
           legend.text= element_text(color="grey25", size = 19), 
           legend.title = element_text(color = "grey25", size = 25), 
           axis.title.x = element_blank(),
           axis.title.y = element_text(color = "grey25", size = 25),
           legend.position="none",
           plot.margin=unit(c(1,1,1.5,1.2),"cm"))

t.embossing <- theme(panel.grid.major = element_blank(), 
                 panel.grid.minor = element_blank(), 
                 panel.grid.major.y = element_line(size=.4, color="grey28"),
                 panel.ontop = TRUE,
                 panel.background = element_blank(), 
                 axis.line = element_line(colour = "black"), 
                 plot.title=element_text(color = "grey25", hjust=.5, size = 25), 
                 axis.text.x = element_text(color="grey25", size = 25), 
                 axis.text.y= element_text(color="grey25"), 
                 legend.text= element_text(color="grey25", size = 19), 
                 legend.title = element_text(color = "grey25", size = 25), 
                 axis.title.x = element_text(color = "grey25", size = 25, margin = margin(t = 20, r = 0, b = 0, l = 0)),
                 axis.title.y = element_text(color = "grey25", size = 25),
                 plot.margin=unit(c(1,1,1.5,1.2),"cm"))


ggplot (d.nevada, aes(x=question, y=value, fill=answer)) + geom_bar(stat="identity", position=position_dodge(0.7), width=0.4) + labs (x="Question", y="Percentage", fill="Answer", title="Nevada Voting Results") + scale_y_continuous(breaks= c(0, 25, 50 ,75), limits=c(0, 75)) + t

ggsave("NevadaBarchart.png", width=16, height=9, dpi=128)


ggplot (d.maine, aes(x=question, y=value, fill=answer)) + geom_bar(stat="identity", position=position_dodge(0.7), width=0.4) + labs (x="Question", y="Percentage", fill="Answer", title="Maine Voting Results")  + scale_y_continuous(breaks= c(0, 25, 50 ,75), limits=c(0, 75)) + t

ggsave("MaineBarchart.png", width=16, height=9, dpi=128)


ggplot (d.meat, aes(x=product, y=number, fill=product)) + geom_bar(stat="identity", position=position_dodge(0.7), width=0.4) + labs (x="Food Group", y="Count", fill="Answer", title="Food Results")  + scale_y_continuous(breaks= c(0, 25, 50 ,75), limits=c(0, 75)) + t.small

ggsave("Foods.png", width=16, height=9, dpi=128)

f <- read.csv("breakfast.csv")
f.filtered <- f %>% filter(product == "Grain products" | product == "Milk products")
ggplot (f.filtered, aes(x=product, y=number, fill=gender)) + geom_bar(stat="identity", position=position_dodge(0.7), width=0.4) + labs (x="Breakfast Item", y="Count", fill="Answer", title="Breakfast Results") + ylim(0,100) + t

ggsave("breakfast.png", width=16, height=9, dpi=128)

